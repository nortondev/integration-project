package com.wwnorton.Integration.utlities;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;

public class Mailinator {

	WebDriver driver;

	// Initializing Web Driver and PageFactory.
	public Mailinator(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@Step("Search the Email in the Mailinator Website,  Method: {method} ")
	public String SearchEmail(String Email) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(
				driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));

		driver.navigate().to("https://www.mailinator.com/");
		Thread.sleep(5000);
		WebElement searchEmail = driver.findElement(By
				.xpath("//div[@class='input-group']/input[@type='text']"));
		searchEmail.sendKeys(Email);
		WebElement clickGoButton = driver
				.findElement(By
						.xpath("//div[@class='input-group-append']/button[@type='button']"));
		clickGoButton.click();
		return Email;

	}

	@Step("Click reset Password link on  Mailinator Website,  Method: {method} ")
	public void clickResetPasswordEmailLink() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//a[contains(text(),'Reset Password for W. W. Norton Account')]|//td[contains(text(),'Reset Password')]")));
		WebElement resetPasswordEmailLink = driver
				.findElement(By
						.xpath("//a[contains(text(),'Reset Password for W. W. Norton Account')]|//td[contains(text(),'Reset Password')]"));
		resetPasswordEmailLink.click();
	}

	@Step("Click reset Password link on  from the User Email,  Method: {method} ")
	public void clickResetPasswordLinkUserEmail() throws InterruptedException {
		Thread.sleep(4000);
		driver.switchTo().frame("html_msg_body");
		WebElement resetPasswordEmailLink = driver.findElement(By
				.partialLinkText("Reset the password"));
		resetPasswordEmailLink.click();
		driver.switchTo().defaultContent();
	}

	@Step("Verify W.W. Norton Instructor Account creation Email ,  Method: {method} ")
	public void instaccountcreationEmail() {
		ReusableMethods.checkPageIsReady(driver);
		String getemailSubject = null;
		String getinstAccount = null;
		List<WebElement> emailSubject = driver
				.findElements(By
						.xpath("//table[@class='table table-striped jambo_table']/tbody//td/a"));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		getemailSubject = emailSubject.get(0).getText();
		getinstAccount = emailSubject.get(1).getText();
		if(getemailSubject.equalsIgnoreCase("W. W. Norton Instructor Account".trim())){
			Assert.assertEquals("W. W. Norton Instructor Account".trim(),
					getemailSubject);
		}else {
		Assert.assertEquals(
				"W.W. Norton & Company, Inc. Account Confirmation".trim(),
				getemailSubject);
		}
		
		if(getinstAccount.equalsIgnoreCase("W. W. Norton Instructor Account".trim())){
			Assert.assertEquals("W. W. Norton Instructor Account".trim(),
					getinstAccount);
		}else {
		Assert.assertEquals(
				"W.W. Norton & Company, Inc. Account Confirmation".trim(),
				getinstAccount);
		}
		WebElement fromeleSeagual = driver
				.findElement(By
						.xpath("//text()[contains(.,'seagull@wwnorton.net')]/ancestor::td[1]"));
		WebElement fromelewwnorton = driver
				.findElement(By
						.xpath("//text()[contains(.,'W.W. Norton and Co.')]/ancestor::td[1]"));
		Assert.assertEquals("seagull@wwnorton.net".trim(),
				fromeleSeagual.getText());
		Assert.assertEquals("W.W. Norton and Co.".trim(),
				fromelewwnorton.getText());
		fromeleSeagual.click();
		WebElement fromEle = driver.findElement(By.xpath("//b[text()='seagull@wwnorton.net']"));
		Assert.assertEquals("seagull@wwnorton.net".trim(),
				fromEle.getText());
		driver.navigate().back();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		fromelewwnorton.click();
		WebElement fromElenoreply = driver.findElement(By.xpath("//b[text()='no-reply@wwnorton.net']"));
		Assert.assertEquals("no-reply@wwnorton.net".trim(),
				fromElenoreply.getText());
	}

	public String fullName(String emailName) {
		// TODO Auto-generated method stub
		
		driver.switchTo().frame("html_msg_body");
		List<WebElement> fullName = driver.findElements(By
				.xpath("/html/body/p[1]"));
		for(int i=0; i<fullName.size(); i++){
			@SuppressWarnings("unused")
			String emailbody = fullName.get(i).getText();
			 Pattern p = Pattern.compile(emailName);
		     Matcher m = p.matcher(emailName);	
		     while(m.find()){
		    	 System.out.println(m.group());
		     }
		
	}
		driver.switchTo().defaultContent();
		return emailName;
	}

	@Step("Verify Email Notifications Subject: W.W. Norton & Company, Inc. Account Confirmation and Subject: Your W.W. Norton Digital Product Registration,  Method: {method} ")
	public List<String> emailNotifications() {
		ReusableMethods.checkPageIsReady(driver);
		String getemailSubject = null;
	    ArrayList<String> emaiSubject = new ArrayList<String>();
		List<WebElement> emailSubject = driver
				.findElements(By
						.xpath("//table[@class='table table-striped jambo_table']/tbody/tr/td/a"));
		for(int i=0; i<emailSubject.size(); i++){
			getemailSubject =emailSubject.get(i).getText(); 
			emaiSubject.add(getemailSubject);
		}
		
		return emaiSubject;
	}
	@Step("Get the Password from Email body,  Method: {method} ")
	public String getPasswordEmailBody() {
		// TODO Auto-generated method stub
		String passwordText1 = null;
		String password=null;
		driver.switchTo().frame("html_msg_body");
		WebElement emailText = driver.findElement(By
				.xpath("/html/body"));		
		     passwordText1 = emailText.getText();
			 String segments[] = passwordText1.split("Password:");
		     password = segments[segments.length-1].split("You")[0]; 
		driver.switchTo().defaultContent();
		return password.trim();
	}
	
	@Step("Get the Your temporary password from Email body,  Method: {method} ")
	public String getTempPasswordEmailBody() {
		// TODO Auto-generated method stub
		String passwordText1 = null;
		String password=null;
		driver.switchTo().frame("html_msg_body");
		WebElement emailText = driver.findElement(By
				.xpath("/html/body"));		
		     passwordText1 = emailText.getText();
			 String segments[] = passwordText1.split("Your temporary password is:");
		     password = segments[segments.length-1].split("You")[0]; 
		driver.switchTo().defaultContent();
		return password.trim();
	}
	
	@Step("Click the GetStarted Button,  Method: {method} ")
	public void clickGetStarted() {
		// TODO Auto-generated method stub
		driver.switchTo().frame("html_msg_body");
		WebElement getStartedEle = driver.findElement(By
				.xpath("/html/body//table/tbody/tr/td/a[contains(text(),'Get Started')]"));
		getStartedEle.click();
		driver.switchTo().defaultContent();
	}
}
