package com.wwnorton.Integration.utlities;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class DataProvider {
	public static String UserDir = System.getProperty("user.dir");
	String dlpTaxoName;

	@org.testng.annotations.DataProvider(name = "DLT")
	public String[] readDataProviderJson() throws IOException, ParseException {
		
		JSONParser jsonparser = new JSONParser();
		FileReader reader = new FileReader(UserDir
				+ "//src//test//resources//ProductTitle.json");
		Object obj = jsonparser.parse(reader);
		JSONObject productCode = (JSONObject) obj;
		JSONObject taxoName = (JSONObject) productCode.get("taxonomyName");
		dlpTaxoName= (String) taxoName.get("entitlementName");
		JSONArray productarr = (JSONArray) productCode.get("ProductTitle");
		String arr[] = new String[productarr.size()] ;
		for(int i=0; i<productarr.size(); i++){
			JSONObject productdata = (JSONObject) productarr.get(i);
			String generalKey = (String) productdata.get("general");
			String aliasKey = (String) productdata.get("alias");
			arr[i] = generalKey+","+aliasKey;
			
		}

		return arr;

	}
	
	public static List<String> readDataProviderEntlJson() throws IOException, ParseException {
		Object list = null;
		ArrayList<String> addlist = new ArrayList<String>();
		JSONParser jsonparser = new JSONParser();
		FileReader reader = new FileReader(UserDir
				+ "//src//test//resources//Product_Code.json");
		JSONObject obj = (JSONObject) jsonparser.parse(reader);
		JSONArray productCodelist = (JSONArray) obj.get("ProductCodeList");
        String[] returnValue =new String[productCodelist.size()];
        System.out.println(returnValue.length);
		for(int i=0; i<productCodelist.size(); i++){
			list =  productCodelist.get(i);
			addlist.add(list.toString());
		}
		return addlist;
       
	}
	
}
