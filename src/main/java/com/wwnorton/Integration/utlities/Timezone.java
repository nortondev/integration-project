package com.wwnorton.Integration.utlities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Timezone {
	static SimpleDateFormat nciadtFormat = new SimpleDateFormat("MM/dd/YY hh:mm aa");

	//private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
	static LocalTime minutes;

	/*
	 * public static void main(String[] args){
	 * System.out.println(getthirtyMinutesTime());
	 * 
	 * }
	 */

	public static String getHourTime() {
		ZoneId z = ZoneId.systemDefault();
		String zoneName = z.getId();
		if (zoneName.equalsIgnoreCase("America/New_York")) {
			ZoneId fromTimeZone = ZoneId.of("America/New_York");
			LocalDateTime today = LocalDateTime.now();
			ZonedDateTime currentESTTime = today.atZone(fromTimeZone);
			minutes = currentESTTime.toLocalTime().plusMinutes(60);
		} else if (zoneName.equalsIgnoreCase("Asia/Calcutta")) {
			ZoneId fromTimeZone = ZoneId.of("Asia/Calcutta"); // Source timezone
			ZoneId toTimeZone = ZoneId.of("America/New_York"); // Target
																// timezone

			LocalDateTime today = LocalDateTime.now(); // Current time

			// Zoned date time at source timezone
			ZonedDateTime currentISTime = today.atZone(fromTimeZone);

			// Zoned date time at target timezone
			ZonedDateTime currentETime = currentISTime
					.withZoneSameInstant(toTimeZone);

			minutes = currentETime.toLocalTime().plusMinutes(60);
			/*
			 * if(minutes.isBefore(LocalTime.of(06, 00)) &&
			 * (minutes.isAfter(LocalTime.of(12,00)))){ LocalTime time
			 * =minutes.now();
			 */
		}
		String RoundoffTime = getNearestHourQuarter(minutes);

		return RoundoffTime;

	}

	public static String getISTTime() {
		ZoneId z = ZoneId.systemDefault();
		String zoneName = z.getId();
		LogUtil.log(zoneName);
		ZoneId fromTimeZone = ZoneId.of("Asia/Calcutta"); 
		LocalDateTime today = LocalDateTime.now(); // Current time

		// Zoned date time at source timezone
		ZonedDateTime currentISTime = today.atZone(fromTimeZone);
		LocalTime minutes = currentISTime.toLocalTime();
		String RoundoffTime = getNearestHourQuarter(minutes);
		return RoundoffTime;
	}
	
	public static String getESTTime() {
		String estTime=null;
		LocalTime now = LocalTime.now(ZoneId.of("America/New_York"));
		estTime =now.toString().substring(0, 5);			
		return estTime;
		
	}

	public static String getthirtyMinutesTime() {
		ZoneId z = ZoneId.systemDefault();
		String zoneName = z.getId();
		if (zoneName.equalsIgnoreCase("America/New_York")) {
			ZoneId fromTimeZone = ZoneId.of("America/New_York");
			LocalDateTime today = LocalDateTime.now();
			ZonedDateTime currentESTTime = today.atZone(fromTimeZone);
			minutes = currentESTTime.toLocalTime().plusMinutes(10);
		} else if (zoneName.equalsIgnoreCase("Asia/Calcutta")) {
			ZoneId fromTimeZone = ZoneId.of("Asia/Calcutta"); // Source timezone
			ZoneId toTimeZone = ZoneId.of("America/New_York"); // Target
																// timezone

			LocalDateTime today = LocalDateTime.now(); // Current time

			// Zoned date time at source timezone
			ZonedDateTime currentISTime = today.atZone(fromTimeZone);

			// Zoned date time at target timezone
			ZonedDateTime currentETime = currentISTime
					.withZoneSameInstant(toTimeZone);

			minutes = currentETime.toLocalTime().plusMinutes(10);

		}
		String RoundoffTime = getNearestHourQuarter(minutes);

		return RoundoffTime;

	}

	public static String getNearestHourQuarter(LocalTime dt2) {
		int hour = dt2.getHour();
		int minutes = dt2.getMinute();

		String meridiam1 = dt2.format(DateTimeFormatter.ofPattern("a"));

		if (minutes > 30) {
			hour = hour + 1;
			minutes = 0;
		} else if (minutes > 0) {
			minutes = 30;
		}

		int hour_12_format = hour % 12;
		if (hour_12_format == 0 && minutes == 0) {
			if (meridiam1.compareTo("AM") == 0 || hour == 24) {
				hour_12_format = 11;
				minutes = 59;
				meridiam1 = "PM";
			} else {
				hour_12_format = 12;
				minutes = 0;
				meridiam1 = "PM (Noon)";
			}
		} else if (hour_12_format < 6 && meridiam1.compareTo("AM") == 0) {
			hour_12_format = 6;
			minutes = 0;
		} else if (hour_12_format == 0 && meridiam1.compareTo("PM") == 0) {
			hour_12_format = 12;
		}

		return hour_12_format + ":" + String.format("%02d", minutes) + " "
				+ meridiam1;

	}

	public static String get5MinsTime() {
		ZoneId z = ZoneId.systemDefault();
		String zoneName = z.getId();
		if (zoneName.equalsIgnoreCase("America/New_York")) {
			ZoneId fromTimeZone = ZoneId.of("America/New_York");
			LocalDateTime today = LocalDateTime.now();
			ZonedDateTime currentESTTime = today.atZone(fromTimeZone);
			minutes = currentESTTime.toLocalTime().plusMinutes(10);
		} else if (zoneName.equalsIgnoreCase("Asia/Calcutta")) {
			ZoneId fromTimeZone = ZoneId.of("Asia/Calcutta"); // Source timezone
			ZoneId toTimeZone = ZoneId.of("America/New_York"); // Target
																// timezone

			LocalDateTime today = LocalDateTime.now(); // Current time

			// Zoned date time at source timezone
			ZonedDateTime currentISTime = today.atZone(fromTimeZone);

			// Zoned date time at target timezone
			ZonedDateTime currentETime = currentISTime
					.withZoneSameInstant(toTimeZone);

			minutes = currentETime.toLocalTime();

		}
		String RoundoffTime = getNearestHourQuarter(minutes);

		return RoundoffTime;

	}

	public static String getNearesttime(LocalTime dt2) {
		int hour = dt2.getHour();
		int minutes = dt2.getMinute();

		String meridiam1 = dt2.format(DateTimeFormatter.ofPattern("a"));

		if (minutes > 50) {
			hour = hour + 1;
			minutes = minutes + 8;
		} else if (minutes > 30 && minutes < 49) {
			minutes = minutes + 8;
		} else if (minutes > 20 && minutes < 29) {
			minutes = minutes + 8;
		} else if (minutes > 0 && minutes < 19) {
			minutes = minutes + 10;
		}

		int hour_12_format = hour % 12;
		if (hour_12_format == 0 && minutes == 0) {
			if (meridiam1.compareTo("AM") == 0 || hour == 24) {
				hour_12_format = 11;
				minutes = 59;
				meridiam1 = "PM";
			} else {
				hour_12_format = 12;
				minutes = 0;
				meridiam1 = "PM (Noon)";
			}
		} else if (hour_12_format < 6 && meridiam1.compareTo("AM") == 0) {
			hour_12_format = 6;
			minutes = 0;
		} else if (hour_12_format == 0 && meridiam1.compareTo("PM") == 0) {
			hour_12_format = 12;
		}

		return hour_12_format + ":" + String.format("%02d", minutes) + " "
				+ meridiam1;

	}
	
	public static String  gettime24hrFormat(String time) throws ParseException{
		 SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
	       SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
	       Date date = parseFormat.parse(time);
	       System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
	       String output = displayFormat.format(date);
		return output;
	}
	
	public static String getAUSTime() {
		ZoneId z = ZoneId.systemDefault();
		String zoneName = z.getId();
		if (zoneName.equalsIgnoreCase("Australia/Brisbane")) {
			ZoneId fromTimeZone = ZoneId.of("Australia/Brisbane");
			LocalDateTime today = LocalDateTime.now();
			ZonedDateTime currentESTTime = today.atZone(fromTimeZone);
			minutes = currentESTTime.toLocalTime().plusMinutes(60);
		} else if (zoneName.equalsIgnoreCase("Asia/Calcutta")) {
			ZoneId fromTimeZone = ZoneId.of("Asia/Calcutta"); // Source timezone
			ZoneId toTimeZone = ZoneId.of("Australia/Brisbane"); // Target
																// timezone

			LocalDateTime today = LocalDateTime.now(); // Current time

			// Zoned date time at source timezone
			ZonedDateTime currentISTime = today.atZone(fromTimeZone);

			// Zoned date time at target timezone
			ZonedDateTime currentBTime = currentISTime
					.withZoneSameInstant(toTimeZone);

			minutes = currentBTime.toLocalTime();
			/*
			 * if(minutes.isBefore(LocalTime.of(06, 00)) &&
			 * (minutes.isAfter(LocalTime.of(12,00)))){ LocalTime time
			 * =minutes.now();
			 */
		}
		String RoundoffTime = getNearestHourQuarter(minutes);

		return RoundoffTime;

	}
	public static String get5MinsAUSTime() {
		ZoneId z = ZoneId.systemDefault();
		String zoneName = z.getId();
		if (zoneName.equalsIgnoreCase("Australia/Brisbane")) {
			ZoneId fromTimeZone = ZoneId.of("Australia/Brisbane");
			LocalDateTime today = LocalDateTime.now();
			ZonedDateTime currentESTTime = today.atZone(fromTimeZone);
			minutes = currentESTTime.toLocalTime().plusMinutes(10);
		} else if (zoneName.equalsIgnoreCase("Asia/Calcutta")) {
			ZoneId fromTimeZone = ZoneId.of("Asia/Calcutta"); // Source timezone
			ZoneId toTimeZone = ZoneId.of("Australia/Brisbane"); // Target
																// timezone

			LocalDateTime today = LocalDateTime.now(); // Current time

			// Zoned date time at source timezone
			ZonedDateTime currentISTime = today.atZone(fromTimeZone);

			// Zoned date time at target timezone
			ZonedDateTime currentETime = currentISTime
					.withZoneSameInstant(toTimeZone);

			minutes = currentETime.toLocalTime();

		}
		String RoundoffTime = getNearestHourQuarter(minutes);

		return RoundoffTime;

	}
	private static final String DATE_FORMAT = "MM/dd/yy hh:mm a";
	public static String timeConversionZone(String dateInString, String fromzoneName, String totimeZone) {
		 
	        LocalDateTime ldt = LocalDateTime.parse(dateInString, DateTimeFormatter.ofPattern(DATE_FORMAT));

	        ZoneId fromTimeZone = ZoneId.of(fromzoneName);
	        System.out.println("TimeZone : " + fromTimeZone);

	        //LocalDateTime + ZoneId = ZonedDateTime
	        ZonedDateTime fromZonedDateTime = ldt.atZone(fromTimeZone);
	        System.out.println("Date and Time "+fromzoneName+" : " + fromZonedDateTime);

	        ZoneId toZoneId = ZoneId.of(totimeZone);
	        System.out.println("TimeZone : " + toZoneId);

	        ZonedDateTime toDateTime = fromZonedDateTime.withZoneSameInstant(toZoneId);
	        System.out.println("Date and Time "+totimeZone+" : " + toDateTime);

	        DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_FORMAT);
	        System.out.println("\n---DateTimeFormatter---");
	        System.out.println("Date  " + format.format(fromZonedDateTime));
	        System.out.println("Date  " + format.format(toDateTime));
			return format.format(toDateTime).toString();
	}

	
	
}
