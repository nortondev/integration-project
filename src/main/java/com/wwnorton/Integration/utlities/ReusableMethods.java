package com.wwnorton.Integration.utlities;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.Objects.LMSCANVAS;
import com.wwnorton.Integration.Objects.NLTPage;

public class ReusableMethods {
	public static String UserDir = System.getProperty("user.dir");
	LMSCANVAS LMSpage;
	NLTPage nlt;
	static Properties prop = new Properties();
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();

	public static void checkPageIsReady(WebDriver driver) {

		boolean isPageReady = false;
		JavascriptExecutor js = (JavascriptExecutor) driver;

		while (!isPageReady) {

			isPageReady = js.executeScript("return document.readyState")
					.toString().equals("complete");

			try {

				Thread.sleep(5000);

			} catch (InterruptedException e) {

				e.printStackTrace();

			}
		}
	}

	public static boolean elementExist(WebDriver driver, String xpath) {

		try {

			driver.findElement(By.xpath(xpath));

		} catch (NoSuchElementException e) {
			return false;
		}

		return true;
	}
	
	public static boolean isdisplayed(WebDriver driver,String xpath){
		boolean isElementexist = driver.findElements(By.xpath(xpath)).size() >0;
		if(isElementexist==true){
			return true;
		}
		return false;
		
	}

	public static void scrollToBottom(WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		try {			
			   js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollToElement(WebDriver driver, By by) {
		try {
			WebElement element = driver.findElement(by);
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.perform();
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollIntoView(WebDriver driver, WebElement webElement) {
		try {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView(true);", webElement);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByXY(WebDriver driver, int xOffset, int yOffset) {
		try {
			String script = "scroll(" + xOffset + ", " + yOffset + ")";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByX(WebDriver driver, int xOffset) {
		try {
			String script = "scroll(" + xOffset + ", 0)";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByY(WebDriver driver, int yOffset) {
		try {
			String script = "scroll(0, " + yOffset + ")";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void waitForPageLoad(WebDriver driver)
			throws InterruptedException {
		Thread.sleep(5000);
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(pageLoadCondition);
	}

	public static void questionCloseLink(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.className("close")));
		driver.findElement(By.className("close")).click();
	}

	public static void waitVisibilityOfElement(WebDriver driver, By by) {

		WebDriverWait Wait = new WebDriverWait(driver, 50);
		Wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public static void waitElementClickable(WebDriver driver, By by) {

		WebDriverWait Wait = new WebDriverWait(driver, 50);
		Wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	public static void loadingWaitDisapper(WebDriver driver, WebElement loader) {
		WebDriverWait wait = new WebDriverWait(driver, 1000L);
		wait.until(ExpectedConditions.visibilityOf(loader)); // wait for loader
																// to appear
		wait.until(ExpectedConditions.invisibilityOf(loader)); // wait for
																// loader to
																// disappear
	}

	// Download the File in download Folder
	public static void downLoadFile() {
		String fileDownloadPath = System.getProperty("user.dir")
				+ "\\Downloads";

		Map<String, Object> prefsMap = new HashMap<String, Object>();
		prefsMap.put("profile.default_content_settings.popups", 0);
		prefsMap.put("download.default_directory", fileDownloadPath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefsMap);
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		// driver = new ChromeDriver(options);
	}

	public static void processZipFile(String zipFilePath, String destDir) {
		try {
			File directoryToScan = new File(zipFilePath);
			if (directoryToScan.isDirectory()) {
				for (File file : directoryToScan.listFiles()) {
					unzipFile(file, destDir);
				}
			} else {
				unzipFile(directoryToScan, destDir);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void unzipFile(File zipFilePath, String destDir) {
		File dir = new File(destDir);
		// create output directory if it doesn't exist
		if (!dir.exists())
			dir.mkdirs();
		FileInputStream fis;
		// buffer for read and write data to file
		byte[] buffer = new byte[1024];
		try {
			System.out.println("Unzipping file : "
					+ zipFilePath.getAbsolutePath());
			fis = new FileInputStream(zipFilePath);
			ZipInputStream zis = new ZipInputStream(fis);
			ZipEntry ze = zis.getNextEntry();
			while (ze != null) {
				String fileName = ze.getName();
				File newFile = new File(destDir + File.separator + fileName);
				System.out.println("Unzipping to " + newFile.getAbsolutePath());
				// create directories for sub directories in zip
				new File(newFile.getParent()).mkdirs();
				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
				// close this ZipEntry
				zis.closeEntry();
				ze = zis.getNextEntry();
			}
			// close last ZipEntry
			zis.closeEntry();
			zis.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void deleteFile(String zipFilePath) {
		File dir = new File(zipFilePath);
		if (dir.isDirectory() == false) {
			return;
		}
		File[] listFiles = dir.listFiles();
		for (File file : listFiles) {
			LogUtil.log("Deleting " + file.getName());
			file.delete();
		}
	}

	public static void deleteFileExt(String zipFilePath) {
		File temp = new File(zipFilePath);
		temp.getAbsolutePath();
		temp.deleteOnExit();
	}

	public static void clickElement(WebDriver driver, WebElement Element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", Element);
	}

	public static void switchToNewWindow(int windowNumber, WebDriver driver) {
		Set<String> s = driver.getWindowHandles();
		Iterator<String> ite = s.iterator();
		int i = 1;
		while (ite.hasNext() && i < 10) {
			String popupHandle = ite.next().toString();
			driver.switchTo().window(popupHandle);
			LogUtil.log("Window title is : " + driver.getTitle());
			if (i == windowNumber)
				break;
			i++;
		}
	}

	public static void closeCurrentWindow(int windowNumber, WebDriver driver) {
		Set<String> s = driver.getWindowHandles();
		Iterator<String> ite = s.iterator();
		int i = 1;
		while (ite.hasNext() && i < 10) {
			String popupHandle = ite.next().toString();
			driver.switchTo().window(popupHandle);
			LogUtil.log("Window title is : " + driver.getTitle());
			System.out.println(driver.getTitle());

			if (i == windowNumber) {
				Actions action = new Actions(driver);
				action.sendKeys(Keys.chord(Keys.CONTROL, "w")).build()
						.perform();
				break;
			}
			i++;
		}
	}
	public static String removeAsciiChar(String str) {
		return str = str.replaceAll("[^\\p{ASCII}]", "");

	}

	public static void closeCurrentWindowByTitle(String windowTitle,
			WebDriver driver) {
		Set<String> winHandles = driver.getWindowHandles();
		// String currentWin = driver.getWindowHandle();
		for (String handle : winHandles) {
			driver.switchTo().window(handle);
			if (windowTitle.equalsIgnoreCase(driver.getTitle())) {
				break;
			}
			driver.close();
		}

	}
	public static void SwitchTabandClose(WebDriver driver,String windowTitle)
	{
	    Set <String> windows = driver.getWindowHandles();
	   // String mainwindow = driver.getWindowHandle();
	    for (String handle: windows)
	    {
	        driver.switchTo().window(handle);
	        LogUtil.log("switched to "+driver.getTitle()+"  Window");
	        String pagetitle = driver.getTitle();
	        if(pagetitle.equalsIgnoreCase(windowTitle))
	        {
	            driver.close();
	            LogUtil.log("Closed the  '"+pagetitle+"' Tab now ...");
	        }
	    }
	    //driver.switchTo().window(mainwindow);
	 }

	public static void altTab(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.chord(Keys.CONTROL, "T")).build().perform();
	}

	public static String getChildWindow(WebDriver driver) {
		String childWindow = null;
		for (String winHandle : driver.getWindowHandles()) {
			childWindow = winHandle;
			driver.switchTo().window(winHandle);
		}

		return childWindow;

	}

	// get Numbers
	public static int getDigits(String text) {
		String str = text.replaceAll("[^0-9]", "");
		int v = Integer.parseInt(str);
		return v;

	}

	// Split String
	public static String splitString(String text) {
		String[] str = text.split(Pattern.quote("("));
		String strNumber = str[0];
		return strNumber;

	}

	// isDisplayed
	public static void elementdisplayed(WebDriver driver, By by) {
		boolean displayed = false;
		do {

			try {
				displayed = driver.findElement(by).isDisplayed();
			} catch (Exception e) {
				driver.navigate().refresh();
			}

		} while (!displayed);
	}

	public static void scrollIntoViewClick(WebElement element, WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		js.executeScript("arguments[0].click();", element);
	}

	public static String getpercentage(String text) {
		String result = null;
		Pattern pat = Pattern.compile("\\d+%");
		Matcher mm = pat.matcher(text);
		if (mm.find()) {
			result = mm.group();

		}
		return result;
	}

	public static void clickTAB(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.chord(Keys.TAB)).build().perform();
	}
	
	public static void clickEnter(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.chord(Keys.ENTER)).build().perform();
	}

	public static boolean isFileDownloaded(String downloadPath, String fileName) {
		boolean flag = false;
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().equals(fileName))
				return flag = true;
		}
		return flag;
	}

	/* Check the file from a specific directory with extension */
	public static boolean isFileDownloaded_Ext(String dirPath, String ext) {
		boolean flag = false;
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			flag = false;
		}

		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().contains(ext)) {
				flag = true;
			}
		}
		return flag;
	}

	/* getFile Name */
	public static String getFileName(String dirPath) {
		String fileName = null;
		File dir = new File(dirPath);
		String files[] = dir.list();
		for (int i = 0; i < files.length; i++) {
			fileName = files[i];
		}

		return fileName;
	}

	public static void fileDownload() {
		String filepath = System.getProperty("user.dir") + "\\Downloads\\";
		System.out.println(filepath);
		// ReusableMethods.isFileDownloaded(filepath, fileName);
		ReusableMethods.isFileDownloaded_Ext(filepath, ".csv");
		String fileName = ReusableMethods.getFileName(filepath);
		String zipFilePath = filepath + fileName;
		// ReusableMethods.deleteFile(zipFilePath);
		ReusableMethods.deleteFileExt(zipFilePath);
	}

	public static void clickOkButton(WebDriver driver)
			throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[starts-with(@id,'general_dialog_outer')]/following::div[@class='ui-dialog-buttonset']/button[@role='button']/span[contains(.,'OK')]")));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			
		}
		boolean isOKButton = driver.findElements(By
				.xpath("//div[@class='ui-dialog-buttonset']/button[@role='button']/span[contains(.,'OK')]")).size()>0;
		if(isOKButton==true){
		
		WebElement okele = driver
				.findElement(By
						.xpath("//div[@class='ui-dialog-buttonset']/button[@role='button']"));
		try {
			okele.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
		}
		}
		Thread.sleep(2000);

	}

	public static void waitMainQuestionHolderLoad(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("main_question_holder")));
	}

	public static void clickTryAgainButton(WebDriver driver)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//button[@role='button']/span[contains(.,'Try Again')]")));
		WebElement clickTryAgaian = driver
				.findElement(By
						.xpath("//button[@role='button']/span[contains(.,'Try Again')]"));
		clickTryAgaian.click();
		Thread.sleep(1000);
	}
	public static void setDLTEnvironment(WebDriver driver) throws IOException, InterruptedException {
		InputStream input = null;	
		LMSCANVAS lc = new LMSCANVAS();
		try {
			input = new FileInputStream(UserDir
					+ "\\src\\test\\resources\\DLTURL.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		prop.load(input);		
		String environment = prop.getProperty("environment");
		if(environment.equalsIgnoreCase("QA")){
			driver.findElement(By.xpath("//nav[@role='navigation']/ul[@id='section-tabs']/li[@class='section']/a[contains(.,'DLT - QA')]")).click();
		}else if(environment.equalsIgnoreCase("STG")){
			driver.findElement(By.xpath("//nav[@role='navigation']/ul[@id='section-tabs']/li[@class='section']/a[contains(.,'DLT - STG')]")).click();
		}else if(environment.equalsIgnoreCase("UAT")){
			driver.findElement(By.xpath("//nav[@role='navigation']/ul[@id='section-tabs']/li[@class='section']/a[contains(.,'DLT - UAT')]")).click();
		}else if(environment.equalsIgnoreCase("PRODUCTION")){
			lc.clickNavigationlink("Norton Deep Linking Tool");
			Thread.sleep(2000);
			
		}
			
	}
	
	public static String getDLTEnvironment(WebDriver driver) throws IOException {
		InputStream input = null;		
		try {
			input = new FileInputStream(UserDir
					+ "\\src\\test\\resources\\DLTURL.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		prop.load(input);		
		String environment = prop.getProperty("environment");
		return environment;
	}

	
	//Date Conversion 
	public static String dateConversion(String dateSTring) throws ParseException{
		String[] dateStringarry =dateSTring.split("at");
		String getnewdate = dateStringarry[0].replaceAll(",", "").trim();
		Date myDate = new SimpleDateFormat("MMM dd yyyy").parse(getnewdate);
		String myDate1 = new SimpleDateFormat("MM/dd/yyyy").format(myDate);
		return myDate1;
	}
	
	public static String dateConversioninDLtformat(String dateSTring) throws ParseException{
		String[] dateStringarry =dateSTring.split("at");
		String getnewdate = dateStringarry[0].replaceAll(",", "").trim();
		Date myDate = new SimpleDateFormat("MM/dd/yyyy").parse(getnewdate);
		String myDate1 = new SimpleDateFormat("EEEE, MMMM d, yyyy").format(myDate);
		return myDate1;
	}
	
	public static String formatMonth(String month) throws ParseException {
	    SimpleDateFormat monthParse = new SimpleDateFormat("MM");
	    SimpleDateFormat monthDisplay = new SimpleDateFormat("MMMM");
	    return monthDisplay.format(monthParse.parse(month));
	}
	
//Uppercase Letter 
	public static String upperCaseWords(String sentence) {
        String words[] = sentence.replaceAll("\\s+", " ").trim().split(" ");
        String newSentence = "";
        for (String word : words) {
            for (int i = 0; i < word.length(); i++)
                newSentence = newSentence + ((i == 0) ? word.substring(i, i + 1).toUpperCase(): 
                    (i != word.length() - 1) ? word.substring(i, i + 1).toLowerCase() : word.substring(i, i + 1).toLowerCase().toLowerCase() + " ");
        }

        return newSentence;
    }
	
	//LMS Course Creation 
	public String courseCreation(String emailID, WebDriver driver) throws InterruptedException{
		String getrandonValue = GetRandomId.randomAlphaNumeric(3);
		String courseName;
		String subAccountlink = jsonobject.getAsJsonObject("SubAccountLink")
				.get("LinkName").getAsString();
		LMSpage = new LMSCANVAS();
		nlt= new NLTPage();
		LMSpage.navigateLMS();
		LMSpage.adminloginLMSCANVAS();
		LMSpage.clicklink("Admin");
		LMSpage.clickAdminlink("W.W. Norton");
		LMSpage.clickNavigationlink("Sub-Accounts");
		LMSpage.clickDLTlink(subAccountlink);
		//Click Course Button 
		LMSpage.clickCourseButton();
		courseName = "DEMOAUTOMATIONDLT " + getrandonValue;
		LMSpage.setDLTCourseName(courseName);
		LMSpage.setDLTReferenceCode("Demo");
		LMSpage.selectSubAccount();
		LMSpage.selectEnrollement();
		LMSpage.clickAddCourse();
		Thread.sleep(2000);
		LMSpage.selectCourseFilter();
		LMSpage.SearchCourse(courseName);
		Thread.sleep(2000);
		LMSpage.clickCourseLink(courseName);
		LMSpage.clickNavigationlink("People");
		LMSpage.clickPeopleButton();
		Thread.sleep(2000);
		LMSpage.selectRadioOption();
		LMSpage.enterEmailTextArea(emailID.toLowerCase());
		LMSpage.selectRole("Teacher");
		LMSpage.clickNextButton();
		Thread.sleep(2000);
		boolean isaddNameLink = driver.findElements(By.xpath("//button[@data-address='"+emailID+"']")).size()>0;
		if(isaddNameLink==true){
		LMSpage.clickAddNameLink(emailID.toLowerCase());
		LMSpage.enterUserName(emailID.toLowerCase());
		Thread.sleep(2000);
		LMSpage.clickNextButton();
		Thread.sleep(3000);
		LMSpage.clickAddUserButton();
		} else {
			LMSpage.clickAddUserButton();
		}
		Assert.assertEquals("pending", LMSpage.getPendingText());
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
		return courseName;
	}
	
	public static void comparePartofString(String str1, String str2){
		int len = str2.length();
		boolean foundIt = false;
		int i = 0;		
		while (!str1.regionMatches(i, str2, 0, len)) {
			i++;
			foundIt = true;
			System.out.println(i);
			}
			
		if (foundIt) {
			System.out.println(str1.substring(i, i+len));
		}
	}
	
}