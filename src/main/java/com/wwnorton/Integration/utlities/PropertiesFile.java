package com.wwnorton.Integration.utlities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;

public class PropertiesFile extends BaseDriver {

	public static WebDriverWait wait;
	public static String UserDir = System.getProperty("user.dir");
	public static String browser;
	public static String url, dlpName, websiteURL, LMSURL,nltTitleCode,wwnortonTitleCode;
	public static String DriverPath;
	static Properties prop = new Properties();

	// Read Browser name, Test url and Driver file path from config.properties.

	public static void loadProperties() throws IOException {
		InputStream input = null;
		try {
			input = new FileInputStream(UserDir
					+ "\\src\\test\\resources\\config.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		prop.load(input);
	}

	public static void readPropertiesFile() throws Exception {

		loadProperties();
		LMSURL = prop.getProperty("CanvasURL");
		dlpName = prop.getProperty("dlpName");
		DriverPath = UserDir + "\\Drivers\\";
		url = prop.getProperty("NCIAtesturl");
		nltTitleCode = prop.getProperty("titleCode");
		wwnortonTitleCode =prop.getProperty("websiteTitleCode");

	}

	// Set Browser configurations by comparing Browser name and Diver file path.
	// @Parameters({"browser"})
	public static void setBrowserConfig(String browser)
			throws IllegalAccessException, IOException {
		loadProperties();
		browser = prop.getProperty("browsername");
		BaseDriver bd = new BaseDriver();
		bd.init_driver(browser);

	}

	// Set Test URL based on config.properties file.

	public static void setURL(String dlpTaxonomyName) {
		if (dlpTaxonomyName.isEmpty()) {
			getDriver().manage().deleteAllCookies();
			getDriver().manage().timeouts()
					.implicitlyWait(15, TimeUnit.SECONDS);
			getDriver().get(url + wwnortonTitleCode);
		} else {
			getDriver().manage().deleteAllCookies();
			getDriver().manage().timeouts()
					.implicitlyWait(15, TimeUnit.SECONDS);
			getDriver().get(url + dlpTaxonomyName);
		}
		
	}
	
	

	// get WebSite URL
	public static void webSiteURL() {

		getDriver().manage().deleteAllCookies();
		getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		getDriver().get(websiteURL);
	}

	// get the title code 
		public static String gettitleCode() {
			try {
				readPropertiesFile();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return nltTitleCode;
		}
		
		// get the title code 
				public static String getwwnortontitleCode() {
					try {
						readPropertiesFile();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return wwnortonTitleCode;
				}
	// get LMS URL
	public static void setlmsURL() {

		getDriver().manage().deleteAllCookies();
		getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		getDriver().get(LMSURL);
	}

	// Close the driver after running test script.

	public static void tearDownTest() throws InterruptedException {

		getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// getDriver().close();
		getDriver().quit();

	}

	/*public static void getEntlName() throws IOException, ParseException {
		// TODO Auto-generated method stub
		Object[] entName = DataProvider.readDataProviderEntlJson();
		getDriver().manage().deleteAllCookies();
		getDriver().manage().timeouts()
				.implicitlyWait(15, TimeUnit.SECONDS);
		getDriver().get(url + entName);
	}*/

	

}
