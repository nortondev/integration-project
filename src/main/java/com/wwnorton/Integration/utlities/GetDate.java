package com.wwnorton.Integration.utlities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class GetDate {

	static Date currentDate;
	private static final DateFormat dateFormat = new SimpleDateFormat(
			"MM/dd/yyyy");
	static String d2,d1;
	static Calendar cal = Calendar.getInstance(TimeZone.getDefault());
	private static final long ONE_DAY_MILLI_SECONDS = 24 * 60 * 60 * 1000;
	static SimpleDateFormat dayFormatmnth = new SimpleDateFormat("MM");
	static SimpleDateFormat df = new SimpleDateFormat("MMM dd yyyy");
	static SimpleDateFormat dt = new SimpleDateFormat("hh:mm aa");
	static SimpleDateFormat nciadtFormat = new SimpleDateFormat("MM/dd/YY hh:mm aa");
	/*
	 * public static void main(String[] args){ GetDate f = new GetDate();
	 * System.out.println(f.getCurrentDate());
	 * System.out.println(f.getNextmonthDate()); }
	 */

	public static String getCurrentDate() {

		currentDate = new Date();
		String SystemDate = (dateFormat.format(currentDate));
		return SystemDate;
	}

	public static String getSystemTime() {
		DateTimeFormatter dtf = DateTimeFormatter
				.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}

	public static String getNextmonthDate() {
		// convert date to calendar
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);

		// manipulate date
		// c.add(Calendar.YEAR, 1);
		c.add(Calendar.MONTH, 1);
		c.add(Calendar.DATE, 1); // same with c.add(Calendar.DAY_OF_MONTH, 1);

		// convert calendar to date
		Date currentDatePlusOne = c.getTime();

		String nextmonthdate = (dateFormat.format(currentDatePlusOne));
		return nextmonthdate;

	}

	public static String addDays(int numberOfDays) {

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, numberOfDays);

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String date = dateFormat.format(cal.getTime());

		return date;

	}
	
	public static String addDaysBasedonDate(String currentDate, int numberOfDays) {
		Date date1 = null;
		try {
			date1 = new SimpleDateFormat("MM/dd/yyyy").parse(currentDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		cal.add(Calendar.DATE, numberOfDays);
		Date date = cal.getTime();
		String dates = dateFormat.format(date);
		return dates;

	}

	public static String getNextweekDate() {

		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);

		// manipulate date
		// c.add(Calendar.YEAR, 1);
		c.add(Calendar.DAY_OF_MONTH, 7);
		// c.add(Calendar.DATE, 28);

		Date currentDatePlusOne = c.getTime();

		String nextweekdate = (dateFormat.format(currentDatePlusOne));
		return nextweekdate;
	}

	public static String getLastDayofMonth() {
		Date today = new Date();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(today);

		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.DATE, -1);

		Date lastDayOfMonth = calendar.getTime();

		DateFormat sdf = new SimpleDateFormat("dd");
		return sdf.format(lastDayOfMonth);

	}

	public static int lastdayoftheMonth() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.getActualMaximum(Calendar.DATE);
		return day;
	}

	public static String getCurrentDay() {
		int todayInt = cal.get(Calendar.DAY_OF_MONTH);
		String todayStr = Integer.toString(todayInt);
		return todayStr;
	}

	public static String getCurrentDayPlus(int days) {
		LocalDate currentDate = LocalDate.now();
		int dayOfWeekPlus = currentDate.getDayOfWeek().plus(days).getValue();
		return Integer.toString(dayOfWeekPlus);
	}

	public static String previousDay(String dataValue) {

		Date d = null;
		try {
			d = dateFormat.parse(dataValue);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dateFormat.format(d);
		long previousDayMilliSeconds = d.getTime() - ONE_DAY_MILLI_SECONDS;
		Date previousDate = new Date(previousDayMilliSeconds);
		String previousDateStr = dateFormat.format(previousDate);

		return previousDateStr;

	}
	public static String nextDay(String dataValue) {

		Date d = null;
		try {
			d = dateFormat.parse(dataValue);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dateFormat.format(d);
		long nextDayMilliSeconds = d.getTime() + ONE_DAY_MILLI_SECONDS;
		Date nextDate = new Date(nextDayMilliSeconds);
		String nextDateStr = dateFormat.format(nextDate);
		return nextDateStr;

	}
	
	
	@SuppressWarnings("deprecation")
	public static int getCurrentMonth(){
		String getmonth = GetDate.getCurrentDate();
		String currentMonth = dayFormatmnth.format(Date.parse(getmonth));
		int cMonth = Integer.parseInt(currentMonth);
		return cMonth;
	}
	
	@SuppressWarnings("deprecation")
	public static String getDateandTime(String dateTimeValue){
		Date d =null;
		String newdatevalue =dateTimeValue.replace(",", "");
		newdatevalue = newdatevalue.replace(",", "").replaceAll("\\[", "").replaceAll("\\]", "");
		/*if(newdatevalue.contains(",")) {
			newdatevalue = newdatevalue.replace(",", "").replaceAll("\\[", "").replaceAll("\\]", "");
		}*/
		if(newdatevalue.trim().contains("at")){
			newdatevalue = newdatevalue.replace("at", "");
		}
		try {
			d=df.parse(newdatevalue.trim());
			
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		d1 =df.format(d);	
		if(d1.substring(4,5).startsWith("0")){
			d1 =d1.replaceFirst("0", "");
		    
		}
		d2 =dt.format(new Date(newdatevalue)).toLowerCase().replace(":00", "").trim();
		if(d2.substring(0,1).startsWith("0")){
			d2 =d2.replaceFirst("0", "");
		    
		}
		 if(d2.contains("am")) {
			 d2 = d2.replace(" am", "am");
		 } else {
			 d2 =d2.replace(" pm", "pm");
		 }
		 if(d1.contains(getCurrentYear())){
			 d1= d1.replace(getCurrentYear(), "");
		 }
		
		return d1+"at "+ d2;
		
	}
	//Convert date in MM/DD/YY HH:MM AM/PM format
	@SuppressWarnings("deprecation")
	public static String convertDate(String dateTimeValue){
		String dateValue=null;
		if(dateTimeValue.trim().contains("at")){
			dateTimeValue = dateTimeValue.replace("at", "");
		}
		if(dateTimeValue.trim().contains(",")) {
			dateTimeValue = dateTimeValue.replace(",", "").replaceAll("\\[", "").replaceAll("\\]", "");
		}
		
		dateValue =nciadtFormat.format(new Date(dateTimeValue));
		return dateValue;
		
	}
	
	public static String getCurrentYear(){
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		String cYear = String.valueOf(year);
		return cYear;
	}
}
