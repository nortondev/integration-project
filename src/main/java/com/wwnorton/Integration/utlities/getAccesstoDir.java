package com.wwnorton.Integration.utlities;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

public class getAccesstoDir {

	
	public void makeDirectory(File dir){
		String perm = "rwxrwx---";
		try {
		    Set<PosixFilePermission> permissions =PosixFilePermissions.fromString(perm);
		Files.setPosixFilePermissions(dir.toPath(), permissions);
		  } catch (IOException e) {
		    // logger.warning("Can't set permission '" + perm + "' to " +   dir.getPath());
		    e.printStackTrace();
		  }
		
	}
}
