package com.wwnorton.Integration.utlities;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;


public class readDocFile {
	

	@SuppressWarnings({ "rawtypes", "resource" })
	public static void main(String[] args)  {
		String filePath =System.getProperty("user.dir") + "\\Downloads\\AutomationTestNamefiu.docx";
		try {
			File file = new File(filePath);
			FileInputStream fis = new FileInputStream(file.getAbsolutePath());

			XWPFDocument document = new XWPFDocument(OPCPackage.open(fis));
			
			System.out.println(document.getLastParagraph().getParagraphText());
			XWPFWordExtractor extract = new XWPFWordExtractor(document) ;
			extract.getText();
			List pic = document.getAllPictures();
			if(!pic.isEmpty()){
			pic.get(0);
			}
		
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void deleteFile(String TestName){
		String filePath =System.getProperty("user.dir") + "\\Downloads\\"+TestName+".docx";
		File dir = new File(filePath);
		if(dir.isFile()== false) {
			return;
		}
		File file = new File(filePath);
		if(file.delete()) {
			LogUtil.log("Deleted "+file.getName());
			
		} else { 
			LogUtil.log("Deleting is failed ");
		}
		//System.exit(0);
	}
}
