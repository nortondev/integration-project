package com.wwnorton.Integration.Objects;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.utlities.BaseDriver;
import com.wwnorton.Integration.utlities.GetDate;
import com.wwnorton.Integration.utlities.GetRandomId;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;



public class CreateNewStudentSet {
	// com.wwnorton.SW5Automation.utilities.GetDate getsystemdate = new
	// com.wwnorton.SW5Automation.utilities.GetDate();

	WebDriverWait wait;
	WebDriver driver;
	String TitleName;

	@FindBy(how = How.XPATH, using = "div[@class='ui-progressbar-overlay']")
	public WebElement Authorize_overlay;

	@FindBy(how = How.ID, using = "create_class_start_radio_new")
	public WebElement create_New_Student_Set_radioOption;

	@FindBy(how = How.ID, using = "create_class_start_radio_copy")
	public WebElement create_class_start_radio_copy_radioOption;

	@FindBy(how = How.ID, using = "create_class_start_radio_child")
	public WebElement create_class_start_radio_child_radioOption;

	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button/span[contains(text(),'Next')]")
	public WebElement Next_Button;

	@FindBy(how = How.ID, using = "new_student_set_title")
	public WebElement new_student_set_title_textbox;

	@FindBy(how = How.ID, using = "ddlcountry")
	public WebElement countryListBox;
	
	@FindBy(how = How.ID, using = "new_student_set_state")
	public WebElement new_student_set_state_listbox;

	@FindBy(how = How.ID, using = "new_student_set_school_text_input")
	public WebElement new_student_set_school_text_input;

	@FindBy(how = How.ID, using = "new_student_set_start_date")
	public WebElement student_set_start_date;

	@FindBy(how = How.XPATH, using = "//a[@class='ui-state-default ui-state-highlight']")
	public WebElement Student_SelectDate;

	@FindBy(how = How.ID, using = "new_student_set_end_date")
	public WebElement student_set_end_date;

	@FindBy(how = How.XPATH, using = "//div[@id='manage_student_sets_div']/h3")
	public WebElement clickoutside;

	@FindBy(how = How.ID, using = "schoolIDNo")
	public WebElement schoolIDNo;

	@FindBy(how = How.XPATH, using = "//div[@id='add_new_student_set']/span[contains(text(),'Create New Student Set')]")
	public WebElement Create_New_Student_Set_Button;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Create Student Set')]")
	public WebElement Create_Student_Set_Button;

	@FindBy(how = How.XPATH, using = "//div[@class='alert_dialog_class ui-dialog-content ui-widget-content' and contains(text(),'Student Set successfully created. The new Student Set ID is:')]")
	public WebElement Student_alert_Text;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'OK')]/parent::button[@type='button']")
	public WebElement Ok_button;

	@FindBy(how = How.XPATH, using = "//li[@class='ui-menu-item']/a[contains(text(),'Manage Student Sets')]")
	public WebElement ManageStudetSets;

	@FindBy(how = How.ID, using = "gear_button_username")
	public WebElement Sign_in_or_Register;

	@FindBy(how = How.XPATH, using = "//div[@id='add_new_student_set']/span[@class='ui-button-text']")
	public WebElement Create_New_Student_Set_button;

	@FindBy(how = How.XPATH, using = "//button[@id='login_button']")
	WebElement loginButton;

	@FindBy(how = How.XPATH, using = "//*[@id='gear_button_username']")
	public WebElement gear_button_username;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a/b[contains(text(),'Sign Out')]")
	public WebElement SignOut_link;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"report_class_menu\"]")
	public WebElement StudentSet;
	
	@FindBy(how = How.ID, using = "activity_gau_set_datepicker")
	public WebElement gaudatepicker;
	
	@FindBy(how = How.XPATH, using = "//button[@id='settings_apply']/span[@class='ui-button-text'][contains(text(),'Save Settings')]")
	public WebElement SaveSettingsButton;

	@FindBy(how = How.XPATH, using = "//select[@id='activity_gau_set_timezonepicker']/option[contains(text(),'Eastern Time')]")
	public WebElement SelecttimeZone;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@class,'ui-state-default ui-state-highlight')]")
	public WebElement gau_SelectDate;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button/span[contains(.,'Set Grades Accepted Until')]")
	public WebElement setGAUbutton;
	
	@FindBy(how = How.ID, using ="lti_connection_badge")
	public WebElement LMSconnection;
	
	@FindBy(how = How.XPATH, using ="//div[starts-with(@id,'alert_dialog_outer_')]/p")
	public WebElement alertDialogmessage;
	
	@FindBy(how = How.XPATH, using ="//div[starts-with(@class,'ui-dialog-buttonset')]/button/span[contains(.,'Upgrade to Full Access')]")
	public WebElement upgradeFullAccessButton;
	
	@FindBy(how = How.XPATH, using ="//button/span[@class='ui-button-text'][contains(.,'Save')]")
	public WebElement saveButton;

	public CreateNewStudentSet() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();

	@Step("Click Sign_in_or_Register button,  Method: {method} ")
	public void SignIn_Register() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(Sign_in_or_Register));
		Sign_in_or_Register.click();
	}

	@Step("Click on Manage StudentSet Link button,  Method: {method} ")
	public void ManageStudentSetlink() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ManageStudetSets));
		ManageStudetSets.click();
	}

	@Step("Click on Create Student set button,  Method: {method} ")
	public void CreateStudentSetButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(Create_New_Student_Set_button));
		Create_New_Student_Set_button.click();
	}

	// Create a new Student Set from scratch.
	@Step("Select the Create New Student from Scratch radio option and click the Next button,  Method: {method} ")
	public void createNewStudentset() {
		create_New_Student_Set_radioOption.click();
		Next_Button.click();
	}

	// Create a New Student Set General information.
	@Step("Enter or Select the Student General informations and Click the Save button,  Method: {method} ")
	public String createStudentset_information(String CourseName) throws InterruptedException {
		if(CourseName==null){
		String Title = jsonobject.getAsJsonObject("StudentSetGeneralInfo").get("Title").getAsString();
		new_student_set_title_textbox.sendKeys(Title + GetRandomId.randomAlphaNumeric(3).toLowerCase());
		}
		Select ddlCountry = new Select(countryListBox);
		ddlCountry.selectByValue("USA");
		Thread.sleep(2000);
		Select drpCountry = new Select(new_student_set_state_listbox);
		drpCountry.selectByValue("USA_AZ");
		WebDriverWait wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(new_student_set_school_text_input));
		
		//String SchoolName = jsonobject.getAsJsonObject("StudentSetGeneralInfo").get("SchoolName").getAsString();
		new_student_set_school_text_input.sendKeys("a");
		Thread.sleep(3000);
		boolean isListdisplayed = driver.findElements(By.xpath("//div[@id='manage_student_sets_div']/ul[starts-with(@id,'ui-id-')]/li")).size() >0;
		if(isListdisplayed==true){
			List<WebElement> schoolNameList = driver.findElements(By.xpath("//div[@id='manage_student_sets_div']/ul[starts-with(@id,'ui-id-')]/li"));
			for(int i=0; i<schoolNameList.size();){
				schoolNameList.get(0).click();
				break;
			}
		}
		new_student_set_school_text_input.click();
		student_set_start_date.click();
		// Student_SelectDate.click();
		student_set_start_date.sendKeys(GetDate.getCurrentDate());
		student_set_end_date.click();
		// Student_SelectDate.click();
		student_set_end_date.sendKeys(GetDate.getNextmonthDate());
		/*boolean isdisplayed = driver.findElements(By.xpath("//div[@id='manage_student_sets_div']/h3")).size() >0;
		if(isdisplayed==true){
		clickoutside.click();
		} else {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.TAB);
		}*/
		new_student_set_title_textbox.click();
		Thread.sleep(3000);
		List<WebElement> oRadiobuttons = driver.findElements(By.name("schoolID"));
		boolean bvalue = false;
		bvalue = oRadiobuttons.get(0).isSelected();
		if (bvalue == true) {
			oRadiobuttons.get(0).click();
		} else {
			oRadiobuttons.get(1).click();
		}

		Create_Student_Set_Button.click();
		Thread.sleep(3000);
		boolean isSaveButton= driver.findElements(By.xpath("//button/span[@class='ui-button-text'][contains(.,'Save')]")).size()>0;
		if(isSaveButton==true){
		saveButton.click();
		WebElement loaderele = driver.findElement(By.id("loading_screen"));
		ReusableMethods.loadingWaitDisapper(driver, loaderele);
		}
		String titletext = null;
		boolean istitleText = driver.findElements(By.id("new_student_set_title_textbox")).size() >0;
		if(istitleText==true){
		titletext = new_student_set_title_textbox.getText();
		}
		if(titletext==null){
			return null;
		}else {
			return titletext;
		}
		
	}

	@Step("Get the Student ID,  Method: {method} ")
	public String createStudentset_ID() throws Exception {
		String studentsetID = null;
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(Student_alert_Text));
		String str = Student_alert_Text.getText();
		Matcher m = Pattern.compile("\\d+").matcher(str);
		while (m.find()) {
			studentsetID = m.group(0);
		}

		Ok_button.click();

		wait.until(ExpectedConditions.elementToBeClickable(Create_New_Student_Set_Button));

		return studentsetID;
	}

	@Step("Logout Smartwork5,  Method: {method} ")
	public void logoutSmartwork5() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(gear_button_username));
		gear_button_username.click();
		SignOut_link.click();
		wait.until(ExpectedConditions.elementToBeClickable(loginButton));
	}
	@Step("Select New Student Set ID by option Value on SW5 DLP page,  Method: {method} ")
	public String selectByPartOfVisibleText(String value) throws Exception {
		String ssidValue = null;
		List<WebElement> optionElements = StudentSet.findElements(By.tagName("option"));

		for (WebElement optionElement : optionElements) {
			if (optionElement.getText().contains(value)) {
				ssidValue =(optionElement.getText());
				optionElement.click();
				try {
					WebDriverWait wait = new WebDriverWait(driver, 50);
					wait.until(ExpectedConditions.visibilityOfElementLocated(
							By.xpath("//div[@id='activity_list_table_wrapper']/table/tbody")));
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("loading_message")));
				} catch (Exception e) {
					System.out.println("Exception Handlled: " + e.getMessage());
				}
				break;
			}
		}
		return ssidValue;

	}
	
	@Step("Click Gau Date text box on InQuizitive Chapter page,  Method: {method} ")
	public void clickGauDate() throws Exception {
		gaudatepicker.click();
		gaudatepicker.sendKeys(GetDate.getNextweekDate());
		/*JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", gau_SelectDate);*/
		driver.findElement(By.xpath("//div[@id='activity_gau_set_div']/b")).click();
	}

	@Step("Select TimjeZone on InQuizitive Chapter page,  Method: {method} ")
	public void selectTimeZone() throws Exception {
		/*Select oselecttimezoneName = new Select(SelecttimeZone);
		oselecttimezoneName.selectByVisibleText(timezoneName);*/
		//LogUtil.log("The time zone Name " + timezoneName);
		SelecttimeZone.click();
	}
	

	@Step("Click Set Grades Accepted Until Button,  Method: {method} ")
	public void clickGAUButton() throws Exception {
		setGAUbutton.click();
	}
	
	@Step("Click the Assignment link associated with assignmentTitle,  Method: {method} ")
	public void clickAssignmentLink(String assignmentTitle) {
		
		WebElement AssignmentLink = driver.findElement(
				By.xpath("//tr[td/a[contains(text(),'" + assignmentTitle + "')]]/td[@class=' title_td']/a"));
		AssignmentLink.click();
	
	}
	@Step("Click the Connected to LMS button from IQ Page,  Method: {method} ")
	public void clickConnLMSButton() {
		LMSconnection.click();
	}
	
	@Step("Verify You accessed this page via an LTI-enabled link from an LMS texts on Pop up,  Method: {method} ")
	public List<String> verifyLMSText(){
	List<WebElement> lmsElemts=driver.findElements(By.xpath("//div[starts-with(@id,'general_dialog_')]/p[contains(.,'LMS should')]"));
	ArrayList<String> list = new ArrayList<String>();
	for(int i=0; i<lmsElemts.size(); i++){
		String textEle = lmsElemts.get(i).getText();
		list.add(textEle);
	}
	return list;
	}
	
	@Step("Verify trial access text is displayed,  Method: {method} ")
	public String getalertDialog(){
		return alertDialogmessage.getText();
		
	}
	
	@Step("click on upGrade Full access button,  Method: {method} ")
	public void clickUpgradeFullAccess(){
		upgradeFullAccessButton.click();
		
	}
}
