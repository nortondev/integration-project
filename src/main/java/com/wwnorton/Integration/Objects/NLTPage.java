package com.wwnorton.Integration.Objects;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Range;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

import com.wwnorton.Integration.utlities.BaseDriver;
import com.wwnorton.Integration.utlities.GetRandomId;
import com.wwnorton.Integration.utlities.LogUtil;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.GetDate;

public class NLTPage {
	WebDriver driver;
	WebDriverWait wait;
	WebElement calMonthEle;
	String calMonth, numberofdays, currentMonth;
	String cday = null;
	int cMonth;
	int lastmonthday;

	PropertiesFile prop = new PropertiesFile();
	private static SimpleDateFormat dayFormatS = new SimpleDateFormat(
			"EEEE, MMMM d, yyyy");
	private static SimpleDateFormat dayFormatmnth = new SimpleDateFormat("MM");
	private static SimpleDateFormat dayFormatday = new SimpleDateFormat("dd");
	private static SimpleDateFormat dayFormatyear = new SimpleDateFormat("yyyy");

	@FindBy(how = How.XPATH, using = "//div[starts-with(@class,'_LMSConfirmStep_success-box')]/span")
	public WebElement LMStext;

	@FindBy(how = How.XPATH, using = "//input[@id='loginEmail']|//input[@id='txtEmail-input']")
	public WebElement TestMakerLoginEmail;

	@FindBy(how = How.XPATH, using = "//input[@id='loginPassword']|//input[@id='txtpassword-input']")
	public WebElement TestMakerLoginPassword;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement TestMakerLogInButton;

	@FindBy(how = How.XPATH, using = "//h2[@id='nds-modal-2-title']")
	public WebElement authorizeCanvasAccesstitle;

	@FindBy(how = How.ID, using = "state_combobox")
	public WebElement statetextbox;

	@FindBy(how = How.ID, using = "school_name_combobox")
	public WebElement schoolNameTextbox;

	@FindBy(how = How.NAME, using = "course_name")
	public WebElement courseName;

	@FindBy(how = How.NAME, using = "search_by_code.product_code")
	public WebElement productcode;

	@FindBy(how = How.XPATH, using = "//footer[@class='nds-modal__actionbar']/button[@type='button']/span[contains(.,'No, this is not my book')]")
	public WebElement noBookButton;

	@FindBy(how = How.XPATH, using = "//footer[@class='nds-modal__actionbar']/button[@type='submit']/span[contains(.,'Yes, this is my book')]")
	public WebElement yesBookButton;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'courseCreationWizard_footer')]/button[@type='button']/span[contains(.,'Create Course')]")
	public WebElement createCoursebutton;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'courseWizard_container')]//button[@type='button']/span[contains(.,'Select Course Content')]")
	public WebElement selectCoursecontentButton;

	@FindBy(how = How.XPATH, using = "//div[starts-with(@class,'DateInput')]/input[@id='test-date-picker']")
	public WebElement selectDateele;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(.,'Cancel')]")
	public WebElement duedateCancelButton;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(.,'Set Due Date')]")
	public WebElement duedateSetDueDatebutton;

	@FindBy(how = How.XPATH, using = "//button/span[contains(.,'Clear')]")
	public WebElement duedateClearbutton;

	@FindBy(how = How.XPATH, using = "//span[@class='nds-button__text'][contains(.,'Review & Send to LMS')]/parent::button[@type='button']")
	public WebElement reviewSendtoLMS;

	@FindBy(how = How.XPATH, using = "//td[starts-with(@class,'_table_statusTimestamp')]/span[starts-with(.,'Sent')]")
	public WebElement sentDate;

	@FindBy(how = How.XPATH, using = "//button[@aria-haspopup='listbox']/span[starts-with(@id,'nds-dropdown')]")
	public WebElement timesetDueDate;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(.,'Cancel')]")
	public WebElement CancelButton;

	@FindBy(how = How.XPATH, using = "//div[@role='combobox']/input[@id='discipline_combobox']")
	public WebElement disciplineTextbox;

	@FindBy(how = How.XPATH, using = "//div[@role='combobox']/input[@id='book_title_combobox']")
	public WebElement bookTitle;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'courseWizard_container')]//div/h1")
	public WebElement courseTitle;
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'course-details-container')]//div/h5")
	public WebElement courseDetailName;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Enter title code']")
	public WebElement enterTitleCode;

	public NLTPage() {
		this.driver = BaseDriver.getDriver();
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);

	}

	@Step("Verify LMS connection Text,  Method: {method} ")
	public String lmsText() {
		boolean isdisplayed = driver
				.findElements(
						By.xpath("//div[starts-with(@class,'_LMSConfirmStep_success-box')]/span"))
				.size() > 0;
		if (isdisplayed == true) {
		}

		return LMStext.getText().trim();

	}

	@Step("Verify User Name on NLT page, Method: {method} ")
	public String getUserName() {
		String getusernameText = null;
		List<WebElement> spanList = driver.findElements(By
				.xpath("//header[starts-with(@class,'layout_header')]//span"));
		for (int i = 0; i < spanList.size(); i++) {
			getusernameText = spanList.get(1).getText();
		}
		return getusernameText;
	}

	@Step("Instructor Login to NLT Application,  Method: {method} ")
	public void loginTestMakerApp(String userName, String passWord)
			throws InterruptedException {
		ReusableMethods.checkPageIsReady(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//input[@id='loginEmail']|//input[@id='txtEmail-input']")));
		TestMakerLoginEmail.click();
		TestMakerLoginEmail.sendKeys(userName);
		TestMakerLoginPassword.sendKeys(passWord);
		TestMakerLogInButton.click();

	}

	@Step("Verify the paragraph on Authorize Access window,  Method: {method} ")
	public List<String> getparaText() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//section[@class='nds-modal__content']/p")));
		String paraText = null;

		List<WebElement> paraEle = driver.findElements(By
				.xpath("//section[@class='nds-modal__content']/p"));
		List<String> paraTextList = new ArrayList<String>();
		for (int i = 0; i < paraEle.size(); i++) {
			paraText = paraEle.get(i).getText();
			paraTextList.add(paraText);
		}

		return paraTextList;
	}

	@Step("Verify the Cancel Button on Authorize Access window,  Method: {method} ")
	public boolean isCancelButton() {
		boolean iscancelButton = driver
				.findElement(
						By.xpath("//button[@type='button']/span[contains(.,'Cancel')]"))
				.isDisplayed();
		return iscancelButton;

	}

	@Step("Verify the Authorize Button on Authorize Access window,  Method: {method} ")
	public boolean isAuthorizeButton() {
		boolean isAuthButton = driver
				.findElement(
						By.xpath("//input[@type='submit'][@value='Proceed to Authorize']"))
				.isDisplayed();
		return isAuthButton;

	}

	@Step("Click the Authorize Button on Authorize Access window,  Method: {method} ")
	public void clickAuthorizeButton() {
		boolean isAuthButton = driver
				.findElements(
						By.xpath("//input[@type='submit'][@value='Proceed to Authorize']"))
				.size() > 0;
		if (isAuthButton == true) {
			driver.findElement(
					By.xpath("//input[@type='submit'][@value='Proceed to Authorize']"))
					.click();
		}

	}

	@Step("verify login confirmation content text,  Method: {method} ")
	public String verifyLoginConfContent() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='ic-Login-confirmation__content']")));
		WebElement getcontent = driver.findElement(By
				.cssSelector("#modal-box > div > h2"));
		return getcontent.getText();
	}

	@Step("verify login confirmation content Paragraph text,  Method: {method} ")
	public String verifyLoginConfContentPara() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='ic-Login-confirmation__content']")));
		WebElement getcontent = driver.findElement(By
				.cssSelector("#modal-box > div > p:nth-child(2) > strong"));
		return getcontent.getText();
	}

	@Step("verify UserName login confirmation content ,  Method: {method} ")
	public String verifyUserNameLoginConfContent() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='ic-Login-confirmation__content']")));
		WebElement getcontent = driver.findElement(By
				.cssSelector("#modal-box > div > p:nth-child(3) > a"));
		return getcontent.getText();
	}

	@Step("Click Authorize button from login confirmation content ,  Method: {method} ")
	public void clickAuthorizeButtonLoginConfContent() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class='ic-Login-confirmation__content']")));
		WebElement clickAuthEle = driver.findElement(By
				.xpath("//input[@type='submit'][@value='Authorize']"));
		clickAuthEle.click();
	}

	@Step("Click Continue button from LMS Connection page ,  Method: {method} ")
	public void clickContinue() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//button[@type='button']")));
		WebElement clickcontinueEle = driver
				.findElement(By
						.xpath("//button[@type='button']/span[contains(.,'Continue')]|//button[@type='submit']/span[contains(.,'Continue')]"));
		clickcontinueEle.click();
	}

	@Step("Verify Course Details page is displayed ,  Method: {method} ")
	public String getCoursedetailspage() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[@class[contains(.,'courseWizard_container')]]")));
		WebElement stepTitle = driver
				.findElement(By
						.xpath("//h1[contains(@class,'step-title')] | //h2[contains(@class,'step-title')]"));
		return stepTitle.getText();
	}

	@Step("Click Previous button from Course Details page ,  Method: {method} ")
	public void clickPrevious() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//button[@type='button']")));
		WebElement clickpreviousEle = driver
				.findElement(By
						.xpath("//button[@type='button']/span[contains(.,'Previous')]"));
		clickpreviousEle.click();
	}

	@Step("Select State from the list box,  Method: {method} ")
	public void selectState(int statevalue) {
		wait.until(ExpectedConditions.elementToBeClickable(By
				.id("state_combobox")));
		statetextbox.click();
		List<WebElement> options = driver.findElements(By
				.xpath("//ul[@id='state_combobox-popup']/li"));
		for (int i = 0; i < options.size();) {
			options.get(i)
					.findElement(
							By.xpath("//ul[@id='state_combobox-popup']/li[@id='state_combobox-option-"
									+ statevalue + "']")).click();
			break;
		}
	}

	@Step("Select School Name from the list box,  Method: {method} ")
	public void selectSchoolName(int statevalue) {
		schoolNameTextbox.click();
		List<WebElement> options = driver.findElements(By
				.xpath("//ul[@id='school_name_combobox-popup']/li"));
		for (int i = 0; i < options.size();) {
			options.get(i)
					.findElement(
							By.xpath("//ul[@id='school_name_combobox-popup']/li[@id='school_name_combobox-option-"
									+ statevalue + "']")).click();
			break;
		}
	}

	@Step("Retrieve School Name from the list box,  Method: {method} ")
	public String getschoolNameCoursePage() {
		WebElement schoolName = driver.findElement(By
				.id("school_name_combobox"));
		return schoolName.getAttribute("value");
	}

	@Step("Enter Course Name on course details page,  Method: {method} ")
	public void setCourseName(String coursename) {
		courseName.sendKeys(coursename);
	}

	@Step("Verify the the Errors on course details page labelName {0},  Method: {method} ")
	public String validateError(String labelName) {
		WebElement labelEle = driver
				.findElement(By
						.xpath("//label[@for='"
								+ labelName
								+ "']/following-sibling::div[@class='nds-field__feedback']/ul[@class='nds-field__errors']/li|//div/input[@name='"
								+ labelName
								+ "']/parent::div/following-sibling::div[@class='nds-field__feedback']/ul[@class='nds-field__errors']/li|//div[starts-with(@class,'_courseDetailsStep_field-group')]//div[contains(.,'"
								+ labelName
								+ "')]/following-sibling::div[@class='nds-field__feedback']/ul[@class='nds-field__errors']/li"));
		return labelEle.getText();
	}

	@Step("Select the Timezone from the list box,  Method: {method} ")
	public void selectTimeZone(String timezone) {
		WebElement timexoneEle = driver.findElement(By
				.xpath("//input[@id='course_timezone_combobox']"));

		timexoneEle.click();
		List<WebElement> options = driver.findElements(By
				.xpath("//ul[@role='listbox']/li"));
		for (int i = 0; i < options.size();) {
			options.get(i)
					.findElement(
							By.xpath("//ul[@role='listbox']/li[@role='option']/span[contains(.,'"
									+ timezone + "')]")).click();
			break;
		}

	}

	@Step("Reterive the timezone after the selection from list box,  Method: {method} ")
	public String getTimeZone() {
		String timezone;
		WebElement gettimezoneText = driver
				.findElement(By
						.xpath("//div[@class='nds-field__info']/div[contains(.,'Course Timezone')]/parent::div/following-sibling::button/span|//input[@placeholder='Select Timezone']"));
		timezone = gettimezoneText.getAttribute("value");
		return timezone;
	}

	@Step("Select Start date on course details page, Method: {method} ")
	public void selectStartDate() throws Exception {
		String cday = GetDate.getCurrentDate();
		System.out.println(cday);
		@SuppressWarnings("deprecation")
		String currentDay = dayFormatS.format(Date.parse(cday));
		WebElement getDateEle = driver.findElement(By
				.id("course_start_date_picker"));
		getDateEle.click();
		Thread.sleep(2000);
		WebElement selectStartDate = driver
				.findElement(By
						.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
								+ currentDay + "']"));
		selectStartDate.click();
	}

	@Step("Select Start date as OCT 01 on course details page, Method: {method} ")
	public void selectoctStartDate() throws Exception {
		String cday = GetDate.getCurrentDate();
		System.out.println(cday);
		@SuppressWarnings("deprecation")
		String currentDay = dayFormatS.format(Date.parse(cday));
		WebElement getDateEle = driver.findElement(By
				.id("course_start_date_picker"));
		getDateEle.click();
		Thread.sleep(2000);
		WebElement selectStartDate = driver
				.findElement(By
						.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
								+ currentDay + "']"));
		selectStartDate.click();
	}

	@SuppressWarnings("deprecation")
	@Step("Select End date on course details page, Method: {method} ")
	public void selectEndDate() throws Exception {
		String getmonth = GetDate.getCurrentDate();
		String currentMonth = dayFormatmnth.format(Date.parse(getmonth));
		String getday = dayFormatday.format(Date.parse(getmonth));
		int cDay = Integer.parseInt(getday);
		int lastmonthday = GetDate.lastdayoftheMonth();
		int cMonth = Integer.parseInt(currentMonth);
		String numberofdays = GetDate.addDays(15);

		//
		String nextMonthDate = GetDate.getNextmonthDate();
		String nextMonth = dayFormatmnth.format(Date.parse(nextMonthDate));
		int nMonth = Integer.parseInt(nextMonth);
		if (cDay == lastmonthday) {
			if (nMonth > cMonth) {
				WebElement getDateEle = driver.findElement(By
						.id("course_end_date_picker"));
				getDateEle.click();
				// driver.findElement(By.xpath("//div[@class='nav-arrow next']")).click();
				Thread.sleep(2000);
				// Click Arrow icon.
				String cday = dayFormatS.format(Date.parse(numberofdays));
				Thread.sleep(2000);
				// Split String

				WebElement selectEndDate = driver
						.findElement(By
								.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
										+ cday + "']"));
				selectEndDate.click();
			}
		} else if (cDay >= 16) {
			WebElement getDateEle = driver.findElement(By
					.id("course_end_date_picker"));
			getDateEle.click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//div[@class='nav-arrow next']"))
					.click();
			Thread.sleep(2000);
			// Click Arrow icon.
			String cday = dayFormatS.format(Date.parse(numberofdays));
			Thread.sleep(2000);
			// Split String

			WebElement selectEndDate = driver
					.findElement(By
							.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
									+ cday + "']"));
			selectEndDate.click();
		} else {
			String cday = dayFormatS.format(Date.parse(numberofdays));
			WebElement getDateEle = driver.findElement(By
					.id("course_end_date_picker"));
			getDateEle.click();
			Thread.sleep(2000);
			WebElement selectEndDate = driver
					.findElement(By
							.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
									+ cday + "']"));
			selectEndDate.click();

		}
	}

	@SuppressWarnings("deprecation")
	@Step("Select End date on course details page, Method: {method} ")
	public void selectDaylightEndDate() throws Exception {
		
		WebElement calMonthEle;
		String calMonth;
		String cday;
		String getmonth = GetDate.getCurrentDate();
		String currentMonth = dayFormatmnth.format(Date.parse(getmonth));
		String getday = dayFormatday.format(Date.parse(getmonth));

		int cDay = Integer.parseInt(getday);
		LogUtil.log(cDay);
		int lastmonthday = GetDate.lastdayoftheMonth();
		LogUtil.log(lastmonthday);
		int cMonth = Integer.parseInt(currentMonth);
		LogUtil.log(cMonth);

		WebElement getDateEle = driver.findElement(By
				.id("course_end_date_picker"));
		getDateEle.click();
		// driver.findElement(By.xpath("//div[@class='nav-arrow next']")).click();
		Thread.sleep(2000);
		// Click Arrow icon.

		if (cMonth == 11 && cDay <= 7) {
			String numberofdays = GetDate.addDays(15);
			cday = dayFormatS.format(Date.parse(numberofdays));
			WebElement selectEndDate = driver
					.findElement(By
							.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
									+ cday + "']"));
			selectEndDate.click();

		} else {
			String numberofdays = GetDate.addDays(380);
			cday = dayFormatS.format(Date.parse(numberofdays));
			String nextyrMonthdigits = dayFormatmnth.format(Date.parse(cday));
			String nextyear = dayFormatyear.format(Date.parse(cday));
			String nextyrMonth = ReusableMethods.formatMonth(nextyrMonthdigits);
			String getNextMthYear = nextyrMonth + " ".concat(nextyear);
			calMonthEle = driver
					.findElement(By
							.xpath("//div[@data-visible='true']/div[contains(@class,'CalendarMonth_caption CalendarMonth')]/strong"));
			calMonth = calMonthEle.getText();
			while (!getNextMthYear.equals(calMonth)) {
				driver.findElement(By.xpath("//div[@class='nav-arrow next']"))
						.click();
				Thread.sleep(5000);
				calMonthEle = driver
						.findElement(By
								.xpath("//div[@data-visible='true']/div[contains(@class,'CalendarMonth_caption CalendarMonth')]/strong"));
				calMonth = calMonthEle.getText();
			}

			WebElement selectEndDate = driver
					.findElement(By
							.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
									+ cday + "']"));
			selectEndDate.click();
		}
		// Split cday String

	}

	@Step("Retrieve Start date from the Enter Course details page,  Method: {method} ")
	public String getstartdateCoursePage() {
		WebElement startDate = driver.findElement(By
				.id("course_start_date_picker"));
		return startDate.getAttribute("value");
	}

	@Step("Retrieve End date from the Enter Course details page,  Method: {method} ")
	public String getEnddateCoursePage() {
		WebElement endDate = driver
				.findElement(By.id("course_end_date_picker"));
		return endDate.getAttribute("value");
	}

	@Step("Verify Select Course Material Label is displayed as Select Course Material page, Method: {method} ")
	public String getCourseMaterialText() {
		WebElement courseMatText = driver.findElement(By
				.xpath("//h2[contains(@class,'step-title')]"));
		return courseMatText.getText();
	}

	@Step("Select and Enter Product Code on Select Course Material page, Method: {method} ")
	public void selectproductCooderadioOption() {
		boolean selectCodeRadio = driver
				.findElements(
						By.xpath("//input[@type='radio'][@value='search_by_code']/parent::div[contains(@class,'checked')]"))
				.size() > 0;
		
		if (!selectCodeRadio == true) {
			WebElement element = driver.findElement(By
					.xpath("//input[@type='radio'][@value='search_by_code']"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		}
	}
	
	@Step("Enter Title Code on Select Course Material page, Method: {method} ")
	public void enterTitleCode(String titleCode) {
		enterTitleCode.sendKeys(titleCode);
	}

	@Step("Search By Book Title Select Course Material page, Method: {method} ")
	public void selectSearchByBookTitleRadioOption() {
		boolean selectCodeRadio = driver
				.findElements(
						By.xpath("//input[@type='radio'][@value='search_by_title']/parent::div[contains(@class,'checked')]"))
				.size() > 0;
		;
		if (!selectCodeRadio == true) {
			WebElement element = driver.findElement(By
					.xpath("//input[@type='radio'][@value='search_by_title']"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		}
	}

	@Step("Select the Discipline from Select Course Material page, Method: {method} ")
	public void selectDiscipline(String disName) {
		disciplineTextbox.click();
		List<WebElement> options = driver.findElements(By
				.xpath("//ul[@role='listbox']/li[@role='option']"));
		for (int i = 0; i < options.size();) {
			options.get(i)
					.findElement(
							By.xpath("//ul[@role='listbox']/li[@role='option']/span[contains(.,'"
									+ disName + "')]")).click();
			break;
		}

	}

	@Step("Select the Book Title from Select Course Material page, Method: {method} ")
	public void selectBookTitle(String text) {
		bookTitle.click();
		List<WebElement> options = driver.findElements(By
				.xpath("//ul[@role='listbox']/li[@role='option']"));
		for (int i = 0; i < options.size();) {
			options.get(i)
					.findElement(
							By.xpath("//ul[@role='listbox']/li[@role='option']/span[contains(.,'"
									+ text + "')]")).click();
			break;
		}
	}

	@Step("Select the Edition from Select Course Material page, Method: {method} ")
	public void selectEdition(String edition) {
		driver.findElement(
				By.xpath("//div[@role='grid']/button//div[contains(@class,'_styles_product-details')]/span[contains(.,'"+edition+"')]"))
				.click();
	}

	@Step("Select Product code from Select Course Material page, Method: {method} ")
	public void clickTitleCodelink() throws InterruptedException {
		WebElement courseMatText = driver
				.findElement(By.linkText("title code"));
		courseMatText.click();
		String tcode = PropertiesFile.gettitleCode();
		ReusableMethods.switchToNewWindow(3, driver);
		ReusableMethods.checkPageIsReady(driver);
		boolean isbannerText = driver.findElements(
				By.xpath("//div[@class='banner-text']")).size() > 0;
		if (isbannerText == true) {
			WebElement bannerClose = driver
					.findElement(By
							.xpath("//div[@class='banner-close']/span/*[name()='svg' and @class='svg-search-clear']"));
			bannerClose.click();
		}
		Thread.sleep(2000);
		boolean istCodeDisplayed = driver
				.findElements(
						By.xpath("//div[@data-id='panelComponentParent']//div[@data-module='header']/div[@class='headerDefault ']/h3[contains(.,'Music')]/following::button[contains(@target-url,'"
								+ tcode.toLowerCase() + "')]")).size() > 0;
		if (istCodeDisplayed == true) {
			WebElement selecttitlecode = driver
					.findElement(By
							.xpath("//div[@data-id='panelComponentParent']//div[@data-module='header']/div[@class='headerDefault ']/h3[contains(.,'Music')]/following::button[contains(@target-url,'"
									+ tcode.toLowerCase() + "')]"));
			selecttitlecode.click();
		}

	}

	@Step("get the Title code Find Your Title Code page, Method: {method} ")
	public String getTitleCode() {
		String tcode = PropertiesFile.getwwnortontitleCode();
		boolean istcode = driver
				.findElements(
						By.xpath("//div[@class=' data-component HeaderCopy ']/span/p/strong[contains(.,'"
								+ tcode + "')]")).size() > 0;
		if (istcode == true) {
			WebElement getTCode = driver
					.findElement(By
							.xpath("//div[@class=' data-component HeaderCopy ']/span/p/strong[contains(.,'"
									+ tcode + "')]"));
			return getTCode.getAttribute("innerText");
		} else {
			return tcode;
		}

	}

	@Step("Enter Product code from Select Course Material page, Method: {method} ")
	public void setProductCode(String tcode) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.name("search_by_code.product_code")));
		productcode.click();
		productcode.sendKeys(tcode);
	}

	// confirm Book Window

	@Step("Verify confirm Book pop up is displayed, Method: {method} ")
	public String getconfirmBookpopup() {
		WebElement confirmBookpopup = driver.findElement(By
				.xpath("//h2[@class='nds-modal__title']"));
		return confirmBookpopup.getText();
	}

	@Step("Click the No, this is not my book button , Method: {method} ")
	public void clickNobook() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//h2[@class='nds-modal__title']")));
		noBookButton.click();
	}

	@Step("Click the Yes, this is  my book button , Method: {method} ")
	public void clickYesbook() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//h2[@class='nds-modal__title']")));
		yesBookButton.click();
	}

	@Step("Connected to Canvas text is displayed on Confirm Course Information, Method: {method} ")
	public String connCanvasText() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//main[contains(@class,'layout_body')]")));
		WebElement conncanvas = driver
				.findElement(By
						.xpath("//div[contains(@class,'summaryStep_container')]//div[contains(@class,'card_body')]/span[contains(.,' Connected to Canvas')]"));
		return conncanvas.getText();
	}

	@Step("verify Course Name on Confirm Course Information page, Method: {method} ")
	public String getCourseName() {
		List<WebElement> courseName = driver
				.findElements(By
						.xpath("//div[contains(@class,'card_body')]/label[contains(.,'Course Name:')]/following-sibling::span"));
		return courseName.get(0).getText();
	}

	@Step("verify School Name on Confirm Course Information page, Method: {method} ")
	public String getSchoolName() {
		List<WebElement> schoolName = driver
				.findElements(By
						.xpath("//div[contains(@class,'card_body')]/label[contains(.,'Course Name:')]/following-sibling::span"));
		return schoolName.get(1).getText();
	}

	@Step("verify TimeZone on Confirm Course Information page, Method: {method} ")
	public String getTimezoneText() {
		List<WebElement> timezone = driver
				.findElements(By
						.xpath("//div[contains(@class,'card_body')]/label[contains(.,'Course Name:')]/following-sibling::span"));
		return timezone.get(2).getText();
	}

	// Store in Maps
	public String getCourseDetails(String labelName) {
		String getLabelName;
		String getLabeltext = null;
		List<WebElement> labelNameEle = driver
				.findElements(By
						.xpath("//div[contains(@class,'card_body')]/dl/dt[contains(@class,'_styles_form-')]"));
		List<WebElement> labelTextEle = driver
				.findElements(By
						.xpath("//div[contains(@class,'card_body')]/span[contains(@class,'_styles_form-value__')] | //div[contains(@class,'card_body')]/dl/dd[contains(@class,'_styles_form-')]"));
		for (int i = 0; i < labelNameEle.size() && i < labelTextEle.size(); i++) {
			getLabelName = labelNameEle.get(i).getText();
			if (getLabelName.equals(labelName)) {
				getLabeltext = labelTextEle.get(i).getText();
				System.out.println(getLabeltext);
				break;
			}
		}

		return getLabeltext;

	}

	@Step("verify start date on Confirm Course Information page, Method: {method} ")
	public String getstartDate() {
		List<WebElement> startDate = driver
				.findElements(By
						.xpath("//div[contains(@class,'card_body')]/label[contains(.,'Course Name:')]/following-sibling::span"));
		return startDate.get(3).getText();
	}

	@Step("verify end date on Confirm Course Information page, Method: {method} ")
	public String getendDate() {
		List<WebElement> endDate = driver
				.findElements(By
						.xpath("//div[contains(@class,'card_body')]/label[contains(.,'Course Name:')]/following-sibling::span"));
		return endDate.get(4).getText();
	}

	@Step("verify Title Code on Confirm Course Information page, Method: {method} ")
	public String gettitleCodeCourseinfopage() {
		WebElement titleCode = driver
				.findElement(By
						.xpath("//div[contains(@class,'card_body')]/label[contains(.,'Title code:')]/following-sibling::span"));
		return titleCode.getText();
	}

	@Step("Click the Create Course button from Confirm Course Information page, Method: {method} ")
	public void clickCreateCourseButton() {
		createCoursebutton.click();
	}

	@Step("Verify Course Created text is displayed, Method: {method} ")
	public String getCourseCreatedtext() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[contains(@class,'courseWizard_container')]")));
		return courseTitle.getText();
	}

	@Step("Click the Select Course Content button from Your course has been created page, Method: {method} ")
	public void clickSelectCourseContentButton() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[contains(@class,'courseWizard_container')]")));
		selectCoursecontentButton.click();
	}

	@Step("Verify Select Course Content for Your LMS is displayed, Method: {method} ")
	public String getCoursecontentLMStext() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//div[contains(@class,'assignmentPicker')]/h1")));
		WebElement coursecontentLMSEle = driver.findElement(By
				.xpath("//div[contains(@class,'assignmentPicker')]/h1"));
		return coursecontentLMSEle.getText();
	}

	// All accordions are in collapse state at first
	@Step("Verify All accordions are in collapse state, Method: {method} ")
	public boolean accordionsCollapseState() {
		List<WebElement> collapseState = driver
				.findElements(By
						.xpath("//div[@id='accordion-group_assignment-groups-accordion']/details[not(@open)]"));
		for (WebElement cState : collapseState) {
			if (cState
					.findElement(
							By.xpath("//div[@id='accordion-group_assignment-groups-accordion']/details[not(@open)]"))
					.isDisplayed())
				;
		}
		return true;

	}

	// The name of the accordions should be same as in DLP
	@Step("Verify The name of the accordions should be same as in DLP , Method: {method} ")
	public String accordionsName(String tileName) {
		String titleName = null;
		List<WebElement> accordiansName = driver.findElements(By
				.xpath("//div[@class='nds-disclosure__title']/h5[contains(.,'"
						+ tileName + "')]"));
		for (int i = 0; i < accordiansName.size(); i++) {
			titleName = accordiansName.get(i).getText();

		}
		return titleName;

	}

	@Step("Verify The name of the accordions should be same as in DLP , Method: {method} ")
	public List<String> accordionsNameList() {
		ArrayList<String> nameList = new ArrayList<String>();
		String titleName = null;
		List<WebElement> accordiansNameList = driver.findElements(By
				.xpath("//div[@class='nds-disclosure__title']/h5"));
		for (WebElement accdNameEle : accordiansNameList) {
			titleName = accdNameEle.getText();
			nameList.add(titleName);
		}
		return nameList;

	}

	// Click the Expand button
	@Step("Click the Expand button from Select Course Content for Your LMS , Method: {method} ")
	public void clickExpandButton(String tileName) {
		WebElement expandButton = driver
				.findElement(By
						.xpath("//summary[@class='nds-disclosure__summary']//div[@class='nds-disclosure__title']/h5[contains(.,'"
								+ tileName + "')]"));
		ReusableMethods.scrollIntoView(driver, expandButton);
		expandButton.click();
	}

	// Click the Expand button
	@Step("Click the Expand button from Select Course Content for Your LMS , Method: {method} ")
	public void clickAccdExpandButton(String tileName) {
		WebElement expandButton = driver
				.findElement(By
						.xpath("//summary[@class='nds-disclosure__summary']//div[@class='nds-disclosure__title']/h5[text()='"
								+ tileName + "']"));
		ReusableMethods.scrollIntoView(driver, expandButton);
		expandButton.click();

	}

	@Step("get the eBook Chapter list from NLT Page,  Method: {method} ")
	public List<String> getNLTEbookChapterlist() {
		String chapters;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//summary[@class='nds-disclosure__summary']//div/h5[starts-with(.,'Ebook')]")));
		List<WebElement> getcList = driver
				.findElements(By
						.xpath("//summary[@class='nds-disclosure__summary']//div/h5[starts-with(.,'Ebook')]/following::table/tbody/tr/td[contains(@class,'_table_table-data__')]/div/button"));
		List<String> chapList = new ArrayList<String>();
		for (int i = 0; i < getcList.size(); i++) {
			chapters = getcList.get(i).getText();
			chapList.add(chapters);

		}
		return chapList;

	}

	@Step("get the Tile Name tilename{0} Chapter list from NLT Page,  Method: {method} ")
	public List<String> getNLTChapterlist(String tileName) {
		String chapters;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//summary[@class='nds-disclosure__summary']//div/h5[starts-with(.,'Ebook')]")));
		List<WebElement> getcList = driver
				.findElements(By
						.xpath("//summary[@class='nds-disclosure__summary']//div/h5[contains(.,'"
								+ tileName
								+ "')]/following::div[@class='nds-disclosure__contents-outer']//tbody/tr/td[contains(@class,'_table_table-data__')]/div/span"));
		List<String> chapList = new ArrayList<String>();
		for (int i = 0; i < getcList.size(); i++) {
			chapters = getcList.get(i).getText();
			chapList.add(chapters);

		}
		return chapList;

	}

	// Select Checkboxes
	@Step("select the Check boxes for the accordian,  Method: {method} ")
	public void selectCheckBoxes(int count, String acdName) {
		List<WebElement> chkboxesList = driver
				.findElements(By
						.xpath("//details/summary//div[@class='nds-disclosure__title']/h5[contains(.,'"
								+ acdName
								+ "')]/following::table/tbody/tr/td/div//*[name()='svg' and @class='nds-icon nds-icon--check']"));
		for (int i = 0; i < chkboxesList.size(); i++) {
			if (i <= count) {
				chkboxesList.get(count).click();
				break;
			} else if (count == -1) {
				chkboxesList.get(i).click();
			}
		}

	}

	@Step("get the count of check boxes after slection for the accordian,  Method: {method} ")
	public int getCountCheckboxes(String acdName) {
		List<WebElement> chkboxesList = driver
				.findElements(By
						.xpath("//div[@class='nds-disclosure__title']/h5[contains(.,'"
								+ acdName
								+ "')]/following::table/tbody/tr/td/div[contains(@class,'checked')]"));
		return chkboxesList.size();

	}

	@Step("get the count of  Check boxes for the accordian,  Method: {method} ")
	public int getCheckBoxesCount(String acdName) {
		String badgeCount;
		WebElement badgeCountEle = driver.findElement(By
				.xpath("//div[@class='nds-disclosure__title']/h5[contains(.,'"
						+ acdName
						+ "')]/following-sibling::div[@class='row']/div"));
		badgeCount = badgeCountEle.getText();
		return Integer.parseInt(badgeCount);

	}

	@Step("verify checkbox is selected for the accordian,  Method: {method} ")
	public boolean isCheckBoxSelected(int count, String acdName) {
		List<WebElement> chkboxesList = driver
				.findElements(By
						.xpath("//div[@class='nds-disclosure__title']/h5[contains(.,'"
								+ acdName
								+ "')]/following::table/tbody/tr/td/div/input[@type='checkbox']"));
		for (int i = 0; i < chkboxesList.size(); i++) {
			if (i <= count) {
				String getcheckvalue = null;
				getcheckvalue = chkboxesList.get(i).getAttribute("checked");
				if (getcheckvalue == null) {
					return false;
				}
				if (!getcheckvalue.equalsIgnoreCase("true")) {
					return false;
				}
			}
		}

		return true;

	}

	@Step("Verify Assignment Name check box is focusable after the selecting the checkbox,  Method: {method} ")
	public boolean isAssignmentNameChboxFocus(String acdName) {
		boolean isAssNameChkboxFocus = driver
				.findElements(
						By.xpath("//div[@class='nds-disclosure__title']/h5[contains(.,'"
								+ acdName
								+ "')]/following::label/*[name()='svg' and @class='nds-icon nds-icon--minus']"))
				.size() > 0;
		if (isAssNameChkboxFocus == true)
			;
		return true;

	}

	@Step("Click Assignment Name check box and Verify All Assignemnts are checked,  Method: {method} ")
	public void clickAssignmentNameChkbox() {
		WebElement AssignmentChbox = driver
				.findElement(By
						.xpath("//button[starts-with(.,'Assignment Name')]/preceding::label/*[name()='svg']"));
		AssignmentChbox.click();
	}

	@SuppressWarnings({ "deprecation" })
	@Step("Select a Due Date for the accordian accdName{0},  Method: {method} ")
	public void selectDueDate(int count) throws InterruptedException {
		SimpleDateFormat dayFormatmnth = new SimpleDateFormat("MM");
		SimpleDateFormat dayFormatday = new SimpleDateFormat("dd");
		SimpleDateFormat dayFormatS = new SimpleDateFormat(
				"EEEE, MMMM d, yyyy");
		String getmonth = GetDate.getCurrentDate();
		String currentMonth = dayFormatmnth
				.format(Date.parse(getmonth));
		int cMonth = Integer.parseInt(currentMonth);
		String getday = dayFormatday.format(Date.parse(getmonth));
		int cDay = Integer.parseInt(getday);
		int lastmonthday = GetDate.lastdayoftheMonth();
		String numberofdays = GetDate.addDays(15);
		String nextMonthDate = GetDate.getNextmonthDate();
		String nextMonth = dayFormatmnth.format(Date
				.parse(nextMonthDate));
		int nMonth = Integer.parseInt(nextMonth);
		List<WebElement> dueDateList = driver
				.findElements(By
						.xpath("//button[starts-with(@class,'_modalDateTimeCell_input__')]"));
		for (int i = 0; i < dueDateList.size(); i++) {
			if (i == count) {
				dueDateList.get(i).click();
				Thread.sleep(2000);
				selectDateele.click();			
				if (cDay == lastmonthday) {
					if (nMonth > cMonth) {
						String cday = dayFormatS.format(Date
								.parse(numberofdays));
						Thread.sleep(2000);
						driver.findElement(
								By.xpath("//div[@class='nav-arrow next']"))
								.click();
						Thread.sleep(2000);
						WebElement selectDueDate = driver
								.findElement(By
										.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][contains(@aria-label,'"
												+ cday + "')]"));
						selectDueDate.click();
						
						
					}
				} else if (cDay >= 16) {
					// Click Arrow icon.
					String cday = dayFormatS.format(Date.parse(numberofdays));
					Thread.sleep(2000);
					driver.findElement(
							By.xpath("//div[@class='nav-arrow next']")).click();
					Thread.sleep(2000);
					WebElement selectDueDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][contains(@aria-label,'"
											+ cday + "')]"));
					selectDueDate.click();					
				} else {
					String cday = dayFormatS.format(Date.parse(numberofdays));
					Thread.sleep(2000);
					WebElement selectDueDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][contains(@aria-label,'"
											+ cday + "')]"));
					selectDueDate.click();
							
				} 
			} else if(count>=i){
				if(i==count){
					break;
				}
				dueDateList.get(i).click();
				
				Thread.sleep(2000);
				selectDateele.click();				
				if (cDay == lastmonthday) {
					if (nMonth > cMonth) {
						String cday = dayFormatS.format(Date
								.parse(numberofdays));
						Thread.sleep(2000);
						driver.findElement(
								By.xpath("//div[@class='nav-arrow next']"))
								.click();
						Thread.sleep(2000);
						WebElement selectDueDate = driver
								.findElement(By
										.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][contains(@aria-label,'"
												+ cday + "')]"));
						selectDueDate.click();
						duedateSetDueDatebutton.click();
					}
				} else if (cDay >= 16) {
					// Click Arrow icon.
					String cday = dayFormatS.format(Date.parse(numberofdays));
					Thread.sleep(2000);
					driver.findElement(
							By.xpath("//div[@class='nav-arrow next']")).click();
					Thread.sleep(2000);
					WebElement selectDueDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][contains(@aria-label,'"
											+ cday + "')]"));
					selectDueDate.click();
					duedateSetDueDatebutton.click();
				} else {
					String cday = dayFormatS.format(Date.parse(numberofdays));
					Thread.sleep(2000);
					WebElement selectDueDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][contains(@aria-label,'"
											+ cday + "')]"));
					selectDueDate.click();
					duedateSetDueDatebutton.click();
				}
			}
			else if (count == -1) {
				dueDateList.get(i).click();
				Thread.sleep(2000);
				selectDateele.click();				
				if (cDay == lastmonthday) {
					if (nMonth > cMonth) {
						String cday = dayFormatS.format(Date
								.parse(numberofdays));
						Thread.sleep(2000);
						driver.findElement(
								By.xpath("//div[@class='nav-arrow next']"))
								.click();
						Thread.sleep(2000);
						WebElement selectDueDate = driver
								.findElement(By
										.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][contains(@aria-label,'"
												+ cday + "')]"));
						selectDueDate.click();
						duedateSetDueDatebutton.click();
					}
				} else if (cDay >= 16) {
					// Click Arrow icon.
					String cday = dayFormatS.format(Date.parse(numberofdays));
					Thread.sleep(2000);
					driver.findElement(
							By.xpath("//div[@class='nav-arrow next']")).click();
					Thread.sleep(2000);
					WebElement selectDueDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][contains(@aria-label,'"
											+ cday + "')]"));
					selectDueDate.click();
					duedateSetDueDatebutton.click();
				} else {
					String cday = dayFormatS.format(Date.parse(numberofdays));
					Thread.sleep(2000);
					WebElement selectDueDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][contains(@aria-label,'"
											+ cday + "')]"));
					selectDueDate.click();
					duedateSetDueDatebutton.click();
				}
			}
		}

	}

	@Step("Click Cancel Button for the Set Due date,  Method: {method} ")
	public void clickDueDateCancel() {
		duedateCancelButton.click();
	}

	@Step("Click Set Due date Button for the Set Due date pop up,  Method: {method} ")
	public void clickSetDueDatebutton() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		duedateSetDueDatebutton.click();
	}

	@Step("Get the date value after unchecking the Check box,  Method: {method} ")
	public String getDateValue() {
		String getDatetimeValue = null;
		ArrayList<String> dateTime = new ArrayList<String>();
		List<WebElement> getDateTextlist = driver
				.findElements(By
						.xpath("//button[starts-with(@class,'_modalDateTimeCell_input')]/div/span[string-length(text())>0][not(contains(@class,'_modalDateTimeCell_at-text'))]"));
		for (int d = 0; d < getDateTextlist.size(); d++) {
			getDatetimeValue = getDateTextlist.get(d).getText();
			String date =getDatetimeValue.replace(",", "");
			dateTime.add(date);
			
		}
		return dateTime.toString();
	}
	@Step("Get the date value after unchecking the Check box,  Method: {method} ")
	public String getDateValue(int size1, int size2) {
		Range<Integer> r = Range.between(size1, size2);
		String getDatetimeValue = null;
		ArrayList<String> dateTime = new ArrayList<String>();
		List<WebElement> getDateTextlist = driver
				.findElements(By
						.xpath("//button[starts-with(@class,'_modalDateTimeCell_input')]/div/span[string-length(text())>0][not(contains(@class,'_modalDateTimeCell_at-text'))]"));
		for (int d = 0; d < getDateTextlist.size(); d++) {
			if(r.contains(d)){
			getDatetimeValue = getDateTextlist.get(d).getText();
			String date =getDatetimeValue.replace(",", "");
			dateTime.add(date);
			}
			
		}
		return dateTime.toString();
	}
	@Step("Get the Chapter based date value ,  Method: {method} ")
	public String getChapterBasedDateValue(String chapterName) {
		String getDatetimeValue = null;
		ArrayList<String> dateTime = new ArrayList<String>();
		List<WebElement> getDateTextlist = driver
				.findElements(By
						.xpath("//tbody/tr/td/div/button[contains(.,'"
								+ chapterName
								+ "')]/following::td[contains(@class,'due_date')]//button[starts-with(@class,'_modalDateTimeCell_input')]/div/span[string-length(text())>0][not(contains(@class,'_modalDateTimeCell_at-text'))]"));
		for (int d = 0; d < getDateTextlist.size(); d++) {

			getDatetimeValue = getDateTextlist.get(d).getText();
			dateTime.add(getDatetimeValue.replace(",", ""));
			if (d >= 1) {
				break;
			}
		}
		return dateTime.toString();
	}

	@Step("Click the Claer Button from Due date section,  Method: {method} ")
	public void clickClearButton() {
		WebElement getDateText = driver
				.findElement(By
						.xpath("//button[starts-with(@class,'_modalDateTimeCell_input')]"));
		getDateText.click();
		duedateClearbutton.click();
		duedateSetDueDatebutton.click();
	}

	@Step("Enter a value in LMS Point Value field and verify Check box gets selected for the accordian accdName{0},  Method: {method} ")
	public void selectLMSpointValue(int count) throws InterruptedException {
		List<WebElement> lmsPVele = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'_LMSPointCell_container')]/input[@type='text']"));
		for (int i = 0; i < lmsPVele.size(); i++) {
			if (i <= count) {
				lmsPVele.get(count).clear();
				int getLMSvale = GetRandomId.getRandomLMSPV();
				lmsPVele.get(count).sendKeys(Integer.toString(getLMSvale));
			} else if (count == -1) {
				lmsPVele.get(i).clear();
				int getLMSvale = GetRandomId.getRandomLMSPV();
				lmsPVele.get(i).sendKeys(Integer.toString(getLMSvale));
			}
		}
	}

	@Step("Enter a incorrect value in LMS Point Value field and verify Error Message accdName{0},  Method: {method} ")
	public String geterrorLMSpointValue(int count, String invalidData)
			throws InterruptedException {
		String errormessage = null;
		List<WebElement> lmsPVele = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'_LMSPointCell_container')]/input[@type='text']"));
		for (int i = 0; i < lmsPVele.size(); i++) {
			if (i <= count) {
				lmsPVele.get(count).clear();
				lmsPVele.get(count).sendKeys(invalidData);
				WebElement errorEle = driver
						.findElement(By
								.xpath("//div[contains(@class,'_LMSPointCell_invalid')]"));
				errormessage = errorEle.getText();
			}
		}
		return errormessage;
	}

	@Step("Enter a boundary values and verify  LMS Point Value field Set to 999 accdName{0},  Method: {method} ")
	public String setboundayLMSpointValue(int count, int boundaryValue)
			throws InterruptedException {
		String defaultvalue = null;
		List<WebElement> lmsPVele = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'_LMSPointCell_container')]/input[@type='text']"));
		for (int i = 0; i < lmsPVele.size(); i++) {
			if (i <= count) {
				lmsPVele.get(count).clear();
				lmsPVele.get(count).sendKeys(Integer.toString(boundaryValue));
				ReusableMethods.clickTAB(driver);
				defaultvalue = lmsPVele.get(count).getAttribute("value");
				break;
			}
		}
		return defaultvalue;
	}

	@Step("get LMS Point Value field Set to 999 accdName{0},  Method: {method} ")
	public String getLMSpointValue(int count) throws InterruptedException {
		String defaultvalue = null;
		List<WebElement> lmsPVele = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'_LMSPointCell_container')]/input[@type='text']"));
		for (int i = 0; i < lmsPVele.size(); i++) {
			if (i == count) {
				defaultvalue = lmsPVele.get(count).getAttribute("value");
				break;
			}
		}
		return defaultvalue;
	}

	@Step("Verify Review and Send to LMS button is disabled,  Method: {method} ")
	public boolean reviewSendToLMS() {
		boolean isEnabled = reviewSendtoLMS.isEnabled();
		if (isEnabled == true) {
			return true;
		}
		return false;

	}

	@Step("Click Review Send to LMS button,  Method: {method} ")
	public void clickreviewSendToLMS() {
		reviewSendtoLMS.click();

	}

	// get the list of accordion
	@Step("Select the Checkboxes for each accordion on Assignment picker page,  Method: {method} ")
	public void clickCheckboxes_accordion() throws InterruptedException {
		List<WebElement> accordionList = driver
				.findElements(By
						.xpath("//div[@id='accordion-group_assignment-groups-accordion']//div[@class='nds-disclosure__title']/h5"));
		for (int i = 0; i < accordionList.size(); i++) {
			Thread.sleep(2000);
			accordionList.get(i).click();
			boolean ischeckbox = driver
					.findElements(
							By.xpath("//table/tbody/tr/td/div/input[@type='checkbox']"))
					.size() > 0;
			if (ischeckbox == true) {
				List<WebElement> chkboxesList = driver
						.findElements(By
								.xpath("//table/tbody/tr/td/div//*[name()='svg' and @class='nds-icon nds-icon--check']"));
				int getcheckboxcount = chkboxesList.size();
				if (getcheckboxcount > 1) {
					int count = GetRandomId.getRandom(10);
					for (int j = 0; j < count; j++) {
						chkboxesList.get(j).click();
					}
					accordionList.get(i).click();
				} else {
					accordionList.get(i).click();
				}
			} else {
				accordionList.get(i).click();
			}
		}
	}

	// Store the Accordion Name and Values in Hash Table
	@Step("Get the Assignment selected values for the accordion from Assignment picker page,  Method: {method} ")
	public LinkedHashMap<String, Integer> getAccordionValues() {
		String accodName = null;
		String accodValues = null;
		LinkedHashMap<String, Integer> accordionValues = new LinkedHashMap<String, Integer>();

		// List<WebElement> accordionName
		// =driver.findElements(By.xpath("//div[@id='accordion-group_assignment-groups-accordion']/details/summary/div/h5"));
		List<WebElement> accordionbadgeValues = driver
				.findElements(By
						.xpath("//div[@id='accordion-group_assignment-groups-accordion']/details/summary/div/div[@class='row']/div[contains(@class,'badge')]"));
		for (int i = 0; i < accordionbadgeValues.size(); i++) {
			List<WebElement> getEletext = driver
					.findElements(By
							.xpath("//div[@id='accordion-group_assignment-groups-accordion']/details/summary/div/div[@class='row']/div[contains(@class,'badge')]/ancestor::div/h5"));
			accodName = getEletext.get(i).getText();
			accodValues = accordionbadgeValues.get(i).getText();
			accordionValues.put(accodName, Integer.parseInt(accodValues));
		}

		return accordionValues;

	}

	// Store the Accordion Name and Values in Hash Table
	@Step("Get the Assignment selected values for the accordion from Send Course Content to LMS POP up,  Method: {method} ")
	public LinkedHashMap<String, Integer> getAccordionValuesLMSPop() {
		String accodName = null;
		String accodValues = null;
		LinkedHashMap<String, Integer> accordionValues = new LinkedHashMap<String, Integer>();

		List<WebElement> accordionbadgeValues = driver
				.findElements(By
						.xpath("//section[@class='nds-modal__content']/div[contains(@class,'linksSummary_container')]/div[contains(@class,'linksSummary_group')]/div[contains(@class,'badge')]"));
		for (int i = 0; i < accordionbadgeValues.size(); i++) {
			List<WebElement> getEletext = driver
					.findElements(By
							.xpath("//section[@class='nds-modal__content']/div[contains(@class,'linksSummary_container')]/div[contains(@class,'linksSummary_group')]/div[contains(@class,'badge')]/preceding-sibling::h5"));
			accodName = getEletext.get(i).getText();
			accodValues = accordionbadgeValues.get(i).getText();
			accordionValues.put(accodName, Integer.parseInt(accodValues));
		}

		return accordionValues;

	}

	// Verify Send Course Content to LMS header Title is displayed
	@Step("Verify Header Title Send Course Content to LMS POP up,  Method: {method} ")
	public String getHeaderTitle() {
		WebElement getlmsHeaderTitle = driver
				.findElement(By
						.xpath("//div[@role='dialog']/header[@class='nds-modal__header']/h2"));
		return getlmsHeaderTitle.getText();
	}

	@Step("Verify Cancel Button on Send Course Content to LMS POP up,  Method: {method} ")
	public boolean isCancel() {
		WebElement cancelButton = driver.findElement(By
				.xpath("//button[@type='button']/span[contains(.,'Cancel')]"));
		boolean isDisplayed = cancelButton.isDisplayed();
		if (isDisplayed == true) {
			return true;
		}
		return false;
	}

	@Step("Verify Send To LMS Button on Send Course Content to LMS POP up,  Method: {method} ")
	public boolean isSendtoLMS() {
		WebElement sendtoLMSlButton = driver
				.findElement(By
						.xpath("//button[@type='button']/span[contains(.,'Send To LMS')]"));
		boolean isDisplayed = sendtoLMSlButton.isDisplayed();
		if (isDisplayed == true) {
			return true;
		}
		return false;
	}

	@Step("Click Cancel Button on Send Course Content to LMS POP up,  Method: {method} ")
	public void clickCancel() {
		boolean isCancel = driver
				.findElements(
						By.xpath("//button[@type='button']/span[contains(.,'Cancel')]"))
				.size() > 0;
		if (isCancel == true) {
			CancelButton.click();
		}
	}

	@Step("Click Send To LMS Button on Send Course Content to LMS POP up,  Method: {method} ")
	public void clickSendToLMS() {
		WebElement sendtoLMSlButton = driver
				.findElement(By
						.xpath("//button[@type='button']/span[contains(.,'Send To LMS')]"));
		sendtoLMSlButton.click();
	}

	@Step("Verify We are sending your content to LMS text when user click Send To LMS button,  Method: {method} ")
	public String getContentTextLMS() {
		WebElement lmsContentxText = driver
				.findElement(By
						.xpath("//div[contains(@class,'page-container')]/h2[@class='page-title']"));
		return lmsContentxText.getText();
	}

	@Step("Verify Next Steps points from Content page,  Method: {method} ")
	public List<String> getContentPointsLMS() {
		ArrayList<String> contentList = new ArrayList<String>();
		List<WebElement> contentPoints = driver.findElements(By
				.xpath("//ul[contains(@class,'steps-list')]/li"));
		for (int i = 0; i < contentPoints.size(); i++) {
			String childPoints = contentPoints.get(i).getAttribute("innerText");
			String[] str = childPoints.split("\\r?\\n");
			String getText = str[1];
			contentList.add(getText);
		}
		return contentList;

	}

	@Step("Verify Next Steps points from Content page Refresh your LMS Course,  Method: {method} ")
	public List<String> getContentPointsRefreshLMS() {
		ArrayList<String> contentList = new ArrayList<String>();
		List<WebElement> contentPoints = driver
				.findElements(By
						.xpath("//ul[contains(@class,'steps-list')]/li/div/node()[not(local-name()='ul')]"));
		for (int i = 0; i < contentPoints.size(); i++) {
			String childPoints = contentPoints.get(1).getAttribute("nodeValue");
			contentList.add(childPoints);
		}
		return contentList;

	}

	@Step("Verify Next Steps points from Content page Refresh your LMS Course,  Method: {method} ")
	public List<String> getContentPointsRefreshLMSchild() {
		ArrayList<String> contentList = new ArrayList<String>();
		List<WebElement> contentPoints = driver.findElements(By
				.xpath("//ul[contains(@class,'steps-list')]/li/div/ul/li"));
		for (int i = 0; i < contentPoints.size(); i++) {
			String childPoints = contentPoints.get(i).getAttribute("innerText");
			contentList.add(childPoints);
		}
		return contentList;

	}

	@Step("Click the Link from the LMS Content page,  Method: {method} ")
	public void clickLink(String linkName) {
		WebElement link = driver.findElement(By.linkText(linkName));
		link.click();

	}

	@Step("Verify Due Date or LMS Value point Column is displayed,  Method: {method} ")
	public boolean isColumnName(String columnName) {
		boolean colName = driver
				.findElements(
						By.xpath("//button[contains(@class,'sortingButton_container')][contains(.,'"
								+ columnName + "')]")).size() > 0;
		if (colName == true) {
			return true;
		}
		return false;
	}

	@Step("get Assignment/Chapter Name from the accordians,  Method: {method} ")
	public String getChapterName(int count) throws InterruptedException {
		String defaultvalue = null;
		List<WebElement> chapterName = driver
				.findElements(By
						.xpath("//ancestor-or-self::tr[contains(@class,'table_selected')]/td/div[contains(@class,'_styles_container__')]/button[contains(@class,'_styles_label')]"));
		for (int i = 0; i < chapterName.size(); ) {
			if(i==count){
				defaultvalue = chapterName.get(count).getText();
				break;
			}
		}
		return defaultvalue;
	}

	
	@Step("get Assignment/Chapter Name from the accordians,  Method: {method} ")
	public String getChapterName(String chapterName) throws InterruptedException {
		String defaultvalue = null;
		List<WebElement> chapterNameEle = driver
				.findElements(By
						.xpath("//ancestor-or-self::tr[contains(@class,'table_table')]/td/div[contains(@class,'_styles_container__')]/button[contains(@class,'_styles_label')][starts-with(.,'"+chapterName+"')]"));
		for (int i = 0; i < chapterNameEle.size(); ) {
				defaultvalue = chapterNameEle.get(i).getText();
				break;
			
		}
		return defaultvalue;
	}
	@Step("get Accordians Name which is expanded mode,  Method: {method} ")
	public String getaccordName() throws InterruptedException {
		WebElement getaccdName = driver
				.findElement(By
						.xpath("//details[@open]/summary/descendant::div[@class='nds-disclosure__title']/h5"));
		return getaccdName.getText();
	}

	@SuppressWarnings("deprecation")
	@Step("Verify Date is disabled and system should not allow user to select a date before course start date,  Method: {method} ")
	public String checkdateDisablity(String dataValue)
			throws InterruptedException {
		String disableValue = null;
		int cMonth = GetDate.getCurrentMonth();
		String previousDate;
		List<WebElement> dueDateList = driver
				.findElements(By
						.xpath("//button[starts-with(@class,'_modalDateTimeCell_input__')]"));
		dueDateList.get(0).click();
		selectDateele.click();
		previousDate = GetDate.previousDay(dataValue);
		System.out.println(previousDate);
		String currentMonth = dayFormatmnth.format(Date.parse(previousDate));
		int pMonth = Integer.parseInt(currentMonth);
		String currentDay = dayFormatS.format(Date.parse(previousDate));
		System.out.println(currentDay);
		if (pMonth < cMonth) {
			driver.findElement(By.xpath("//div[@class='nav-arrow prev']"))
					.click();
			Thread.sleep(2000);
			WebElement selectDueDate = driver
					.findElement(By
							.xpath("//table[@role='presentation']/tbody/tr/td[@aria-label='"
									+ currentDay + "']"));
			System.out.println(selectDueDate.getText());
		} else {
			WebElement selectDueDate = driver
					.findElement(By
							.xpath("//table[@role='presentation']/tbody/tr/td[contains(@aria-label,'"
									+ currentDay + "')]"));
			disableValue = selectDueDate.getAttribute("aria-disabled");
		}
		return disableValue;

	}

	@SuppressWarnings("deprecation")
	@Step("Verify Date is disabled and system should not allow user to select a date after course  end date,  Method: {method} ")
	public String checkdateDisablityendDate(String dataValue)
			throws InterruptedException {
		String disableValue = null;
		int cMonth = GetDate.getCurrentMonth();
		String nextDate;
		List<WebElement> dueDateList = driver
				.findElements(By
						.xpath("//button[starts-with(@class,'_modalDateTimeCell_input__')]"));
		dueDateList.get(0).click();
		selectDateele.click();
		nextDate = GetDate.nextDay(dataValue);
		String currentMonth = dayFormatmnth.format(Date.parse(nextDate));
		int nMonth = Integer.parseInt(currentMonth);
		String currentDay = dayFormatS.format(Date.parse(nextDate));
		if (nMonth > cMonth) {
			driver.findElement(By.xpath("//div[@class='nav-arrow next']"))
					.click();
			Thread.sleep(5000);
			WebElement selectDueDate = driver
					.findElement(By
							.xpath("//table[@role='presentation']/tbody/tr/td[contains(@aria-label,'"
									+ currentDay + "')]"));
			System.out.println(selectDueDate.getText());
			disableValue = selectDueDate.getAttribute("aria-disabled");
		} else {
			WebElement selectDueDate = driver
					.findElement(By
							.xpath("//table[@role='presentation']/tbody/tr/td[contains(@aria-label,'"
									+ currentDay + "')]"));
			disableValue = selectDueDate.getAttribute("aria-disabled");
		}
		return disableValue;

	}

	@Step("NOQUE This content is unavailable right now,  Method: {method} ")
	public String contentNoQUE() {
		String text = null;
		WebElement getContentText = driver
				.findElement(By
						.xpath("//p[contains(@class,'_groupAssignmentPicker_icon-text')]"));
		text = getContentText.getText();
		return text.trim();
	}

	@Step("Site Licensing pop up,  Method: {method} ")
	public void isSiteLicensePopUp() {
		boolean issiteLicEle = driver
				.findElements(
						By.xpath("//button[@type='button']/span[contains(.,'No, not participating')]"))
				.size() > 0;
		if (issiteLicEle == true) {
			driver.findElement(
					By.xpath("//button[@type='button']/span[contains(.,'No, not participating')]"))
					.click();
		}
	}

	@Step("Get the Sent date from the accordians Table,  Method: {method} ")
	public String getSentDate() {
		return sentDate.getText();
	}

	@Step("Get the Due date from the accordians Table,  Method: {method} ")
	public String getDueDate() {
		WebElement getdueDate = driver
				.findElement(By
						.xpath("//button[starts-with(@class,'_modalDateTimeCell_input')]/div/span[string-length(text())>0]"));
		return getdueDate.getText();
	}

	@Step("Set the Time from Set Due date pop up,  Method: {method} ")
	public String setDueDate(String time) {
		String getTimeText = null;
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(
				By.xpath("//*[name()='svg' and contains(@class,'nds-icon nds-icon--chevron-down nds-button__icon')]"))
				.click();
		List<WebElement> timesele = driver
				.findElements(By
						.xpath("//ul[@role='listbox']/li[@role='option']/span[@class='nds-dropdown__option-label']"));
		for (int i = 0; i < timesele.size(); i++) {
			getTimeText = timesele.get(i).getText();
			if (getTimeText.equals(time)) {
				timesele.get(i).click();
				break;
			}
		}
		return getTimeText;
	}

	@SuppressWarnings({ "deprecation" })
	@Step("Select a Due Date for the accordian accdName{0},  Method: {method} ")
	public void selectDueDate1(int count) throws InterruptedException {
		List<WebElement> dueDateList = driver
				.findElements(By
						.xpath("//button[starts-with(@class,'_modalDateTimeCell_input__')]"));
		for (int i = 0; i < dueDateList.size(); i++) {
			if (i == count) {
				dueDateList.get(i).click();
				Thread.sleep(2000);
				selectDateele.click();
				SimpleDateFormat dayFormatmnth = new SimpleDateFormat("MM");
				SimpleDateFormat dayFormatday = new SimpleDateFormat("dd");
				SimpleDateFormat dayFormatS = new SimpleDateFormat(
						"EEEE, MMMM d, yyyy");
				String getmonth = GetDate.getCurrentDate();
				String currentMonth = dayFormatmnth
						.format(Date.parse(getmonth));
				int cMonth = Integer.parseInt(currentMonth);
				String getday = dayFormatday.format(Date.parse(getmonth));
				int cDay = Integer.parseInt(getday);
				int lastmonthday = GetDate.lastdayoftheMonth();
				String numberofdays = GetDate.addDays(15);
				String nextMonthDate = GetDate.getNextmonthDate();
				String nextMonth = dayFormatmnth.format(Date
						.parse(nextMonthDate));
				int nMonth = Integer.parseInt(nextMonth);
				if (cDay == lastmonthday) {
					if (nMonth > cMonth) {
						String cday = dayFormatS.format(Date
								.parse(numberofdays));
						Thread.sleep(2000);
						driver.findElement(
								By.xpath("//div[@class='nav-arrow next']"))
								.click();
						Thread.sleep(2000);
						WebElement selectDueDate = driver
								.findElement(By
										.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
												+ cday + "']"));
						selectDueDate.click();
					}
				} else if (cDay >= 16) {
					// Click Arrow icon.
					String cday = dayFormatS.format(Date.parse(numberofdays));
					Thread.sleep(2000);
					driver.findElement(
							By.xpath("//div[@class='nav-arrow next']")).click();
					Thread.sleep(2000);
					WebElement selectDueDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
											+ cday + "']"));
					selectDueDate.click();
				} else {
					String cday = dayFormatS.format(Date.parse(numberofdays));
					Thread.sleep(2000);
					WebElement selectDueDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
											+ cday + "']"));
					selectDueDate.click();
				}
			}

		}

	}

	@SuppressWarnings({ "deprecation" })
	@Step("Select a Due Date for the accordian accdName{0},  Method: {method} ")
	public void selectDueDate_jsondata(int count, String date)
			throws InterruptedException, ParseException {
		List<WebElement> dueDateList = driver
				.findElements(By
						.xpath("//button[starts-with(@class,'_modalDateTimeCell_input__')]"));
		for (int i = 0; i < dueDateList.size(); i++) {
			if (i == count) {
				dueDateList.get(i).click();
				Thread.sleep(2000);
				selectDateele.click();
				SimpleDateFormat dayFormatmnth = new SimpleDateFormat("MM");
				SimpleDateFormat dayFormatday = new SimpleDateFormat("dd");
				SimpleDateFormat dayFormatS = new SimpleDateFormat(
						"EEEE, MMMM d, yyyy");
				String getmonth = date;
				String currentMonth = dayFormatmnth
						.format(Date.parse(getmonth));
				int cMonth = Integer.parseInt(currentMonth);
				String getday = dayFormatday.format(Date.parse(getmonth));
				int cDay = Integer.parseInt(getday);
				int lastmonthday = GetDate.lastdayoftheMonth();
				String numberofdays = GetDate.addDays(15);
				String nextMonthDate = GetDate.getNextmonthDate();
				String nextMonth = dayFormatmnth.format(Date
						.parse(nextMonthDate));
				int nMonth = Integer.parseInt(nextMonth);
				if (cDay == lastmonthday) {
					if (nMonth > cMonth) {
						String cday = dayFormatS.format(Date
								.parse(numberofdays));
						Thread.sleep(2000);
						driver.findElement(
								By.xpath("//div[@class='nav-arrow next']"))
								.click();
						Thread.sleep(2000);
						WebElement selectDueDate = driver
								.findElement(By
										.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
												+ cday + "']"));
						selectDueDate.click();
					}
				} else if (cDay >= 16) {
					// Click Arrow icon.
					String cday = dayFormatS.format(Date.parse(numberofdays));
					Thread.sleep(2000);
					driver.findElement(
							By.xpath("//div[@class='nav-arrow next']")).click();
					Thread.sleep(2000);
					WebElement selectDueDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
											+ cday + "']"));
					selectDueDate.click();
					break;
				} else {
					String cday = ReusableMethods
							.dateConversioninDLtformat(date);
					System.out.println(cday);
					Thread.sleep(2000);
					WebElement selectDueDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][contains(@aria-label,'"
											+ cday + "')]"));
					selectDueDate.click();
					break;
				}
			}

		}

	}

	//
	@Step("Select the check boxes based the Chapter/Assignment name,  Method: {method} ")
	public void checkCheckBox(String chapterName) {
		List<WebElement> assignmentNames = driver
				.findElements(By
						.xpath("//div[@id='accordion-group_assignment-groups-accordion']/details[(@open)]/descendant::div[@class='nds-disclosure__contents-outer']//div/table/tbody/tr/td[contains(@class,'_table_table-data')]/div[contains(@class,'_styles_container')]/button[contains(@class,'_styles_label__')]"));
		for (int a = 0; a < assignmentNames.size(); a++) {
			String getEleText = assignmentNames.get(a).getText();
			List<WebElement> checkboxesEle = driver
					.findElements(By
							.xpath("//td[contains(@class,'checkbox')]/div/label//*[name()='svg' and @class='nds-icon nds-icon--check']"));
			WebElement getELementText = driver
					.findElement(By
							.xpath("//td[contains(@class,'checkbox')]/div/input/following::button[contains(.,'"
									+ chapterName + "')]"));
			String getChapterText = getELementText.getText();
			if (getEleText.contains(getChapterText)) {
				checkboxesEle.get(a).click();
				break;
			}

		}

	}

	@SuppressWarnings({ "deprecation" })
	@Step("Select a Due Date for the accordian accdName{0},  Method: {method} ")
	public void selectDueDate_jsondata1(int count, String date) throws InterruptedException, ParseException {		
		List<WebElement> dueDateList = driver
				.findElements(By
						.xpath("//button[starts-with(@class,'_modalDateTimeCell_input__')]"));
		for (int i = 0; i < dueDateList.size(); i++) {
			if (i == count) {
				dueDateList.get(i).click();
				Thread.sleep(2000);
				selectDateele.click();
				SimpleDateFormat dayFormatmnth = new SimpleDateFormat("MM");
				SimpleDateFormat dayFormatday = new SimpleDateFormat("dd");
				SimpleDateFormat dayFormatS = new SimpleDateFormat(
						"EEEE, MMMM d, yyyy");
				String getmonth = date;
				currentMonth = dayFormatmnth
						.format(Date.parse(getmonth));
				cMonth = Integer.parseInt(currentMonth);
				String getday = dayFormatday.format(Date.parse(getmonth));
				int cDay = Integer.parseInt(getday);
				
				if(cMonth==11 && cDay <= 7) {
					numberofdays = GetDate.addDaysBasedonDate(date,15);
					cday = dayFormatS.format(Date.parse(numberofdays));
					WebElement selectEndDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
										+ cday + "']"));
					selectEndDate.click();
					break;
					
				} else if(cMonth==3 && cDay <= 14 ){
					numberofdays = GetDate.addDaysBasedonDate(date,0);
					cday = dayFormatS.format(Date.parse(numberofdays));
					String nextyrMonthdigits =dayFormatmnth.format(Date.parse(numberofdays));
					String nextyear =dayFormatyear.format(Date.parse(numberofdays));
					String nextyrMonth = ReusableMethods.formatMonth(nextyrMonthdigits);
					String getNextMthYear =nextyrMonth+" ".concat(nextyear);
					calMonthEle =driver.findElement(By.xpath("//div[@data-visible='true']/div[contains(@class,'CalendarMonth_caption CalendarMonth')]/strong"));
					calMonth =calMonthEle.getText();
					while(!getNextMthYear.equals(calMonth)){
						driver.findElement(
								By.xpath("//div[@class='nav-arrow next']"))
								.click();
						Thread.sleep(5000);
						calMonthEle =driver.findElement(By.xpath("//div[@data-visible='true']/div[contains(@class,'CalendarMonth_caption CalendarMonth')]/strong"));
						calMonth =calMonthEle.getText();
					}
					
					WebElement selectEndDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
										+ cday + "']"));
					selectEndDate.click();
				    break;
				} else if(cMonth==3 && cDay >= 14 ){
					numberofdays = GetDate.addDaysBasedonDate(date,0);
					cday = dayFormatS.format(Date.parse(numberofdays));
					String nextyrMonthdigits =dayFormatmnth.format(Date.parse(numberofdays));
					String nextyear =dayFormatyear.format(Date.parse(numberofdays));
					String nextyrMonth = ReusableMethods.formatMonth(nextyrMonthdigits);
					String getNextMthYear =nextyrMonth+" ".concat(nextyear);
					calMonthEle =driver.findElement(By.xpath("//div[@data-visible='true']/div[contains(@class,'CalendarMonth_caption CalendarMonth')]/strong"));
					calMonth =calMonthEle.getText();
					while(!getNextMthYear.equals(calMonth)){
						driver.findElement(
								By.xpath("//div[@class='nav-arrow next']"))
								.click();
						Thread.sleep(5000);
						calMonthEle =driver.findElement(By.xpath("//div[@data-visible='true']/div[contains(@class,'CalendarMonth_caption CalendarMonth')]/strong"));
						calMonth =calMonthEle.getText();
					}
					
					WebElement selectEndDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
										+ cday + "']"));
					selectEndDate.click();
				    break;
				   
				} else {
					switch(cMonth) {
					case 1:
						numberofdays = GetDate.addDays(297);
						break;
					case 2:
						numberofdays = GetDate.addDays(266);
						break;
					case 3:
						numberofdays = GetDate.addDays(237);
						break;
					case 4:
						numberofdays = GetDate.addDays(207);
						break;
					case 5:
						numberofdays = GetDate.addDays(177);
						break;
					case 6:
						numberofdays = GetDate.addDays(145);
						break;
					case 7:
						numberofdays = GetDate.addDays(116);
						break;
					case 8:
						numberofdays = GetDate.addDays(85);
						break;
					case 9:
						numberofdays = GetDate.addDays(55);
						break;
					case 10:
						numberofdays = GetDate.addDays(27);
						break;
					default:
						numberofdays = GetDate.addDays(10);
					}
					cday = dayFormatS.format(Date.parse(numberofdays));
					String nextyrMonthdigits =dayFormatmnth.format(Date.parse(cday));
					String nextyear =dayFormatyear.format(Date.parse(cday));
					String nextyrMonth = ReusableMethods.formatMonth(nextyrMonthdigits);
					String getNextMthYear =nextyrMonth+" ".concat(nextyear);
					calMonthEle =driver.findElement(By.xpath("//div[@data-visible='true']/div[contains(@class,'CalendarMonth_caption CalendarMonth')]/strong"));
					calMonth =calMonthEle.getText();
					while(!getNextMthYear.equals(calMonth)){
						driver.findElement(
								By.xpath("//div[@class='nav-arrow next']"))
								.click();
						Thread.sleep(5000);
						calMonthEle =driver.findElement(By.xpath("//div[@data-visible='true']/div[contains(@class,'CalendarMonth_caption CalendarMonth')]/strong"));
						calMonth =calMonthEle.getText();
					}
					
					WebElement selectEndDate = driver
							.findElement(By
									.xpath("//table[@role='presentation']/tbody/tr/td[@aria-disabled='false'][@aria-label='"
										+ cday + "']"));
					selectEndDate.click();
					break;
				}
			}
		}

	}
}
