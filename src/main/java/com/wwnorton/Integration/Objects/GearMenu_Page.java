package com.wwnorton.Integration.Objects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.utlities.BaseDriver;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;

public class GearMenu_Page {

	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a[contains(text(),'Change Your Email')]")
	public WebElement Change_Your_Email_link;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a[contains(text(),'Change Your Password')]")
	public WebElement Change_Your_Password_link;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a[contains(text(),'Connected to LMS')]")
	public WebElement ConnectedtoLMS_link;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a[contains(text(),'Add Yourself to a Student Set')]")
	public WebElement Add_Yourself_Student_set_link;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a[contains(text(),'Request Support')]")
	public WebElement Request_Support_link;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a[contains(text(),'Help')]")
	public WebElement Help_link;

	@FindBy(how = How.ID, using = "change_email_current")
	public WebElement Current_email_address_Textbox;

	@FindBy(how = How.ID, using = "change_email_new")
	public WebElement change_email_new_Textbox;

	@FindBy(how = How.ID, using = "change_email_password")
	public WebElement change_email_password_Textbox;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Submit')]")
	public WebElement Submit_button;

	@FindBy(how = How.ID, using = "ncp_current_password")
	public WebElement current_password_Textbox;

	@FindBy(how = How.ID, using = "ncp_new_password")
	public WebElement new_password_Textbox;

	@FindBy(how = How.ID, using = "ncp_new_password_repeat")
	public WebElement reset_new_password_Textbox;
	
	@FindBy(how = How.ID, using = "authorize_instructor_first_name")
	public WebElement authFName;
	
	@FindBy(how = How.ID, using = "authorize_instructor_last_name")
	public WebElement authLName;
	
	@FindBy(how = How.ID, using = "authorize_instructor_email")
	public WebElement authEmail;
	
	@FindBy(how = How.XPATH, using ="//span[contains(.,'Authorize Instructor')]/parent::button[@role='button']")
	public WebElement authorizeInstButton;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Change Password')]")
	public WebElement changePassword_button;

	@FindBy(how = How.XPATH, using = "//div[@id='alert_dialog_outer_1']")
	public WebElement PasswordChangedText;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public WebElement Ok_button;

	// Manage Student Sets link on gearMenu
	@FindBy(how = How.XPATH, using = "//li[@class='ui-menu-item']/a[contains(text(),'Manage Student Sets')]")
	public WebElement ManageStudetSets;

	// Authorize an Instructor Account link on gearMenu
	@FindBy(how = How.XPATH, using = "//li[@class='ui-menu-item']/a[contains(text(),'Authorize an Instructor Account')]")
	public WebElement AuthorizeInstructorAccount;
	
	@FindBy(how = How.ID, using = "login_dialog_error_msg")
	public WebElement loginErrorMessage;

	public GearMenu_Page() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();

	// Allure Steps Annotations and Methods
	@Step("Click Change your Email,  Method: {method} ")
	public void change_Email() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		wait.until(ExpectedConditions.visibilityOf(Change_Your_Email_link));
		Change_Your_Email_link.click();
	}

	@Step("Click update your Email,  Method: {method} ")
	public void update_Email(String emailID,String password) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		wait.until(ExpectedConditions
				.visibilityOf(Current_email_address_Textbox));
		//String currentEmailID = jsonobject.getAsJsonObject("Change_Your_Email").get("Current_EmailID").getAsString();
		Current_email_address_Textbox.sendKeys(emailID);
		//String newEmailID = jsonobject.getAsJsonObject("Change_Your_Email").get("New_EmailID").getAsString();
		String[] str =emailID.split("@");
		String newEmailID = str[0]+ com.wwnorton.Integration.utlities.GetRandomId.randomAlphaNumeric(3).toLowerCase()+ "@mailinator.com";
		change_email_new_Textbox.sendKeys(newEmailID);
		//String password = jsonobject.getAsJsonObject("Change_Your_Email").get("Password").getAsString();
		change_email_password_Textbox.sendKeys(password);
		Submit_button.click();
	}

	@Step("Click Change your Passwordlink,  Method: {method} ")
	public void change_password() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		wait.until(ExpectedConditions.visibilityOf(Change_Your_Password_link));
		Change_Your_Password_link.click();
	}
	
	@Step("Click reset your Password validation of Password Error when Password less than 8 char,  Method: {method} ")
	public void validatePasswordError() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(current_password_Textbox));
		String currentpassword = jsonobject
				.getAsJsonObject("Change_Your_Password").get("CurrentPassword")
				.getAsString();
		current_password_Textbox.sendKeys(currentpassword);
		String newpassword = "Test";
		new_password_Textbox.sendKeys(newpassword);
		String resetpassword = "Testa";
		reset_new_password_Textbox.sendKeys(resetpassword);
		changePassword_button.click();
			
	}
	
	@Step("New Password and Retype New Password does not match,  Method: {method} ")
	public void passwordNotmatch() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(current_password_Textbox));
		String currentpassword = jsonobject
				.getAsJsonObject("Change_Your_Password").get("CurrentPassword")
				.getAsString();
		current_password_Textbox.sendKeys(currentpassword);
		String newpassword = "Tabletop1";
		new_password_Textbox.sendKeys(newpassword);
		String resetpassword = "Tabletop2";
		reset_new_password_Textbox.sendKeys(resetpassword);
		changePassword_button.click();
			
	}
	
	@Step("Current Password does not match,  Method: {method} ")
	public void currentpasswordNotmatch() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(current_password_Textbox));
		String currentpassword = "CurrentPass123";
		current_password_Textbox.sendKeys(currentpassword);
		String newpassword = "Tabletop1";
		new_password_Textbox.sendKeys(newpassword);
		String resetpassword = "Tabletop1";
		reset_new_password_Textbox.sendKeys(resetpassword);
		changePassword_button.click();
			
	}
	
	@Step("verify Error message for Password fields,  Method: {method} ")
	public String errormsgpasswordfields() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions.visibilityOf(loginErrorMessage));
		return loginErrorMessage.getText();
	}

	@Step("Click reset your Password,  Method: {method} ")
	public String reset_password() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(current_password_Textbox));
		String currentpassword = jsonobject
				.getAsJsonObject("Change_Your_Password").get("CurrentPassword")
				.getAsString();
		current_password_Textbox.sendKeys(currentpassword);
		String newpassword = jsonobject.getAsJsonObject("Change_Your_Password")
				.get("NewPassword").getAsString();
		new_password_Textbox.sendKeys(newpassword);
		String resetpassword = jsonobject
				.getAsJsonObject("Change_Your_Password").get("ResetPassword")
				.getAsString();
		reset_new_password_Textbox.sendKeys(resetpassword);
		changePassword_button.click();
		wait.until(ExpectedConditions.visibilityOf(PasswordChangedText));
		Ok_button.click();
		return resetpassword;
	}

	@Step("Click Connected to LMS link,  Method: {method} ")
	public void clickConnectedtoLMSlink() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		TimeUnit.SECONDS.sleep(5);

		wait.until(ExpectedConditions.visibilityOf(ConnectedtoLMS_link));
		ConnectedtoLMS_link.click();
	}
	
	@Step("Authorize the Instructor,  Method: {method} ")
	public void authorizeinst(String fName, String LName, String Email) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(authFName));
		TimeUnit.SECONDS.sleep(5);
        authFName.sendKeys(fName);
        authLName.sendKeys(LName);
        authEmail.sendKeys(Email);
        Thread.sleep(3000);
        authorizeInstButton.click();
	}

	@Step("Click on Manage StudentSet Link button,  Method: {method} ")
	public void clickManageStudentSetlink() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(ManageStudetSets));
		ManageStudetSets.click();
	}
}
