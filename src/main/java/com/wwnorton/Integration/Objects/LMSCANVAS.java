package com.wwnorton.Integration.Objects;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.utlities.BaseDriver;
import com.wwnorton.Integration.utlities.GetRandomId;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;


public class LMSCANVAS {

	WebDriver driver;
	WebDriverWait wait;
	JavascriptExecutor executor; 

	@FindBy(how = How.ID, using = "pseudonym_session_unique_id")
	public WebElement LMS_Email_ID;

	@FindBy(how = How.ID, using = "pseudonym_session_password")
	public WebElement LMS_Email_Password;

	@FindBy(how = How.XPATH, using = "//div[@class='ic-Form-control ic-Form-control--login']/button[@type='submit']")
	public WebElement LoginButton;

	@FindBy(how = How.XPATH, using = "//a[@id='global_nav_dashboard_link']")
	public WebElement DashboardLink;

	
	@FindBy(how = How.XPATH, using = "//div[@id='nav-tray-portal']/span/span/div/div/div/div/ul/li/a/span[contains(text(),'Automation_Data_Home')]")
	public WebElement CoursesLink;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ig-info']/a[1]")
	public WebElement AssignmentTitleLink;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	public WebElement LoadAssignmentButton;

	@FindBy(how = How.ID, using = "start_new_course")
	public WebElement StartNewCourseButton;

	@FindBy(how = How.ID, using = "course_name")
	public WebElement CourseName;

	@FindBy(how = How.ID, using = "course_license")
	public WebElement Courselicense_list;

	@FindBy(how = How.ID, using = "course_is_public")
	public WebElement course_is_public_checkbox;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Create course')]")
	public WebElement CreateCourseButton;

	@FindBy(how = How.XPATH, using = "//nav[@role='navigation']/ul/li[@class='section']/a[@class='settings']")
	public WebElement Settingslink;

	@FindBy(how = How.XPATH, using = "//div[@id='course_details_tabs']/ul/li[@id='external_tools_tab']/a[@id='tab-tools-link']")
	public WebElement AppsTab;

	@FindBy(how = How.XPATH, using = "//div[@class='externalApps_buttons_container']/a[@href='/courses/2610427/settings/configurations']")
	public WebElement ViewAppConfigurationslink;

	@FindBy(how = How.XPATH, using = "//div[@class='externalApps_buttons_container']/span[@class='AddExternalToolButton']/a[@href='#']")
	public WebElement APPlink;

	@FindBy(how = How.ID, using = "configuration_type_selector")
	public WebElement ConfigurationType;

	@FindBy(how = How.XPATH, using = "//div[@class='control-group']/label/input[@placeholder='Name']")
	public WebElement NameTextbox;

	@FindBy(how = How.XPATH, using = "//div[@class='control-group']/label/input[@placeholder='Consumer Key']")
	public WebElement ConsumerKey;

	@FindBy(how = How.XPATH, using = "//div[@class='control-group']/label/input[@placeholder='Shared Secret']")
	public WebElement SharedSecret;

	@FindBy(how = How.XPATH, using = "//div[@class='control-group']/label/input[@placeholder='Launch URL']")
	public WebElement LaunchURL;

	@FindBy(how = How.XPATH, using = "//div[@class='control-group']/label/input[@placeholder='Domain']")
	public WebElement Domain;

	@FindBy(how = How.ID, using = "submitExternalAppBtn")
	public WebElement Submitbutton;
	
	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(.,'Save')]")
	public WebElement SaveButton;
	
	@FindBy(how =How.LINK_TEXT, using ="Assignment")
	public WebElement Assignmentlink;
	
	@FindBy(how =How.ID, using ="assignment_name")
	public WebElement assignment_nameTextbox;
	
	@FindBy(how =How.ID, using ="assignment_points_possible")
	public WebElement assignment_points_possibleTextbox;
	
	@FindBy(how =How.ID, using ="assignment_external_tool_tag_attributes_url")
	public WebElement ExternalToolTextbox;
	
	@FindBy(how =How.XPATH, using ="//button[@class='btn btn-default save_and_publish']")
	public WebElement SaveAndPublish;
	
	@FindBy(how =How.ID, using ="addUsers")
	public WebElement addUser;
	
	@FindBy(how =How.ID, using ="addpeople_next")
	public WebElement addPeopleNext;
	
	@FindBy(how =How.XPATH, using ="//div[starts-with(@class,'chpSa_byIz')]/textarea")
	public WebElement textareasEmail;
	
	@FindBy(how =How.XPATH, using ="//button[@id='addpeople_next']")
	public WebElement NextButton;
	
	@FindBy(how =How.XPATH, using ="//input[@name='name'][@placeholder[starts-with(.,'New user')]]")
	public WebElement enterUserName;
	
	@FindBy(how =How.XPATH, using="//div[@data-view='users']/table/tbody[@class='collectionViewItems']/tr/td/span[contains(.,'pending')]")
	public WebElement pendingtext;
	
	@FindBy(how =How.XPATH, using="//button[@type='submit']/i[starts-with(@class,'icon-publish')]")
	public WebElement clickPublishButton;
	
	@FindBy(how =How.XPATH, using="//button[@class='ui-button btn-published disabled']")
	public WebElement published;
	
	@FindBy(how =How.XPATH, using="//button[@type='submit']/span/span[contains(.,'Logout')]")
	public WebElement logOutLMS;
	
	@FindBy(how =How.XPATH, using="//span[contains(.,'Choose and Publish')]/parent::button[@type='button']")
	public WebElement chooseandpublish;
	
	@FindBy(how =How.XPATH, using="//div[@class='button_box ic-Login-confirmation__actions']/a[@id='register']")
	public WebElement  createMyAccount;
	
	@FindBy(how =How.ID, using="pseudonym_password")
	public WebElement password;
	
	@FindBy(how =How.ID, using="choose_home_page")
	public WebElement choose_home_pagebutton;
	
	@FindBy(how =How.ID, using="user_terms_of_use")
	public WebElement agreecheckbox;
	
	@FindBy(how =How.XPATH, using="//span[contains(.,'Assignments List')]/preceding::input[@type='radio'][@value='assignments']")
	public WebElement assignmentList;
	
	@FindBy(how =How.XPATH, using="//div[@class='controls']/button[@type='submit']")
	public WebElement Registerbutton;
	
	@FindBy(how =How.XPATH, using="//div[@class='ic-notification__actions']/form/button[@name='accept']")
	public WebElement acceptButton;
	
	@FindBy(how =How.XPATH, using="//div[@id='assignment_show']/div[@class='assignment-title']/div[@class='title-content']/h1")
	public WebElement getAssignmentName;	
	
	@FindBy(how =How.XPATH, using="//table[@id='grades_summary']/tbody//tr[@class='student_assignment hard_coded final_grade']/td[@class='assignment_score']//span[@class='grade']")
	public WebElement studentGrade;
	
	@FindBy(how =How.XPATH, using="//div[@class='gradebook-cell']/span/span[@class='percentage']")
	public WebElement inststudentGrade;
	
	@FindBy(how =How.XPATH, using="//span[starts-with(.,'Course Name')]/following-sibling::span/span/input[@id]")
	public WebElement dltCourseName;
	
	@FindBy(how =How.XPATH, using="//span[starts-with(.,'Reference Code')]/following-sibling::span/span/input[@id]")
	public WebElement dltReferenceCode;
	
	@FindBy(how =How.XPATH, using="//button[@type='submit']")
	public WebElement dltAddCourse;
	
	@FindBy(how =How.XPATH, using="//div[@aria-label='Home Tutorial Tray']//div[contains(@class,'NewUserTutorialTray')]/span/button[not(contains(.,'Show Again'))]")
	public WebElement toggleArrow;
	
	public LMSCANVAS() {
		this.driver = BaseDriver.getDriver();
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
		executor = (JavascriptExecutor) BaseDriver.getDriver();

	}

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();
	
	@Step("Navigate to LMS application,  Method: {method} ")
	public void navigateLMS() {
		PropertiesFile.setlmsURL();
	}
	
	@Step("Enter an admin credenatils and click the Login button,  Method: {method} ")
	public void adminloginLMSCANVAS() throws InterruptedException {
		ReusableMethods.checkPageIsReady(driver);
		String LMSUserName = jsonobject.getAsJsonObject("LMSadminUser")
				.get("LMSadminUserLogin").getAsString();
		LMS_Email_ID.sendKeys(LMSUserName);
		String LMSPassword = jsonobject.getAsJsonObject("LMSadminUser")
				.get("LMSadminUserPassword").getAsString();
		LMS_Email_Password.sendKeys(LMSPassword);
		LoginButton.click();
	}
	
	@Step("Toggle Arrow Displayed Click the Arrow,  Method: {method} ")
	public void clickToggleArrow() throws InterruptedException {
		boolean istoggleArrow = driver.findElements(By.xpath("//div[@aria-label='Home Tutorial Tray']//div[contains(@class,'NewUserTutorialTray')]")).size() >0;
		if(istoggleArrow==true){
			toggleArrow.click();
		}
	}

	@Step("Enter an instructor credenatils and click the Login button,  Method: {method} ")
	public void instloginLMSCANVAS() throws InterruptedException {
		ReusableMethods.checkPageIsReady(driver);
		String LMSUserName = jsonobject.getAsJsonObject("LMSUser")
				.get("LMSINSTUserLogin").getAsString();
		LMS_Email_ID.sendKeys(LMSUserName);
		String LMSPassword = jsonobject.getAsJsonObject("LMSUser")
				.get("LMSINSTUserPassword").getAsString();
		LMS_Email_Password.sendKeys(LMSPassword);
		LoginButton.click();
	}
	
	@Step("Enter an DLT instructor credenatils and click the Login button,  Method: {method} ")
	public void dltInstloginLMSCANVAS() throws InterruptedException {
		ReusableMethods.checkPageIsReady(driver);
		String LMSUserName = jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserLogin").getAsString();
		LMS_Email_ID.sendKeys(LMSUserName);
		String LMSPassword = jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserPassword").getAsString();
		LMS_Email_Password.sendKeys(LMSPassword);
		LoginButton.click();
	}
	
	@Step("LMS Login with Student Login details,  Method: {method} ")
	public void studtloginLMSCANVAS(String LMSUserName, String LMSPassword) {
		LMS_Email_ID.sendKeys(LMSUserName);
		LMS_Email_Password.sendKeys(LMSPassword);
		LoginButton.click();
	}
	
	@Step("LMS Login with DLT Instructor Login details,  Method: {method} ")
	public void dltNewInstloginLMSCANVAS(String LMSUserName, String LMSPassword) {
		LMS_Email_ID.sendKeys(LMSUserName);
		LMS_Email_Password.sendKeys(LMSPassword);
		LoginButton.click();
	}
	
	

	@Step("Click on the link from left menu item list linkName {0}, Method:{method} ")
	public void clicklink(String linkName) throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='menu']")));
		TimeUnit.SECONDS.sleep(10);
		WebElement menuItemList = driver.findElement(By.xpath("//ul[@id='menu']/li//div[contains(text(),'"+linkName+"')]"));
		ReusableMethods.clickElement(driver, menuItemList);
		
	}
	@Step("Click on the Start New Couse Button, Method:{method} ")
	public void clickStartNewCouseButton() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(By.id("start_new_course")));
		TimeUnit.SECONDS.sleep(10);
		boolean isdisplayed= driver.findElements(By.id("start_new_course")).size() >0;
		if(isdisplayed==true){
		executor.executeScript("arguments[0].click();", StartNewCourseButton);
		}
		
	}
	
	@Step("Enter a Course Name, Method:{method} ")
	public String enterCourseName() {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("course_name")));
		boolean isdisplayed= driver.findElements(By.id("course_name")).size() >0;
		if(isdisplayed==true){
		executor.executeScript("arguments[0].click();", CourseName);
		}
		//CourseName.click();
		ReusableMethods.checkPageIsReady(driver);
		String courseName ="LMSAutomation"+GetRandomId.randomAlphaNumeric(3);
		CourseName.sendKeys(courseName);
		return courseName;
	}
	
	@Step("Click Create Course Button, Method:{method} ")
	public void clickCreateCourseButton() {
		CreateCourseButton.click();
	}
	
	@Step("Click Add user Button, Method:{method} ")
	public void clickAddUserButton() {
		addPeopleNext.click();
	}
	
	@Step("Click Assignment link, Method:{method} ")
	public void clickCreatedAssignmentLink() throws InterruptedException {
		Thread.sleep(5000);
		boolean isdisplayed= driver.findElements(By.xpath("//div[@class='ig-info']/a[1]")).size() >0;
		if(isdisplayed==true){
		try {
			AssignmentTitleLink.click();
		} catch (StaleElementReferenceException e) {
			// TODO Auto-generated catch block
			AssignmentTitleLink.click();
		}
		}else {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='ig-info']/a[1]")));
			try {
				AssignmentTitleLink.click();
			} catch (StaleElementReferenceException e) {
				// TODO Auto-generated catch block
				AssignmentTitleLink.click();
			}
		}
	}
	@Step("Click the Load Tab button Method:{method} ")
	public void clickLoadAssignmentButton() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
		LoadAssignmentButton.click();
		
	}
	
	@Step("Click Accept Button , Method:{method} ")
	public void clickAcceptButton() {
		boolean isAcceptButton = driver.findElements(By.xpath("//div[@class='ic-notification__actions']/form/button[@name='accept']")).size() >0;
		if(isAcceptButton==true){
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='ic-notification__actions']/form/button[@name='accept']")));
		//acceptButton.click();
		Actions act = new Actions(driver);
		act.moveToElement(acceptButton).click().build().perform();
		}
	}
	
	@Step("Click Admin Tray link adminlink {0}, Method:{method} ")
	public void clickAdminlink(String adminlink) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='tray-with-space-for-global-nav']")));
		WebElement navigationItemList = driver.findElement(By.xpath("//div[@class='tray-with-space-for-global-nav']/div/ul/li/a[contains(text(),'"+adminlink+"')]"));
		ReusableMethods.clickElement(driver, navigationItemList);
		
	}
	
	@Step("Click Navigation link navigationLink {0}, Method:{method} ")
	public void clickNavigationlink(String navigationLink) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//nav[@role='navigation'][@aria-label='Admin Navigation Menu']|//nav[@role='navigation'][@aria-label='Courses Navigation Menu']")));
		WebElement navigationItemList = driver.findElement(By.xpath("//nav[@role='navigation'][@aria-label='Admin Navigation Menu']/ul[@id='section-tabs']/li/a[contains(.,'"+navigationLink+"')] | //nav[@role='navigation'][@aria-label='Courses Navigation Menu']/ul[@id='section-tabs']/li/a[contains(.,'"+navigationLink+"')]"));
		ReusableMethods.clickElement(driver, navigationItemList);
		
	}
	
	@Step("Click Deep Linking tool link navigationLink {0}, Method:{method} ")
	public void clickDLTlink(String dltLink) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='show_account']")));
		WebElement link = driver.findElement(By.linkText(dltLink));
		link.click();
		
	}
	@Step("Click + Course button, Method:{method} ")
	public void clickCourseButton() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@aria-label='Create new course']")));
		WebElement courseDetailsTabList = driver.findElement(By.xpath("//button[@aria-label='Create new course']/span[contains(.,'Course')]"));
		ReusableMethods.clickElement(driver, courseDetailsTabList);
	}
	
	@Step("Enter a DLT Course Name, Method:{method} ")
	public void setDLTCourseName(String courseName) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[starts-with(.,'Course Name')]/following-sibling::span/span/input[@id]")));
		dltCourseName.sendKeys(courseName);
		
	}
	
	@Step("Enter a DLT Reference Code, Method:{method} ")
	public void setDLTReferenceCode(String referenceCode) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[starts-with(.,'Course Name')]/following-sibling::span/span/input[@id]")));
		dltReferenceCode.sendKeys(referenceCode);
		
	}
	
	@Step("Select SubAccount, Method:{method} ")
	public void selectSubAccount() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[starts-with(.,'Course Name')]/following-sibling::span/span/input[@id]")));
		WebElement acctSelector = driver.findElement(By.id("accountSelector"));
		acctSelector.click();
		
	}
	
	@Step("Select Enrollment , Method:{method} ")
	public void selectEnrollement() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[starts-with(.,'Course Name')]/following-sibling::span/span/input[@id]")));
		WebElement termSelector = driver.findElement(By.id("termSelector"));
		termSelector.click();
		WebElement formdialog = driver.findElement(By.xpath("//form[@role='dialog']/div/h2"));
		formdialog.click();
		
	}
	@Step("Click Add Course Button, Method:{method} ")
	public void clickAddCourse() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[starts-with(.,'Course Name')]/following-sibling::span/span/input[@id]")));
		dltAddCourse.click();
		
	}
	
	@Step("Select Course Name From Search By Filter, Method:{method} ")
	public void selectCourseFilter() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[starts-with(@id,'searchByFilter')]")));
		WebElement clickarrowdownIcon = driver.findElement(By.xpath("//label[@for='searchByFilter']//*[name()='svg'and @name='IconArrowOpenDown']"));
		clickarrowdownIcon.click();
		WebElement couseFilter = driver.findElement(By.xpath("//input[starts-with(@id,'searchByFilter')][@aria-activedescendant='course']"));
		couseFilter.click();
	}
	
	@Step("Enter Course Name and Search the Courses, Method:{method} ")
	public void SearchCourse(String couseName) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[starts-with(@id,'searchByFilter')]")));
		WebElement searchCouse = driver.findElement(By.xpath("//input[starts-with(@placeholder,'Search courses...')]"));
		searchCouse.sendKeys(couseName);
	}
	
	@Step("Select the CourseLink from CourseList, Method:{method} ")
	public void clickCourseLink(String couseName) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tbody[starts-with(@data-automation,'courses list')]")));
		WebElement linkCourse = driver.findElement(By.xpath("//tbody[starts-with(@data-automation,'courses list')]/tr/td/a/span"));
		if(linkCourse.getText().equalsIgnoreCase(couseName)){
			driver.findElement(By.xpath("//tbody[starts-with(@data-automation,'courses list')]/tr/td/a")).click();
		
		}
		
	}
	
	@Step("Click Course details Tab courseDetailsTab {0} link, Method:{method} ")
	public void clickcourseDetailsTab(String courseDetailsTab) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@id='course_details_tab']")));
		WebElement courseDetailsTabList = driver.findElement(By.xpath("//div[@id='course_details_tabs']/ul/li/a[contains(text(),'"+courseDetailsTab+"')]"));
		ReusableMethods.clickElement(driver, courseDetailsTabList);
	}
	
	@Step("Click View App Configurations link , Method:{method} ")
	public void clickViewAppConfigurations() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='externalApps_buttons_container']")));
		WebElement ViewAppConfigurations = driver.findElement(By.xpath("//div[@class='externalApps_buttons_container']/a[contains(text(),'View App Configurations')]"));
		ReusableMethods.clickElement(driver, ViewAppConfigurations);
	}
	
	@Step("Click +App Button, Method:{method} ")
	public void clickAppButton() {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='externalApps_buttons_container']")));
		APPlink.click();
	}
	
	@Step("Enter an Information in Add App section, Method:{method} ")
	public void enterAppInfo() {
		wait.until(ExpectedConditions.elementToBeClickable(NameTextbox));
		NameTextbox.click();
		NameTextbox.sendKeys("AppAssignment"+GetRandomId.randomAlphaNumeric(2));
		String ConsumerKeys = jsonobject.getAsJsonObject("LMSKeys").get("ConsumerKey").getAsString();
		ConsumerKey.sendKeys(ConsumerKeys);	
		SharedSecret.sendKeys(jsonobject.getAsJsonObject("LMSKeys").get("SharedSecret").getAsString());
	}
	
	@Step("Enter an launch URL Information in Add App section, Method:{method} ")
	public void enterAppInfoLaunchURL(String launchURL,String dlpName)  {
		wait.until(ExpectedConditions.elementToBeClickable(LaunchURL));
		LaunchURL.click();
		//String launchURL= jsonobject.getAsJsonObject("LMSKeys").get("LaunchURL").getAsString();
		LaunchURL.sendKeys(launchURL+dlpName);
		URL u = null;
		try {
			u = new URL(launchURL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String hostURL =u.getHost();
	    Domain.sendKeys("https://"+hostURL);
	}
	
	@Step("Click Submit Button in Add App section, Method:{method} ")
	public void clickSubmitButton()  {
		Submitbutton.click();
		
	}
	
	@Step("Click Assignment link from Assignment page, Method:{method} ")
	public void clickAssignmentlink()  {
		Assignmentlink.click();
		
	}
	
	@Step("Click Add Assignment Button from Assignment page, Method:{method} ")
	public void clickAddAssignmentbutton()  {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		boolean isAddAssignment =  driver.findElements(By.xpath("//a[@title='Add Assignment']")).size() >0;
		if(isAddAssignment==true){
		WebElement addAssignment = driver.findElement(By.xpath("//a[@title='Add Assignment']"));
		addAssignment.click();
		}
		
		
		
	}
	@Step("Enter Assignment Name from Assignment page, Method:{method} ")
	public void enterAssignmentName()  {
		assignment_nameTextbox.click();
		assignment_nameTextbox.sendKeys("TestAssignment"+GetRandomId.randomAlphaNumeric(2));
		
	}
	@Step("get the Assignment Name , Method:{method} ")
	public String getAssignmentName()  {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='assignment_show']/div[@class='assignment-title']/div[@class='title-content']/h1")));
		return getAssignmentName.getText();
	
	}
	@Step("Enter Points in Assignment page, Method:{method} ")
	public void enterPoints(String points)  {
		assignment_points_possibleTextbox.clear();
		assignment_points_possibleTextbox.click();
		assignment_points_possibleTextbox.sendKeys(points);
		
	}
	@Step("Select Assignments from Assignment Group  , Method:{method} ")
	public void selectAssignmentGroup(String assignmentgroupname)  {
		Select assignmentGroup = new Select(driver.findElement(By.id("assignment_group_id")));
		assignmentGroup.selectByVisibleText(assignmentgroupname.trim());
		
	}
	
	@Step("Select Content License from Content License list box  , Method:{method} ")
	public void selectContentLicense()  {
		Select contentLicenselist = new Select(driver.findElement(By.id("course_license")));
		contentLicenselist.selectByValue("public_domain");
		
	}
	
	@Step("Select Privacy from Privacy list box  , Method:{method} ")
	public void selectPrivacy()  {
		Select privacyList = new Select(driver.findElement(By.xpath("//select[starts-with(@class,'form-control input-block-level')]")));
		privacyList.selectByValue("public");
		
	}
		
	@Step("Select Role from People select list , Method:{method} ")
	public void selectRole(String roleName) throws InterruptedException  {
		WebElement roleEle =driver.findElement(By.id("peoplesearch_select_role"));
        roleEle.click();	
        Thread.sleep(4000);
        List<WebElement> roleList = driver.findElements(By.xpath("//ul[@role='listbox']/li/span"));
        for(WebElement role : roleList){
        	if(role.getText().equals(roleName)){
        		role.click();
        		return;
        	}
        }
       
		
	}
	
	

	@Step("Select Grades from Display grade select list , Method:{method} ")
	public void selectGrades(String gradeName)  {
		Select assignmentGroup = new Select(driver.findElement(By.id("assignment_grading_type")));
		assignmentGroup.selectByVisibleText(gradeName.trim());
		
	}
	@Step("Select Submission Type from list, Method:{method} ")
	public void selectSubmissionType(String submissionType)  {
		Select assignmentGroup = new Select(driver.findElement(By.id("assignment_submission_type")));
		assignmentGroup.selectByVisibleText(submissionType.trim());
		
	}
	
	@Step("Enter or find an External Tool URL, Method:{method}")
	public void enterExternalURL(String dlpName)  {
		ExternalToolTextbox.click();
		//String externalToolURL=jsonobject.getAsJsonObject("LMSKeys").get("LaunchURL").getAsString();
		ExternalToolTextbox.sendKeys(dlpName);
	}
	
	@Step("Click the Checkbox Load This Tool In A New Tab,Method:{method}")
	public void clickChekboxLoadThisToolInANewTab()  {
	//	WebElement labelCheckbox = driver.findElement(By.xpath("//label[@class='checkbox']/input[@type='checkbox']"));
		((JavascriptExecutor)driver).executeScript("document.getElementById('assignment_external_tool_tag_attributes_new_tab').click()");
	}
	
	@Step("Click Save and Publish Button ,Method:{method}")
	public void clickSaveandPublishButton()  {
		wait.until(ExpectedConditions.elementToBeClickable(SaveAndPublish));
		SaveAndPublish.click();	
	}
	
	@Step("Click the toggle Button, Method:{method}")
	public void clickToggleButton() {
		boolean istogglehButton = driver.findElements(By.xpath("//div[@role='dialog']//button[contains(@id,'new_user_tutorial_toggle')]")).size()>0;
		if(istogglehButton==true){
			driver.findElement(By.xpath("//div[@role='dialog']//button[contains(@id,'new_user_tutorial_toggle')]")).click();
		}
	}
	
	@Step("Click the Publish Button, Method:{method}")
	public void clickPublishButton()  {
		boolean isPublishButton = driver.findElements(By.xpath("//button[@type='submit'][contains(.,'Publish')]")).size()>0;
		if(isPublishButton==true){
		try {
			WebElement publishButton = driver.findElement(By.xpath("//button[@type='submit'][contains(.,'Publish')]"));
			publishButton.click();
		} catch (StaleElementReferenceException e) {
			// TODO Auto-generated catch block
			WebElement publishButton = driver.findElement(By.xpath("//button[@type='submit'][contains(.,'Publish')]"));
			publishButton.click();
		}
		}
	}
	
	@Step("Click on Assignment Button, Method:(method)")
	public void clickAssignmenthButton(){
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn']")));
		ReusableMethods.checkPageIsReady(driver);
		WebElement AssignmentButton = driver.findElement(By.xpath("//button[@class='btn']"));
		executor.executeScript("arguments[0].click();", AssignmentButton);
	}
	@Step("Get NCIA Digital resource Window,  Method: {method} ")
	public String getNCIAWindow(WebDriver driver) throws Exception {
		String childWindow = null;
		for (String winHandle : driver.getWindowHandles()) {
			childWindow = winHandle;
			driver.switchTo().window(winHandle);
		}
		driver.switchTo().frame("tool_content");
		
		return childWindow;
	}
	
	@Step("Click + People Button from People Section, Method:{method} ")
	public void clickPeopleButton() {
		addUser.click();
	}
	
	@Step("Click Save Button , Method:{method} ")
	public void clickSaveButton() throws InterruptedException {
		Thread.sleep(4000);
		try {
			executor.executeScript("arguments[0].click();", SaveButton);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	
	@Step("Select Email Address Radio option, Method:{method} ")
	public void selectRadioOption() {
		List<WebElement> OptionEleName = driver.findElements(By.name("search_type"));
		for(int i=0; i<OptionEleName.size(); i++){
			String ischecked  = OptionEleName.get(i).getAttribute("checked");
			System.out.println(ischecked);
			String emailRadio=OptionEleName.get(i).getAttribute("id");
			if(ischecked.equalsIgnoreCase("true")&& emailRadio.equalsIgnoreCase("peoplesearch_radio_cc_path")){
				break;
			} else {
				driver.findElement(By.id("peoplesearch_radio_cc_path")).click();
			}
			
		}
	}
	
	@Step("Enter Email Address Email Text area section, Method:{method} ")
	public void enterEmailTextArea(String emailID) {
		wait.until(ExpectedConditions.visibilityOf(textareasEmail));
		textareasEmail.sendKeys(emailID);
	}
	
	@Step("Click the next button , Method:{method} ")
	public void clickNextButton() {
		wait.until(ExpectedConditions.visibilityOf(NextButton));
		NextButton.click();
	}
	
	@Step("Click the Add Name link , Method:{method} ")
	public void clickAddNameLink(String emailID) {
		WebElement clickAddNamelink = driver.findElement(By.xpath("//button[@data-address='"+emailID+"']"));
		clickAddNamelink.click();
	}
	
	@Step("Enter User Name in the User Name Text box , Method:{method} ")
	public void enterUserName(String userName) {
		wait.until(ExpectedConditions.visibilityOf(enterUserName));
		enterUserName.click();
		enterUserName.sendKeys(userName);
	}
	
	@Step("verify Pending text is displayed for the Email ID , Method:{method} ")
	public String getPendingText() {
		wait.until(ExpectedConditions.visibilityOf(pendingtext));
		return pendingtext.getText();
	}
	
	@Step("Select Assignment List Radio Option , Method:{method} ")
	public void selectCourseRadioOption(String valueName) {
		boolean isdisplayed =  driver.findElements(By.xpath("//input[@type='radio'][@value='"+valueName+"']")).size()>0;
		if(isdisplayed==true){
		WebElement radioEle = driver.findElement(By.xpath("//input[@type='radio'][@value='"+valueName+"']"));
		executor.executeScript("arguments[0].click();", radioEle);
		}
	}
	@Step("click Choose Home Page button , Method:{method} ")
	public void clickChooseHomePage() {
		wait.until(ExpectedConditions.elementToBeClickable(choose_home_pagebutton));
		choose_home_pagebutton.click();
	}
	
	@Step("Click Chose and Publish button , Method:{method} ")
	public void choseandPublish() throws InterruptedException {
	Thread.sleep(4000);
	boolean isdisplayed = driver.findElements(By.xpath("//span[contains(.,'Choose and Publish')]/parent::button[@type='button']")).size() >0;
	if(isdisplayed==true) {
	boolean isbuttonenbled =chooseandpublish.isEnabled();
		if(isbuttonenbled==true){
			executor.executeScript("arguments[0].click();", chooseandpublish);
		}
	}
	}
	
	
	@Step("Logout the LMS application , Method:{method} ")
	public void logoutLMS() {
		wait.until(ExpectedConditions.elementToBeClickable(logOutLMS));
			executor.executeScript("arguments[0].click();", logOutLMS);
	}
	
	
	@Step("Click Create My Account Button , Method:{method} ")
	public void clickCreateMyAccount() {
		wait.until(ExpectedConditions.elementToBeClickable(createMyAccount));
			executor.executeScript("arguments[0].click();", createMyAccount);
	}
	
	@Step("Enter a password for the user , Method:{method} ")
	public void enterPassword(String PassValue) {
		wait.until(ExpectedConditions.elementToBeClickable(password));
		executor.executeScript("arguments[0].click();", password);
		password.sendKeys(PassValue);
	}
	
	@Step("Check the I agree to the Acceptable Use Policy checkbox, Method:{method} ")
	public void clickAgreecheckbox() {
		wait.until(ExpectedConditions.elementToBeClickable(agreecheckbox));
		executor.executeScript("arguments[0].click();", agreecheckbox);
		}
	
	@Step("Click Register Button, Method:{method} ")
	public void clickRegisterButton() {
	    wait.until(ExpectedConditions.elementToBeClickable(Registerbutton));
		executor.executeScript("arguments[0].click();", Registerbutton);
		}
	
	@Step("Click on the Assignment from left menu Course menu, Method:{method} ")
	public void clickAssignmentName(String linkName) {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/h2[starts-with(.,'Courses')]/following::ul/li/a[contains(.,'"+linkName+"')]")));
		WebElement assignmentLink = driver.findElement(By.xpath("//div/h2[starts-with(.,'Courses')]/following::ul/li/a[contains(.,'"+linkName+"')]"));
		
		executor.executeScript("arguments[0].click();", assignmentLink);
		
	}
	

	@Step("Click on the Course Name from Dashboard Page, Method:{method} ")
	public void clickCourseName(String courseName) {
		boolean isdisplayed =driver.findElements(By.xpath("//div[starts-with(@class,'ic-DashboardCard__header')]//a[starts-with(@class,'ic-DashboardCard__link')]")).size() >0;
		if(isdisplayed==true){
		WebElement couseNameLink = driver.findElement(By.xpath("//div[starts-with(@class,'ic-DashboardCard__header')]//a[starts-with(@class,'ic-DashboardCard__link')]//div[@title='"+courseName+"']"));
		String getCouseName=couseNameLink.getText();
		System.out.println(getCouseName);
		couseNameLink.click();
		}
	}
	
	@Step("get the Student grade , Method:{method} ")
	public String getStudentGrade(){
		return studentGrade.getText();
	}
	
	@Step("get the Student grade from Instructor grade section , Method:{method} ")
	public String getAssignmentGradeInstPage(){
		return inststudentGrade.getText();
	}
	@Step("Click the Assignment link from Navigation bar , Method:{method} ")
	public void clickAssignmentLink(String assignmentName){
		boolean linkClick= driver.findElements(By.xpath("//nav[@id='breadcrumbs']//a/span[contains(.,'"+assignmentName+"')]")).size() >0;
		if(linkClick==true){
			driver.findElement(By.xpath("//nav[@id='breadcrumbs']//a/span[contains(.,'"+assignmentName+"')]")).click();
		}
	}
	//Click Publish from DLT inst login 
	@Step("Click the Publish Button from Dashboard Page , Method:{method} ")
	public void clickCouseNameLink(String courseName){	
	Actions action = new Actions(driver);
	boolean publishCourse= driver.findElements(By.xpath("//span[starts-with(.,'"+courseName+"')]/parent::span[contains(.,'Publish')]")).size()>0;
	if(publishCourse==true){
		try {
			WebElement courses =driver.findElement(By.xpath("//a/div/h3[starts-with(.,'"+courseName+"')]"));
			action.moveToElement(courses).click().perform();
		} catch (StaleElementReferenceException e) {
			WebElement courses =driver.findElement(By.xpath("//a/div/h3[starts-with(.,'"+courseName+"')]"));
			action.moveToElement(courses).click().perform();
		}		
	} else {
		try {
			WebElement courses =driver.findElement(By.xpath("//a/div/h3[starts-with(.,'"+courseName+"')]"));
			action.moveToElement(courses).click().perform();
		} catch (StaleElementReferenceException e) {
			WebElement courses =driver.findElement(By.xpath("//a/div/h3[starts-with(.,'"+courseName+"')]"));
			action.moveToElement(courses).click().perform();
		}		
	}
	}
	
	/*@Step("Get the list of Assignments like Assignment Name , Method:{method} ")
	public List<String> getassignmentNameList(){	
		ArrayList<String> assignmentLists = new ArrayList<String>();
		String assignmentName;
		List<WebElement> listEle = driver.findElements(By.xpath("//div[@class='assignment-list']//div[@class='ig-info']/a"));
		for(int i=0; i<listEle.size(); i++){
			assignmentName =listEle.get(i).getText();
			assignmentLists.add(assignmentName);
		}
		return assignmentLists;
	}*/
	
	@Step("Get the list of Assignments like Assignment Name,  Method: {method} ")
	public LinkedHashMap<String, String> getAssignmentNames() throws InterruptedException{
		Thread.sleep(3000);
		String assignmentName=null;
		LinkedHashMap<String, String> assignmentNameValues = new LinkedHashMap<String, String>();
		List<WebElement> assignmentValues =driver.findElements(By.xpath("//div[@class='assignment-list']//div[@class='ig-info']/a"));
		for(int i=0;  i<assignmentValues.size(); i++){
		//	List<WebElement> getEletext =  driver.findElements(By.xpath("//section[@class='nds-modal__content']/div[contains(@class,'linksSummary_container')]/div[contains(@class,'linksSummary_group')]/div[contains(@class,'badge')]/preceding-sibling::h5"));
			try {
				assignmentName = assignmentValues.get(i).getText();
			} catch (StaleElementReferenceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			assignmentNameValues.put("Assignment Name " +i,assignmentName);
		}
		
		return assignmentNameValues;
		
	}
	
	
	@Step("Get the Module Item titles,  Method: {method} ")
	public LinkedHashMap<String, String> getModuleItemTitle(){
		String moduleName=null;
		LinkedHashMap<String, String> moduleValues = new LinkedHashMap<String, String>();
		List<WebElement> moduleList =driver.findElements(By.xpath("//div[@aria-label ='W.W. Norton course materials']/child::div[contains(@id,'context_module_content_')]//div[@class='ig-info']/div[@class='module-item-title']/span[@class='item_name']/a[string-length(@title)>0]"));
		for(int i=0;  i<moduleList.size(); i++){
		//	List<WebElement> getEletext =  driver.findElements(By.xpath("//section[@class='nds-modal__content']/div[contains(@class,'linksSummary_container')]/div[contains(@class,'linksSummary_group')]/div[contains(@class,'badge')]/preceding-sibling::h5"));
			moduleName = moduleList.get(i).getText();
			moduleValues.put("Module Name " +i,moduleName);
		}
		
		return moduleValues;
		
	}
	/*@Step("Get the Due Date from the list of Assignments , Method:{method} ")
	public List<String> getassignmentDueDateList(){	
		ArrayList<String> assignmentDueDateLists = new ArrayList<String>();
		String assignmentDueDate = null;
		List<WebElement> listEle = driver.findElements(By.xpath("//div[@class='assignment-list']//div[@class='ig-details']/div[@class='ig-details__item assignment-date-due']/span[(@data-html-tooltip-title)]"));
		for(int i=0; i<listEle.size(); i++){
			try {
				assignmentDueDate =listEle.get(i).getText();
			} catch (StaleElementReferenceException e) {
				// TODO Auto-generated catch block
				assignmentDueDate =listEle.get(i).getText();
			}
			assignmentDueDateLists.add(assignmentDueDate);
		}
		return assignmentDueDateLists;
	}*/
	
	@Step("Get the Due Date from the list of Assignments,  Method: {method} ")
	public LinkedHashMap<String, String> getassignmentDueDateMap() throws InterruptedException{
		Thread.sleep(3000);
		String dueDate=null;
		
		LinkedHashMap<String, String> dueDateValues = new LinkedHashMap<String, String>();
		List<WebElement> dueDateList =driver.findElements(By.xpath("//div[@class='assignment-list']//div[@class='ig-details']/div[@class='ig-details__item assignment-date-due']/span[(@data-html-tooltip-title)]"));
		for(int i=0;  i<dueDateList.size(); i++){
		try {
				dueDate = dueDateList.get(i).getText();
			} catch (StaleElementReferenceException e) {
				dueDate = dueDateList.get(i).getText();
			}
			dueDateValues.put("Due Date " +i,dueDate);
		}
		
		return dueDateValues;
		
	}
	
	/*@Step("Get the LMS Point from the list of Assignments , Method:{method} ")
	public List<String> getassignmentLmsPointList(){	
		ArrayList<String> assignmentLPLists = new ArrayList<String>();
		String assignmentLMSP;
		List<WebElement> listEle = driver.findElements(By.xpath("//div[@class='assignment-list']//div[@class='ig-details__item js-score']/span[@class='non-screenreader']"));
		for(int i=0; i<listEle.size(); i++){
			assignmentLMSP =listEle.get(i).getText();
			assignmentLPLists.add(assignmentLMSP);
		}
		return assignmentLPLists;
	}*/
	
	@Step("Get the LMS Point from the list of Assignments , Method:{method} ")
	public LinkedHashMap<String, String> getassignmentLmsPointMap() throws InterruptedException{	
		Thread.sleep(3000);
		LinkedHashMap<String, String> lmspointValues = new LinkedHashMap<String, String>();
		String lmspointvalue=null;;
		List<WebElement> listEle = driver.findElements(By.xpath("//div[@class='assignment-list']//div[contains(@class,'ig-details__item js-score')]/span[@class='non-screenreader']"));
		for(int i=0; i<listEle.size(); i++){
			try {
				lmspointvalue =listEle.get(i).getText();
			} catch (StaleElementReferenceException e) {
				// TODO Auto-generated catch block
				lmspointvalue =listEle.get(i).getText();
			}
			lmspointValues.put("LMSPoint value " +i, lmspointvalue);
		}
		return lmspointValues;
	}
}
