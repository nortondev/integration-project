package com.wwnorton.Integration.Objects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

import com.wwnorton.Integration.utlities.BaseDriver;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReusableMethods;

public class NLTCoursePage {
	WebDriver driver;
	WebDriverWait wait;	
	PropertiesFile prop = new PropertiesFile();
	
	@FindBy(how = How.XPATH, using ="//button/span[contains(.,'Review Course Content')]")
	public WebElement reviewCourseButton;
	
	@FindBy(how = How.XPATH, using ="//button/span[contains(.,'Send Content')]")
	public WebElement selectCourseButton;
	
	public NLTCoursePage() {
		this.driver = BaseDriver.getDriver();
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
		

	}
	
	@Step("Get the Course Details on Course Created page,  Method: {method} ")
	public String getCourseName() {
		String courseName=null;
		boolean courseNameEle = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'_styles_course-details__')]/h5")).size() >0;
	   if(courseNameEle==true){
		   courseName=  driver
			.findElement(By
					.xpath("//div[starts-with(@class,'_styles_course-details__')]/h5")).getText();
	   }
		return courseName;
	}

	@Step("Get the School Name on Course Created page,  Method: {method} ")
	public String getSchoolName() {
		List<WebElement> schoolNameele = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'_styles_course-details__')]/p"));
	   
		return schoolNameele.get(0).getText();
	}
	
	@Step("Get the Start Date on Course Created page,  Method: {method} ")
	public String getStartDate() {
		List<WebElement> startdateEle = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'_styles_course-details__')]/p"));
	   
		return startdateEle.get(1).getText();
	}
	@Step("Get the End date on Course Created page,  Method: {method} ")
	public String getEndDate() {
		List<WebElement> endDateEle = driver
				.findElements(By
						.xpath("//div[starts-with(@class,'_styles_course-details__')]/p"));
	    return endDateEle.get(2).getText();
	}
	
	@Step("Get the TimeZone on Course Created page,  Method: {method} ")
	public String getTimeZone() {
		WebElement timeZoneEle = driver
				.findElement(By
						.xpath("//div[starts-with(@class,'_styles_course-details__')]/p/span"));
	   
		return timeZoneEle.getText();
	}
	
	@Step("Get the Book Code, Title and Subtitle on Course Created page,  Method: {method} ")
	public String getBookDetails(int count) {
		String bookdetails=null;
		List<WebElement> bookdetailsEle = driver
				.findElements(By
						.xpath("//div[contains(@class,'container')]/div[contains(@class,'field-column')]/span"));
	    for(int i=0; i<bookdetailsEle.size(); i++){
	    	if(i==count){
	    	bookdetails =bookdetailsEle.get(i).getText();
	    	break;
	    	}
	    }
		return bookdetails;
	}
	
	@Step("Verify What Next Text on Course Created page,  Method: {method} ")
	public String getheaderCalloutText() {
		List<WebElement> headerCallOutTextEle = driver
				.findElements(By
						.xpath("//div[contains(@class,'callout')]//div/h4"));
	    
		return headerCallOutTextEle.get(0).getText();
	}
	
	@Step("Verify Ready to send your assignments? label on Course Created page,  Method: {method} ")
	public String getReadysendassignmentsText() {
		List<WebElement> headerCallOutTextEle = driver
				.findElements(By
						.xpath("//div[contains(@class,'callout')]//div/h4"));
	    
		return headerCallOutTextEle.get(1).getText();
	}
	
	@Step("Verify Review Course Content Text  on Course Created page,  Method: {method} ")
	public String geteditAssignmentText() {
		List<WebElement> headerCallOutTextEle = driver
				.findElements(By
						.xpath("//div[contains(@class,'callout')]//div/p"));
	    
		return headerCallOutTextEle.get(0).getText();
	}
	@Step("Verify Ready to send your assignments Text  on Course Created page,  Method: {method} ")
	public String getsendAssignmentText() {
		List<WebElement> headerCallOutTextEle = driver
				.findElements(By
						.xpath("//div[contains(@class,'callout')]//div/p"));
	    String assignmentText =ReusableMethods.removeAsciiChar(headerCallOutTextEle.get(1).getText());
		return assignmentText;
	}
	
	@Step("Verify Review Course button on Course Created page,  Method: {method} ")
	public boolean getreviewCouseButton() {
		WebElement reviewCourseButton = driver
				.findElement(By
						.xpath("//button/span[contains(.,'Review Course Content')]"));
	    
		return reviewCourseButton.isDisplayed();
	}
	@Step("Verify Select Content button on Course Created page,  Method: {method} ")
	public boolean getSelectContentButton() {
		WebElement selectCourseButton = driver
				.findElement(By
						.xpath("//button/span[contains(.,'Send Content')]"));
	    
		return selectCourseButton.isDisplayed();
	}
	
	@Step("Click Review Course button from Course Created page,  Method: {method} ")
	public void ClickreviewCouseButton() {
		reviewCourseButton.click();
	}
	
	@Step("Click Select Course button from Course Created page,  Method: {method} ")
	public void ClickSelectCouseButton() {
		selectCourseButton.click();
	}
}
