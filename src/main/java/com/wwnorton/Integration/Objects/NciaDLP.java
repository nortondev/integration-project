package com.wwnorton.Integration.Objects;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.utlities.BaseDriver;
import com.wwnorton.Integration.utlities.GetDate;
import com.wwnorton.Integration.utlities.LogUtil;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.Timezone;

import ru.yandex.qatools.allure.annotations.Step;

public class NciaDLP {

	WebDriver driver;
	static String UserName_Top;
	WebDriverWait wait;
	

	// Finding Web Elements on NCIA Home page using PageFactory.

	@FindBy(how = How.XPATH, using = "//div[@id='loading_screen'][@style='display: block;']")
	public WebElement Loader;

	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span[@id='gear_button_username']")
	public WebElement Sign_in_or_Register;

	@FindBy(how = How.XPATH, using = "//label[@for='register_login_choice_login']/span/span[@class='ncia_button_color_background']")
	public WebElement register_Login_choice;

	@FindBy(how = How.XPATH, using = "//input[@type='email']")
	public WebElement Username;

	@FindBy(how = How.XPATH, using = "//input[@type='password']")
	public WebElement Password;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_button']")
	public WebElement SignInButton;

	@FindBy(how = How.XPATH, using = "//span[@id='gear_button_username']")
	public WebElement username;

	@FindBy(how = How.XPATH, using = "//label[@for='register_login_choice_register']/span/span")
	public WebElement Need_to_register;

	@FindBy(how = How.XPATH, using = "//label[@for='register_login_choice_login']/span/span")
	public WebElement yes_SignIn;

	@FindBy(how = How.XPATH, using = "//label[@for='register_login_choice_login']")
	public WebElement yes_SignIn1;

	@FindBy(how = How.XPATH, using = "//button[starts-with(@id,'gear_button')]//span[starts-with(@id,'gear_button_username')]")
	public WebElement gear_button_username;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a/b[contains(text(),'Sign Out')]")
	public WebElement SignOut_link;

	@FindBy(how = How.XPATH, using = "//button[@id='purchase_options_button']")
	public WebElement purchase_options_button;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_top_msg']")
	public WebElement login_Dialog_msg;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_purchase_info']/span[@id='login_dialog_purchase_info_msg']")
	public WebElement purchase_text;

	@FindBy(how = How.XPATH, using = "//td[@class=' title_td']/a")
	public WebElement Assignmentlink;

	@FindBy(how = How.XPATH, using = "//*[@id='main-content']/div/div[2]/div[2]/h1/span[1]")
	public WebElement AssignmentText;

	@FindBy(how = How.XPATH, using = "//div[@id='activity_list_table_wrapper']/table/tbody/tr/td[@class=' title_td']/a[1]")
	public WebElement ZAPSAssignmentlink;

	@FindBy(how = How.ID, using = "login_dialog_error_msg")
	public WebElement TrialErrorMessage;

	@FindBy(how = How.ID, using = "register_purchase_choice_register")
	public WebElement registration_code_option;

	@FindBy(how = How.ID, using = "access_code")
	public WebElement AccessCode;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_forgot_password_link']/a")
	public WebElement ForgotPasswordlink;

	@FindBy(how = How.ID, using = "reset_password_email")
	public WebElement reset_password_emailTextbox;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Reset Password')]")
	public WebElement resetPasswordbutton;
	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(.,'Cancel')]")
	public WebElement Cancelbutton;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_reg_go_button']")
	public WebElement getFreeItemsButton;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_reg_go_button']/span[contains(normalize-space(.),'Show Purchasing Options')]")
	public WebElement ShowPurchasingOptionsButton;
	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button[@type='button']/span[contains(normalize-space(.),'Close')]")
	public WebElement closeButton;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog']//div[starts-with(text(),'Please check your email inbox and follow the link to complete the process.')]")
	public WebElement expireMsg;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Ebook')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public WebElement eBookTile;
	@FindBy(how = How.XPATH, using = "//table[@id='product_info_table']/tbody/tr/td[@id='product_info_td']/h2[@id='product_title']")
	public WebElement productTitle;

	@FindBy(how = How.XPATH, using = "//div[@id='user']/div[@class='whatsNew']")
	public WebElement userProfile;

	@FindBy(how = How.XPATH, using = "//div[@id='return_to_resources_button']/span[@id='return_to_resources_button_text']")
	public WebElement digitalResourceButton;
	@FindBy(how = How.XPATH, using = "//select[@id='activity_gau_set_timepicker']")
	public WebElement SelectTime;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button[@type='button']/span[contains(.,'Set Grades Accepted Until')]")
	public WebElement setGradesAcceptedUntilButton;
	
	@FindBy(how = How.XPATH, using ="//button[@type='button']/span[contains(.,'OK')]")
	public WebElement okButton;

	// Initializing Web Driver and PageFactory.
	public NciaDLP() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	// Call ReadJsonobject method to read and set node values from Json testData
	// file.

	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();

	// Allure Steps Annotations and Methods
	@Step("Click on Sign in or Register,  Method: {method} ")
	public void clickSignin_Register() throws InterruptedException {

		wait = new WebDriverWait(driver, 3);
		TimeUnit.SECONDS.sleep(3);

		wait.until(ExpectedConditions.visibilityOf(Sign_in_or_Register));

		/*
		 * Actions act = new Actions(driver);
		 * act.moveToElement(Sign_in_or_Register).click().build().perform();
		 */
		ReusableMethods.scrollIntoViewClick(Sign_in_or_Register, driver);

	}

	@Step("Navigate to back to DLP Home Pagee,  Method: {method} ")
	public void clickDigitalResourceButton() {
		digitalResourceButton.click();
	}

	@Step("exiting user having active registration code,  Method: {method} ")
	public void existingUser_RegCode() throws Exception {

		wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(10);

		// wait.until(ExpectedConditions.visibilityOf(yes_SignIn));
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(yes_SignIn);
		SignIn.click();
		SignIn.perform();
		String UserName = jsonobject.getAsJsonObject("ExistingUser_RegCode")
				.get("loginemail").getAsString();
		Username.sendKeys(UserName);
		Password.sendKeys(jsonobject.getAsJsonObject("ExistingUser_RegCode")
				.get("loginpassword").getAsString());
		SignInButton.click();
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		String actualUserName = UserName.toLowerCase();
		Thread.sleep(5000);
		String expectedUserName = username.getText();

		Assert.assertEquals(expectedUserName, actualUserName);

	}

	@Step("No, I need to register, purchase, or sign up for trial access.,  Method: {method} ")
	public void need_to_Register() throws Exception {
		Actions act = new Actions(driver);
		act.moveToElement(Need_to_register).click().build().perform();
		SignInButton.click();

	}

	@Step("Signin to User Account,  Method: {method} ")
	public void signInUserAccount(String jsonUserName, String jsonPassword)
			throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		TimeUnit.SECONDS.sleep(10);

		Actions act = new Actions(driver);
		act.moveToElement(register_Login_choice).click().build().perform();
		Username.clear();
		Username.sendKeys(jsonUserName);
		Password.sendKeys(jsonPassword);

		wait.until(ExpectedConditions.visibilityOf(SignInButton));
		SignInButton.click();

	}

	@Step("Verify User Name,  Method: {method} ")
	public void verify_Username(String userName) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//span[@id='gear_button_username']")));
		String actualUserName = userName;

		String expectedUserName = username.getText();

		Assert.assertEquals(expectedUserName.toLowerCase().trim(),
				actualUserName.toLowerCase().trim());

	}

	@Step("SignOut the User Name,  Method: {method} ")
	public void SignOut() throws InterruptedException {
		Thread.sleep(5000);
		ReusableMethods.checkPageIsReady(driver);
		WebDriverWait wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//*[@id='gear_button_username']")));
		gear_button_username.click();
		SignOut_link.click();
	}

	@Step("Click Ok Button,  Method: {method} ")
	public void clickOkButton() {
		boolean isOKButton = driver
				.findElements(
						By.xpath("//div[@class='ui-dialog-buttonset']/button[@type='button']/span[contains(text(),'OK')]"))
				.size() > 0;
		if (isOKButton == true) {
			WebElement clickOkbutton = driver
					.findElement(By
							.xpath("//div[@class='ui-dialog-buttonset']/button[@type='button']/span[contains(text(),'OK')]"));

			clickOkbutton.click();
		}
	}

	@Step("User reset the password,  Method: {method} ")
	public void resetPassword(String emailID) throws InterruptedException {
		ForgotPasswordlink.click();
		Thread.sleep(5000);
		reset_password_emailTextbox.sendKeys(emailID);
		resetPasswordbutton.click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	@Step("User Click Purchase Option Button,  Method: {method} ")
	public void clickPurchaseOption() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(5);
		wait.until(ExpectedConditions
				.elementToBeClickable(purchase_options_button));
		purchase_options_button.click();
	}

	@Step("User Purchase the product which is Free,Method: {method} ")
	public void checkPurchaseCheckbox(String optionText) {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//div[@id='login_dialog']/div[@id='login_dialog_inner']")));
		List<WebElement> purchaseOptionList = driver
				.findElements(By
						.xpath("//div[@id='login_dialog']/div[@id='login_dialog_inner']//div[@class='login_dialog_purchase_option_div']/table/tbody/tr"));
		for (int i = 0; i < purchaseOptionList.size();) {
			List<WebElement> colVals = purchaseOptionList
					.get(i)
					.findElements(
							By.xpath("//td[@class='login_dialog_purchase_price_td']/span"));

			java.util.Iterator<WebElement> purchasecol = colVals.iterator();
			WebElement val = null;
			while (purchasecol.hasNext()) {
				val = purchasecol.next();
				System.out.println(val.getText());

				if (val.getText().equalsIgnoreCase(optionText)) {
					WebElement purchaseOptionCheckbox = driver
							.findElement(By
									.xpath("//td[@class='login_dialog_purchase_price_td']/span[contains(text(),'"
											+ optionText
											+ "')]/following::td[@class='login_dialog_purchase_option_checkbox_td']/input[@class='purchase_option_checkbox']"));
					purchaseOptionCheckbox.click();

				}

			}
			break;
		}
	}

	@Step("User Click the get free items button,Method: {method} ")
	public void clickGetFreeItemsButton() {
		ReusableMethods.scrollIntoView(driver, getFreeItemsButton);
		getFreeItemsButton.click();
	}

	@Step("User Click Show Purchasing option button,Method: {method} ")
	public void clickShowPurchaseOptionButton() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions
				.elementToBeClickable(ShowPurchasingOptionsButton));
		ShowPurchasingOptionsButton.click();
	}

	@Step("get the Reset Password expired text,Method: {method} ")
	public String getexpiredText() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(expireMsg));
		return expireMsg.getText();
	}

	@Step("Click the Preview WorksheetButton,Method: {method} ")
	public void clickPreviewWorksheet() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.xpath("//td[@class='open_worksheet_button_td']")));
		List<WebElement> previewWSButton = driver
				.findElements(By
						.xpath("//td[@class='open_worksheet_button_td']/div[@role='button']"));
		for (int i = 0; i < previewWSButton.size();) {
			previewWSButton.get(0).click();
			break;
		}

	}

	@Step("Click the User Name on NoteFlight Application ,Method: {method} ")
	public void clickUserProfile() {
		/*
		 * WebDriverWait wait = new WebDriverWait(driver, 50);
		 * wait.until(ExpectedConditions
		 * .visibilityOfElementLocated(By.xpath("//div[@id='user']")));
		 */
		userProfile.click();

	}

	@Step("Click the Gear Button user Name ,Method: {method} ")
	public void clickGearButtonUserName() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//button[starts-with(@id,'gear_button')]//span[starts-with(@id,'gear_button_username')]")));
		gear_button_username.click();

	}

	@Step("Click the ZAPS Assignment Link ,Method: {method} ")
	public void clickZAPSAssignment(String assignmentName) {
		List<WebElement> assignmentlinks = driver
				.findElements(By
						.xpath("//div[@id='activity_list_table_wrapper']/table/tbody/tr/td[@class=' title_td']/a[1]"));
		for (int i = 0; i < assignmentlinks.size(); i++) {
			String assignmentgetText = assignmentlinks.get(i).getText();
			if (assignmentgetText.equalsIgnoreCase(assignmentName)) {
				assignmentlinks.get(i).click();
				break;
			}
		}

	}

	@Step("Click the Assignment link associated with assignmentTitle,  Method: {method} ")
	public void clickAssignmentLink(String assignmentTitle) {
		WebElement AssignmentLink = driver.findElement(By
				.xpath("//tr[td/a[contains(.,'" + assignmentTitle
						+ "')]]/td[@class=' title_td']/a"));
		AssignmentLink.click();

	}

	@Step("Click the Assignment link associated with assignmentTitle,  Method: {method} ")
	public void clickAssignmentLinkInst(String assignmentTitle) {
		WebElement AssignmentLink = driver.findElement(By
				.xpath("//tr[td/a[starts-with(.,'" + assignmentTitle
						+ "')]]/td[@class=' title_td']/a"));
		AssignmentLink.click();

	}

	@Step("Click the eBook Tile,  Method: {method} ")
	public void clickEbookTile() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//div[contains(text(),'Ebook')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")));
		eBookTile.click();
	}

	@Step("Click the tileName{0} Tile,  Method: {method} ")
	public void clickDLPtile(String tileName) throws InterruptedException {
		wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(3);
		List<WebElement> tileList = driver
				.findElements(By
						.xpath("//div[contains(text(),'"
								+ tileName
								+ "')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']"));
		tileList.get(0).click();

	}


	@Step("get the Product Title from DLP page,Method: {method} ")
	public String getProductTitle() {
		String product_Title = null;
		boolean isactivityDesc = driver
				.findElements(
						By.xpath("//div[@id='activity_group_description_title']/i|//div[@id='activity_group_description_title']"))
				.size() > 0;
		if (isactivityDesc == true) {
			product_Title = driver
					.findElement(
							By.xpath("//div[@id='activity_group_description_title']/i|//div[@id='activity_group_description_title']"))
					.getText();
		} else {
			product_Title = productTitle.getText().trim();
			String[] gettitle = product_Title.split("\\r?\\n");
			return gettitle[0];
		}
		return product_Title;

	}

	@Step("get the Product Editon from DLP page,Method: {method} ")
	public String getProductEditon() {
		String product_Edition = productTitle.getText().trim();
		String[] getEdition = product_Edition.split("\\r?\\n");
	    if(product_Edition.contains("EDITION")){
	    	return product_Edition;
	    } else {
		boolean isEdition = driver.findElements(By.id("product_edition"))
				.size() > 0;
		if (isEdition == true) {
			return getEdition[2];
		} else {
			return getEdition[1];
		}
	    }
	}

	// get the ebook Chapter list and add it into ArrayList
	@Step("get the Chapter list,  Method: {method} ")
	public List<String> getDLPChapterlist() {
		String chapters;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		wait.until(ExpectedConditions.elementToBeClickable(By
				.xpath("//div[@id='activity_list_table_wrapper']//td[@class=' title_td']/a")));
		List<WebElement> getcList = driver
				.findElements(By
						.xpath("//div[@id='activity_list_table_wrapper']//td[@class=' title_td']/a"));
		List<String> chapList = new ArrayList<String>();
		for (int i = 0; i < getcList.size(); i++) {
			chapters = getcList.get(i).getText();
			chapList.add(chapters);
		}
		return chapList;

	}

	@Step("get the list of DLP Name from the NCIA page ,Method: {method} ")
	public List<String> getProductCodeNames() {
		ArrayList<String> nameList = new ArrayList<String>();
		String pcodeName;
		List<WebElement> productNamelist = driver
				.findElements(By
						.xpath("//div[@id='activity_group_selectors']/button[starts-with(@class,'activity_group_selector')]/div[contains(@class,'activity_group_selector_name')] | //span[@id='instructor_activity_group_extras']/button[@data-activity_group='getting_started']/div[contains(@class,'activity_group_selector_name')]"));
		for (WebElement pcodeNameEle : productNamelist) {
			pcodeName = pcodeNameEle.getText();
			nameList.add(pcodeName);
		}
		return nameList;
	}
	
	@Step("Set the GAU date for the Assignment,Method: {method} ")
	public void clickSetLink(String assignmentTitleName) {
		List<WebElement> setLink = driver.findElements(By.xpath("//tbody/tr/td/a[starts-with(.,'"+assignmentTitleName+"')]/following::td[@class=' gau_td']/a"));
		setLink.get(0).click();
	}
	
	@Step("Set the Date link for the Assignment,Method: {method} ")
	public void clickDateLink(String assignmentTitleName, String dateStr) {
		List<WebElement> dateLink = driver.findElements(By.xpath("//tbody/tr/td/a[starts-with(.,'"+assignmentTitleName+"')]/following::td[@class=' gau_td']/a[contains(.,'"+dateStr+"')]"));
		dateLink.get(0).click();
	}
	
	@Step("Select GAU date as a System Date, Method: {method} ")
	public void SelectCurrentGauDate() throws Exception {		
		String getdate = GetDate.getCurrentDate();
		System.out.println(getdate);
		WebElement getDateTextbox = driver.findElement(By
				.id("activity_gau_set_datepicker"));
		getDateTextbox.sendKeys(getdate);
		ReusableMethods.clickTAB(driver);
	}
	
	@Step("User Set the GAU to +30 mins of current date & time,  Method: {method} ")
	public String getSetGAUTime5min() {
		String time = Timezone.get5MinsTime().toString();
		LogUtil.log(time);
		Select oselect = new Select(SelectTime);
		oselect.selectByVisibleText(time);

		// ... Appending ) to time for Assert
		if (!time.startsWith("1"))
			time = "0" + time;

		LogUtil.log("Assignment GAUTime  " + time);
		return time;
	}
	
	@Step("User Set the GAU to +30 mins of current date & time,  Method: {method} ")
	public String setAUSGAUTime5min() {
		String time = Timezone.get5MinsAUSTime().toString();
		LogUtil.log(time);
		Select oselect = new Select(SelectTime);
		oselect.selectByVisibleText(time);

		// ... Appending ) to time for Assert
		if (!time.startsWith("1"))
			time = "0" + time;

		LogUtil.log("Assignment GAUTime  " + time);
		return time;
	}

	@Step("User Set the GAU to +30 mins of current date & time,  Method: {method} ")
	public String getSetGAUTime() {
		String time = Timezone.getthirtyMinutesTime().toString();
		LogUtil.log(time);
		Select oselect = new Select(SelectTime);
		oselect.selectByVisibleText(time);

		// ... Appending ) to time for Assert
		if (!time.startsWith("1"))
			time = "0" + time;

		LogUtil.log("Assignment GAUTime  " + time);
		return time;
	}
	
	@Step("User Set the GAU to Hours of current date & time,  Method: {method} ")
	public String getHourGAUTime() {
		String time = Timezone.getHourTime().toString();
		LogUtil.log(time);
		Select oselect = new Select(SelectTime);
		oselect.selectByVisibleText(time);

		// ... Appending ) to time for Assert
		if (!time.startsWith("1"))
			time = "0" + time;

		LogUtil.log("Assignment GAUTime  " + time);
		return time;
	}
	@Step("Select TimjeZone on ZAPS Chapter page,  Method: {method} ")
	public void selectTimeZone(String timezoneKey) {
		WebElement selectTime = driver.findElement(By.xpath("//select[@id='activity_gau_set_timezonepicker']/option[contains(text(),'"+timezoneKey+"')]"));
		selectTime.click();
	}
	//get the selected Date 
	
	@Step("get the TimjeZone on  Activity setting pop up page,  Method: {method} ")
	public String getTimezone() {
		WebElement  gettimezone = driver.findElement(By.xpath("//select[@id='activity_gau_set_timezonepicker']"));
		return gettimezone.getText();
	}
	
	@Step("get the selected date from the Date picker Text box,  Method: {method} ")
	public String getGAUDate(int count) {
		List<WebElement> getDateTextbox = driver.findElements(By
				.xpath("//tbody/tr/td[@class=' gau_td']/a[@class='active_activity gau_set_link'][not(contains(.,'set'))]"));
		
		return getDateTextbox.get(count).getText();

	}
	
	@Step("get the selected time from the Time Text box ,  Method: {method} ")
	public String getTime() {
		WebElement getTimeTextbox = driver.findElement(By
				.id("activity_gau_set_timepicker"));
		return getTimeTextbox.getText();

	}
	
	@Step("get the selected time zone from the Time zone Text box ,  Method: {method} ")
	public String getTimeZone() {
		Select getTimezone = new Select(driver.findElement(By
				.id("activity_gau_set_timezonepicker")));
		String gt = getTimezone.getFirstSelectedOption().getText();
		System.out.println(gt);
		return gt;

	}

	@Step("Click Set Grade Accepted until button,  Method: {method} ")
	public void clickSGAUntilbutton()  {
		setGradesAcceptedUntilButton.click();
	}
	
	@Step("Click Ok Button to select Student button,  Method: {method} ")
	public void clickOKButton()  {
		boolean isOkButton = driver.findElements(By.xpath("//button[@type='button']/span[contains(.,'OK')]")).size()>0;
		if(isOkButton==true){
			okButton.click();
		}
	}
	@Step("Click Cancel Button from More Activity pop up,  Method: {method} ")
	public void clickCancelButton()  {
		Cancelbutton.click();
	}
}
