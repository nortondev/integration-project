package com.wwnorton.Integration.Objects;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.wwnorton.Integration.utlities.BaseDriver;
import com.wwnorton.Integration.utlities.FakerNames;
import com.wwnorton.Integration.utlities.GetRandomId;
import com.wwnorton.Integration.utlities.LogUtil;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class SmartWork5NewRegisteration {

	WebDriver driver;
	WebDriverWait wait;
	
	
	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span[@id='gear_button_username']")
	public WebElement Sign_in_or_Register;
	
	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']")
	public WebElement gearButtonSignIn;

	@FindBy(how = How.XPATH, using = "//label[@for='register_login_choice_register']/span/span")
	public WebElement Need_to_register;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_button']")
	public WebElement SignInButton;

	@FindBy(how = How.ID, using = "name")
	public WebElement FirstName_LastName;

	@FindBy(how = How.XPATH, using = "//input[@type='email']")
	public WebElement StudentEmail;

	@FindBy(how = How.XPATH, using = "//input[@type='password']")
	public WebElement Password;

	@FindBy(how = How.ID, using = "password2")
	public WebElement Password2;

	@FindBy(how = How.ID, using = "register_purchase_choice_trial")
	public WebElement register_purchase_choice_trial;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_reg_go_button']")
	public WebElement Sign_Up_for_Trial_AccessButton;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK, Sign Up for Trial Access')]")
	public WebElement OK_SignUpforTrialAccess_button;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Confirm')]/ancestor::button")
	public WebElement Confirm_Button;

	@FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
	public WebElement Checkbox;

	@FindBy(how = How.ID, using = "login_dialog_school_type_select")
	public WebElement school_dropdown;

	@FindBy(how = How.ID, using = "login_dialog_country_select")
	public WebElement Select_Country;

	@FindBy(how = How.ID, using = "login_dialog_state_select")
	public WebElement Select_State;

	@FindBy(how = How.ID, using = "login_dialog_school_text_input")
	public WebElement School_Institution;

	@FindBy(how = How.XPATH, using = "//ul[@id='ui-id-4']/li")
	public WebElement School_Selection;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Continue')]/ancestor::button")
	public WebElement Continue_Button;

	@FindBy(how = How.ID, using = "login_dialog_error_msg")
	public WebElement Error_Msg;
	@FindBy(how = How.XPATH, using = "//div[@class='login_dialog_reg_input_div tooltip']/span[@class='tooltiptext']")
	public WebElement toolTipText;	

	@FindBy(how = How.XPATH, using ="//div[@role='dialog']/div[starts-with(@id,'alert_dialog_outer_')]")
	public WebElement passworddialogtext;	
	
	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public WebElement oKbutton;
	@FindBy(how = How.XPATH, using = "//span[@id='gear_button_username']")
	public WebElement username;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_confirmation_go_button']")
	public WebElement Get_Started;

	@FindBy(how = How.XPATH, using = "//*[@id='gear_button_username']")
	public WebElement gear_button_username;
	
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a/b[contains(text(),'Sign Out')]")
	public WebElement SignOut_link;

	@FindBy(how = How.ID, using = "login_dialog_error_msg")
	public WebElement ErrorMessage;

	@FindBy(how = How.CLASS_NAME, using = "ui-progressbar-overlay")
	public WebElement Overlay;
	
	@FindBy(how = How.ID, using = "login_dialog_reg_startover_button")
	public WebElement backUpbutton;

	public SmartWork5NewRegisteration() {

		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	
	@Step("Navigate to NCIA DLP application,  Method: {method} ")
	public void navigateNCIADLP(String urlName){
		PropertiesFile.setURL(urlName);
	}

	@Step("Create a New User user through Smartwork5 Page and Verify User is created successfully,  Method: {method} ")
	public String Login_Smartwork5() throws InterruptedException {
		ReusableMethods.checkPageIsReady(driver);
		boolean isDisplayed= driver.findElements(By.xpath("//button[@id='login_button']")).size()>0;
		if(isDisplayed==true){
		Sign_in_or_Register.click();
		}
		Thread.sleep(3000);
		List<WebElement> oRadiobuttons = driver.findElements(By
				.name("register_login_choice"));
		/*for(int i=0; i<oRadiobuttons.size(); i++){
		radiochecked =oRadiobuttons.get(i).getAttribute("checked");
		}
		//boolean bvalue = false;
		//bvalue = oRadiobuttons.get(1).isSelected();
		if (radiochecked.equalsIgnoreCase("checked") ) {
			oRadiobuttons.get(0).click();
		} else {*/
		oRadiobuttons.get(1).click();
		SignInButton.click();
		Thread.sleep(3000);
		String FirstName = "john";
		String LastName = "mercey";
		FirstName_LastName.sendKeys(FirstName + " " + LastName);
		String EmailID = FirstName + "_" + LastName
				+ GetRandomId.randomAlphaNumeric(3).toLowerCase()
				+ "@mailinator.com";
		StudentEmail.sendKeys(EmailID);
		StudentEmail.sendKeys(Keys.CONTROL +"t");
		Password.sendKeys("Tabletop1");
		Password2.sendKeys("Tabletop1");
		boolean isOkButton = driver.findElements(By.xpath("//div[@class='ui-dialog-buttonset']/button[@type='button']/span[contains(text(),'OK')]")).size()>0;
		if(isOkButton==true){
		oKbutton.click();
		}
		register_purchase_choice_trial.click();
		Sign_Up_for_Trial_AccessButton.click();
		Thread.sleep(2000);
		OK_SignUpforTrialAccess_button.click();
		StudentEmail.sendKeys(EmailID);
		Confirm_Button.click();

		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Checkbox));
		Checkbox.click();
		Thread.sleep(2000);
		Select drop = new Select(school_dropdown);
		drop.selectByValue("HIGH_SCHOOL");
		Thread.sleep(2000);
		Select country = new Select(Select_Country);
		country.selectByValue("USA");
		Thread.sleep(2000);
		Select state = new Select(Select_State);
		state.selectByValue("USA_AZ");
		Thread.sleep(5000);
		School_Institution.click();
		School_Institution.sendKeys("Agua Fria High School");

		Thread.sleep(2000);
		Continue_Button.click();
		Thread.sleep(3000);
		Boolean isPresent = driver.findElements(
				By.id("login_dialog_confirmation_go_button")).size() > 0;
		if (isPresent == true) {
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);
			wait.until(ExpectedConditions.visibilityOf(Get_Started));
			Get_Started.click();
		} else {
			wait = new WebDriverWait(driver, 5000L);
			wait.until(ExpectedConditions.invisibilityOf(Overlay));
			wait.until(ExpectedConditions.visibilityOf(ErrorMessage));
			System.out.println(ErrorMessage.getText());
			
		}
		ReusableMethods.checkPageIsReady(driver);
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(username));
		String actualUserName = EmailID;
		Thread.sleep(5000);
		
		String expectedUserName = username.getText();
		LogUtil.log("The SW5 User Name is " + expectedUserName);
		Assert.assertEquals(expectedUserName, actualUserName);
		gear_button_username.click();
		SignOut_link.click();
		return expectedUserName;
	}

	@Step("Create an account with existing User credentails and Verify Error Message,  Method: {method} ")
	public void createaccountExistingCredentials(String fName, String LName,String emailID, String password) throws InterruptedException{
		ReusableMethods.checkPageIsReady(driver);
		boolean isDisplayed= driver.findElements(By.xpath("//button[@id='login_button']")).size()>0;
		if(isDisplayed==true){
		Sign_in_or_Register.click();
		}
		driver.findElement(By.id("username")).clear();
		Thread.sleep(3000);
		List<WebElement> oRadiobuttons = driver.findElements(By
				.name("register_login_choice"));
		boolean bvalue = false;
		bvalue = oRadiobuttons.get(1).isSelected();
		if (bvalue == true) {
			oRadiobuttons.get(1).click();
		} else {
			oRadiobuttons.get(1).click();
		}

		SignInButton.click();
		Thread.sleep(3000);
		FirstName_LastName.sendKeys(fName + " " + LName);
		StudentEmail.sendKeys(emailID);
		Password.sendKeys(password);
		Password2.sendKeys(password);
		boolean isOkButton = driver.findElements(By.xpath("//button[@type='button']/span[contains(text(),'OK')]")).size()>0;
		if(isOkButton==true){
		oKbutton.click();
		}
		register_purchase_choice_trial.click();
		Sign_Up_for_Trial_AccessButton.click();
		Thread.sleep(2000);
		OK_SignUpforTrialAccess_button.click();
		StudentEmail.sendKeys(emailID);
		Confirm_Button.click();

		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Checkbox));
		Checkbox.click();
		Select drop = new Select(school_dropdown);
		drop.selectByValue("HIGH_SCHOOL");

		Select country = new Select(Select_Country);
		country.selectByValue("USA");

		Select state = new Select(Select_State);
		state.selectByValue("USA_AZ");
		Thread.sleep(5000);
		School_Institution.click();
		School_Institution.sendKeys("Agua Fria High School");

		Thread.sleep(2000);
		Continue_Button.click();
		
	}

	@Step(" Verify Error Message,  Method: {method} ")
	public String getErrorMsg() {
		return Error_Msg.getText();
		
	}
	@Step(" Verify Tool Tip Text Message,  Method: {method} ")
	public String gettooltiptext() {
		return toolTipText.getText();
		
	}
	@Step(" Verify password message on dialog pop up,  Method: {method} ")
	public String getPasswordmsg() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(passworddialogtext));
		return passworddialogtext.getText();
		
	}
	
	@Step("Create a New User user through NCIA Page and Verify User is created successfully,  Method: {method} ")
	public String createNewUser() throws InterruptedException {
		ReusableMethods.checkPageIsReady(driver);
		
		Sign_in_or_Register.click();
		Thread.sleep(3000);
		List<WebElement> oRadiobuttons = driver.findElements(By
				.name("register_login_choice"));
		/*for(int i=0; i<oRadiobuttons.size(); i++){
		radiochecked =oRadiobuttons.get(i).getAttribute("checked");
		}
		//boolean bvalue = false;
		//bvalue = oRadiobuttons.get(1).isSelected();
		if (radiochecked.equalsIgnoreCase("checked") ) {
			oRadiobuttons.get(0).click();
		} else {*/
		oRadiobuttons.get(1).click();
		SignInButton.click();
		Thread.sleep(3000);
		String FirstName = FakerNames.getStudentFName();
		String LastName = FakerNames.getStudentFName();
		FirstName_LastName.sendKeys(FirstName + " " + LastName);
		String EmailID = FirstName + "_" + LastName
				+ GetRandomId.randomAlphaNumeric(3).toLowerCase()
				+ "@mailinator.com";
		StudentEmail.sendKeys(EmailID);
		StudentEmail.sendKeys(Keys.CONTROL +"t");
		Password.sendKeys("Tabletop1");
		Password2.sendKeys("Tabletop1");
		boolean isOkButton = driver.findElements(By.xpath("//div[@class='ui-dialog-buttonset']/button[@type='button']/span[contains(text(),'OK')]")).size()>0;
		if(isOkButton==true){
		oKbutton.click();
		}
		register_purchase_choice_trial.click();
		Sign_Up_for_Trial_AccessButton.click();
		Thread.sleep(2000);
		OK_SignUpforTrialAccess_button.click();
		StudentEmail.sendKeys(EmailID);
		Confirm_Button.click();

		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(Checkbox));
		Checkbox.click();
		Select drop = new Select(school_dropdown);
		drop.selectByValue("HIGH_SCHOOL");

		Select country = new Select(Select_Country);
		country.selectByValue("USA");

		Select state = new Select(Select_State);
		state.selectByValue("AZ");
		Thread.sleep(5000);
		School_Institution.click();
		School_Institution.sendKeys("Agua Fria High School");

		Thread.sleep(2000);
		Continue_Button.click();
		Thread.sleep(3000);
		Boolean isPresent = driver.findElements(
				By.id("login_dialog_confirmation_go_button")).size() > 0;
		if (isPresent == true) {
			wait = new WebDriverWait(driver, 8);
			TimeUnit.SECONDS.sleep(8);
			wait.until(ExpectedConditions.visibilityOf(Get_Started));
			Get_Started.click();
		} else {
			wait = new WebDriverWait(driver, 5000L);
			wait.until(ExpectedConditions.invisibilityOf(Overlay));
			wait.until(ExpectedConditions.visibilityOf(ErrorMessage));
			System.out.println(ErrorMessage.getText());
			
		}
		ReusableMethods.checkPageIsReady(driver);
		wait = new WebDriverWait(driver, 8);
		TimeUnit.SECONDS.sleep(8);
		wait.until(ExpectedConditions.visibilityOf(username));
		String actualUserName = EmailID;
		Thread.sleep(5000);		
		ReusableMethods.checkPageIsReady(driver);
		String expectedUserName = username.getText();
		LogUtil.log("The SW5 User Name is " + expectedUserName);
		Assert.assertEquals(expectedUserName.toLowerCase(), actualUserName.toLowerCase());
		gear_button_username.click();
		SignOut_link.click();
		return expectedUserName;
	}
}
