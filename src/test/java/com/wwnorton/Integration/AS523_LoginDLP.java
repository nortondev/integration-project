package com.wwnorton.Integration;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.Objects.LMSCANVAS;
import com.wwnorton.Integration.Objects.NLTPage;
import com.wwnorton.Integration.utlities.GetRandomId;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.TestListener;


@Listeners({TestListener.class})
public class AS523_LoginDLP extends PropertiesFile 
{
	LMSCANVAS LMSpage;
	NLTPage nlt;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();
		
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("INT-207_TC03_As an Instructor, verify that user could launch deep linking tool though Canvas LMS Successfully using LTI1.1 integration service")
	@Stories("INT-207_TC03_As an Instructor, verify that user could launch deep linking tool though Canvas LMS Successfully using LTI1.1 integration service")
	@Test()
	public void launchDLTInst() throws Exception {
		driver = getDriver();
		String subAccountlink = jsonobject.getAsJsonObject("SubAccountLink")
				.get("LinkName").getAsString();
		String getrandonValue = GetRandomId.randomAlphaNumeric(3);
		LMSpage = new LMSCANVAS();
		nlt= new NLTPage();
		LMSpage.navigateLMS();
		LMSpage.adminloginLMSCANVAS();
		LMSpage.clicklink("Admin");
		LMSpage.clickAdminlink("W.W. Norton");
		LMSpage.clickNavigationlink("Sub-Accounts");
		LMSpage.clickDLTlink(subAccountlink);
		//Click Course Button 
		LMSpage.clickCourseButton();
		String courseName = "DEMOAUTOMATIONDLT " + getrandonValue;
		LMSpage.setDLTCourseName(courseName);
		LMSpage.setDLTReferenceCode("Demo");
		LMSpage.selectSubAccount();
		LMSpage.selectEnrollement();
		LMSpage.clickAddCourse();
		Thread.sleep(2000);
		LMSpage.selectCourseFilter();
		LMSpage.SearchCourse(courseName);
		Thread.sleep(2000);
		LMSpage.clickCourseLink(courseName);
		LMSpage.clickNavigationlink("People");
		LMSpage.clickPeopleButton();
		Thread.sleep(2000);
		LMSpage.selectRadioOption();
		LMSpage.enterEmailTextArea(jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserLogin").getAsString());
		LMSpage.selectRole("Teacher");
		LMSpage.clickNextButton();
		Thread.sleep(2000);
		LMSpage.clickAddUserButton();
		Assert.assertEquals("pending", LMSpage.getPendingText());
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
		Thread.sleep(2000);
		LMSpage.dltInstloginLMSCANVAS();
		Thread.sleep(2000);
		LMSpage.clickAcceptButton();
		Thread.sleep(1000);
		LMSpage.clickCouseNameLink(courseName);
		LMSpage.clickPublishButton.click();
		getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		LMSpage.selectCourseRadioOption("assignments");
		Thread.sleep(3000);
		LMSpage.choseandPublish();
		Thread.sleep(5000);
		String getpublished = LMSpage.published.getText();
		Assert.assertEquals("Published", getpublished.trim());
		Thread.sleep(3000);
		ReusableMethods.setDLTEnvironment(driver);
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Assert.assertEquals(nlt.lmsText(), "Successfully connected to your Learning Management System (LMS).");
		Assert.assertNotNull(jsonobject.getAsJsonObject("DLTLMSUser").get("LMSINSTUserLogin").getAsString(), nlt.getUserName().toLowerCase());
		ReusableMethods.SwitchTabandClose(driver,"Norton Learning Tools");
		ReusableMethods.switchToNewWindow(1, driver);
		Thread.sleep(5000);
		LMSpage.clickNavigationlink("Settings");
		driver.findElement(By.linkText("Delete this Course")).click();
		driver.findElement(
				By.xpath("//div[@class='form-actions']/button[@type='submit'][contains(text(),'Delete Course')]"))
				.click();
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
	}
	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}
}
