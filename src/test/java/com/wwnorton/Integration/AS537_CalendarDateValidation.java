package com.wwnorton.Integration;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.Objects.GearMenu_Page;
import com.wwnorton.Integration.Objects.LMSCANVAS;
import com.wwnorton.Integration.Objects.NLTCoursePage;
import com.wwnorton.Integration.Objects.NLTPage;
import com.wwnorton.Integration.Objects.NciaDLP;
import com.wwnorton.Integration.Objects.RegisterPurchasePage;
import com.wwnorton.Integration.Objects.SmartWork5NewRegisteration;
import com.wwnorton.Integration.utlities.LogUtil;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.TestListener;
@Listeners({TestListener.class})
public class AS537_CalendarDateValidation extends PropertiesFile {
	LMSCANVAS LMSpage;
	NLTPage nlt;
	NLTCoursePage nltCourse;
	String FirstName, LastName, emailID, Password, titleCode, productTitle;
	WebElement getEmailcourseInvi = null;
	NciaDLP ncia;
	SmartWork5NewRegisteration createaccount;
	List<WebElement> oRadiobutton;
	RegisterPurchasePage RP;
	GearMenu_Page gmlinks;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();
	
	@Parameters({ "browser" })
	@BeforeTest()
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");

	}
	@Severity(SeverityLevel.NORMAL)
	@Description("AS-537 Calender picker validations and verifications on DLT")
	@Stories("AS-537 Calender picker validations and verifications on DLT")
	@Test()
	public void calendarPickerValidation() throws InterruptedException {
		driver = getDriver();
		String getDiscipline = jsonobject.getAsJsonObject("Bookinfo")
				.get("Discipline").getAsString();
		String getBookTitle =jsonobject.getAsJsonObject("Bookinfo")
				.get("BookTitle").getAsString();
		String getEdition = jsonobject.getAsJsonObject("Bookinfo")
				.get("Edition").getAsString();
		LMSpage = new LMSCANVAS();
		nlt = new NLTPage();
		nltCourse = new NLTCoursePage();
		ReusableMethods RM = new ReusableMethods();
		emailID=jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserLogin").getAsString();
		String courseName =RM.courseCreation(emailID,driver);
		LogUtil.log(courseName);
		Thread.sleep(2000);
		LMSpage.dltInstloginLMSCANVAS();
		Thread.sleep(2000);
		LMSpage.clickAcceptButton();
		Thread.sleep(1000);
		LMSpage.clickCouseNameLink(courseName);
		LMSpage.clickPublishButton.click();
		getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		LMSpage.selectCourseRadioOption("assignments");
		Thread.sleep(3000);
		LMSpage.choseandPublish();
		Thread.sleep(5000);
		String getpublished = LMSpage.published.getText();
		Assert.assertEquals("Published", getpublished.trim());
		Thread.sleep(3000);
		try {
			ReusableMethods.setDLTEnvironment(driver);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Assert.assertEquals(nlt.lmsText(),
				"Successfully connected to your Learning Management System (LMS).");
		Assert.assertNotNull(
				jsonobject.getAsJsonObject("DLTLMSUser")
						.get("LMSINSTUserLogin").getAsString(), nlt
						.getUserName().toLowerCase());
		nlt.clickContinue();
		nlt.selectState(5);
		Thread.sleep(1000);
		nlt.selectSchoolName(4);
		nlt.setCourseName(courseName);
		// Select Time Zone
		nlt.selectTimeZone("Eastern");
		Thread.sleep(3000);
		try {
			nlt.selectStartDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(2000);
		try {
			nlt.selectEndDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String StartDate = nlt.getstartdateCoursePage();
		String Enddate = nlt.getEnddateCoursePage();
		
		Thread.sleep(2000);
		nlt.clickContinue();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseMaterialText(), "Select Course Material");
		nlt.selectSearchByBookTitleRadioOption();
		nlt.selectDiscipline(getDiscipline);
		nlt.selectBookTitle(getBookTitle);
		nlt.selectEdition(getEdition);
		Thread.sleep(2000);
		nlt.clickContinue();
		Assert.assertEquals(nlt.getconfirmBookpopup(), "Confirm Book");
		nlt.clickNobook();
		Assert.assertEquals(nlt.getCourseMaterialText(), "Select Course Material");
		Thread.sleep(2000);
		nlt.clickContinue();
		nlt.clickYesbook();
		Thread.sleep(2000);
		nlt.isSiteLicensePopUp();
		Assert.assertEquals(nlt.connCanvasText().trim(), "Connected to Canvas");
		nlt.clickCreateCourseButton();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseCreatedtext().trim(),
				"Your course has been created.");
		nltCourse.ClickSelectCouseButton();
		nlt.clickExpandButton("InQuizitive");
		Thread.sleep(2000);
		String ariaLablestartDate = nlt.checkdateDisablity(StartDate);
		if(ariaLablestartDate.equals(true)){
			Assert.assertTrue(ariaLablestartDate.equals(true)," The Previous date is disabled");
		}
		String ariaLableendDate = nlt.checkdateDisablityendDate(Enddate);
		if(ariaLableendDate.equals(true)){
			Assert.assertTrue(ariaLableendDate.equals(true)," The Next date is disabled");
		} 
		ReusableMethods.SwitchTabandClose(driver,"Norton Learning Tools");
		ReusableMethods.switchToNewWindow(1, driver);
		Thread.sleep(5000);
		LMSpage.clickNavigationlink("Settings");
		driver.findElement(By.linkText("Delete this Course")).click();
		driver.findElement(
				By.xpath("//div[@class='form-actions']/button[@type='submit'][contains(text(),'Delete Course')]"))
				.click();
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
	}
	
	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}
}
