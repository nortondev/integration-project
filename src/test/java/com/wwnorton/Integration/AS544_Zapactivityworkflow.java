package com.wwnorton.Integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.Objects.GearMenu_Page;
import com.wwnorton.Integration.Objects.LMSCANVAS;
import com.wwnorton.Integration.Objects.NLTCoursePage;
import com.wwnorton.Integration.Objects.NLTPage;
import com.wwnorton.Integration.Objects.NciaDLP;
import com.wwnorton.Integration.Objects.RegisterPurchasePage;
import com.wwnorton.Integration.Objects.SmartWork5NewRegisteration;
import com.wwnorton.Integration.utlities.GetDate;
import com.wwnorton.Integration.utlities.GetRandomId;
import com.wwnorton.Integration.utlities.LogUtil;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.TestListener;

@Listeners({ TestListener.class })
public class AS544_Zapactivityworkflow extends PropertiesFile {
	LMSCANVAS LMSpage;
	NLTPage nlt;
	NLTCoursePage nltCourse;
	String FirstName, LastName, emailID, Password, titleCode, productTitle;
	WebElement getEmailcourseInvi = null;
	NciaDLP ncia;
	SmartWork5NewRegisteration createaccount;
	List<WebElement> oRadiobutton;
	RegisterPurchasePage RP;
	GearMenu_Page gmlinks;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();
	List<String> zpasChapterList = new ArrayList<String>();
	List<String> nltZAPSChapList = new ArrayList<String>();
	List<String> dltAccdNames = new ArrayList<String>();
	LinkedHashMap<String, String> getgradedAssignmentValues;

	@Parameters({ "browser" })
	@BeforeTest()
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("psychsci6");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS-544  Zap activity workflow")
	@Stories("AS-544  Zap activity workflow")
	@Test()
	public void zapActivityWorkflow() throws InterruptedException {
		driver = getDriver();
		String getDiscipline = jsonobject
				.getAsJsonObject("BookinfoPsychological").get("Discipline")
				.getAsString();
		String getBookTitle = jsonobject
				.getAsJsonObject("BookinfoPsychological").get("BookTitle")
				.getAsString();
		String getEdition = jsonobject.getAsJsonObject("BookinfoPsychological")
				.get("Edition").getAsString();
		String subAccountlink = jsonobject.getAsJsonObject("SubAccountLink")
				.get("LinkName").getAsString();
		ncia = new NciaDLP();
		LMSpage = new LMSCANVAS();
		nltCourse = new NLTCoursePage();
		RP = new RegisterPurchasePage();
		nlt = new NLTPage();
		ReusableMethods.checkPageIsReady(driver);
		try {
			ncia.clickSignin_Register();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(5000);
		oRadiobutton = driver.findElements(By.name("register_login_choice"));
		oRadiobutton.get(1).click();

		String UserName = jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserLogin").getAsString();
		ncia.Username.sendKeys(UserName);
		ncia.Password.sendKeys(jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserPassword").getAsString());
		ncia.SignInButton.click();
		ReusableMethods.checkPageIsReady(driver);
		try {
			ncia.clickDLPtile("ZAPS");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean isOkButton = driver
				.findElements(
						By.xpath("//button[@type='button']/span[contains(text(),'OK')]"))
				.size() > 0;
		if (isOkButton == true) {
			RP.oKbutton.click();
		}
		boolean isactivityDesc = driver
				.findElements(
						By.xpath("//div[@id='activity_group_description_title']/i|//div[@id='activity_group_description_title']"))
				.size() > 0;
		if (isactivityDesc == true) {
			productTitle = ncia.getProductTitle();
		} else {
			String edition = ncia.getProductEditon();
			productTitle = ncia.getProductTitle() + ", "
					+ ReusableMethods.upperCaseWords(edition);
		}
		zpasChapterList = ncia.getDLPChapterlist();
		// if Alias is displayed

		ncia.clickDigitalResourceButton();
		try {
			ncia.SignOut();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.navigate().refresh();
		// Login into LMS and Select the Course and Click the Link

		try {
			LMSpage.navigateLMS();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		LMSpage.adminloginLMSCANVAS();
		LMSpage.clicklink("Admin");
		LMSpage.clickAdminlink("W.W. Norton");
		LMSpage.clickNavigationlink("Sub-Accounts");
		LMSpage.clickDLTlink(subAccountlink);
		// Click Course Button
		LMSpage.clickCourseButton();
		String getrandonValue = GetRandomId.randomAlphaNumeric(3);
		String courseName = "DEMOAUTOMATIONDLT " + getrandonValue;
		LMSpage.setDLTCourseName(courseName);
		LMSpage.setDLTReferenceCode("Demo");
		LMSpage.selectSubAccount();
		LMSpage.selectEnrollement();
		LMSpage.clickAddCourse();
		Thread.sleep(2000);
		LMSpage.selectCourseFilter();
		LMSpage.SearchCourse(courseName);
		Thread.sleep(2000);
		LMSpage.clickCourseLink(courseName);
		LMSpage.clickNavigationlink("People");
		LMSpage.clickPeopleButton();
		Thread.sleep(2000);
		LMSpage.selectRadioOption();
		LMSpage.enterEmailTextArea(jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserLogin").getAsString());
		LMSpage.selectRole("Teacher");
		LMSpage.clickNextButton();
		Thread.sleep(2000);
		LMSpage.clickAddUserButton();
		Assert.assertEquals("pending", LMSpage.getPendingText());
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
		Thread.sleep(2000);
		LMSpage.dltInstloginLMSCANVAS();
		Thread.sleep(2000);
		LMSpage.clickAcceptButton();
		Thread.sleep(1000);
		LMSpage.clickCouseNameLink(courseName);
		LMSpage.clickPublishButton.click();
		getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		LMSpage.selectCourseRadioOption("assignments");
		Thread.sleep(3000);
		LMSpage.choseandPublish();
		Thread.sleep(5000);
		String getpublished = LMSpage.published.getText();
		Assert.assertEquals("Published", getpublished.trim());
		Thread.sleep(3000);
		try {
			ReusableMethods.setDLTEnvironment(driver);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Assert.assertEquals(nlt.lmsText(),
				"Successfully connected to your Learning Management System (LMS).");
		Assert.assertNotNull(
				jsonobject.getAsJsonObject("DLTLMSUser")
						.get("LMSINSTUserLogin").getAsString(), nlt
						.getUserName().toLowerCase());
		nlt.clickContinue();
		nlt.selectState(5);
		Thread.sleep(1000);
		nlt.selectSchoolName(4);
		nlt.setCourseName(courseName);
		// Select Time Zone
		nlt.selectTimeZone("Eastern");
		Thread.sleep(3000);
		try {
			nlt.selectStartDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(2000);
		try {
			nlt.selectEndDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(2000);
		nlt.clickContinue();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseMaterialText(),
				"Select Course Material");
		nlt.selectSearchByBookTitleRadioOption();
		nlt.selectDiscipline(getDiscipline);
		nlt.selectBookTitle(getBookTitle);
		nlt.selectEdition(getEdition);
		Thread.sleep(2000);
		nlt.clickContinue();
		Assert.assertEquals(nlt.getconfirmBookpopup(), "Confirm Book");
		nlt.clickNobook();
		Assert.assertEquals(nlt.getCourseMaterialText(),
				"Select Course Material");
		Thread.sleep(2000);
		nlt.clickContinue();
		nlt.clickYesbook();
		Thread.sleep(2000);
		nlt.isSiteLicensePopUp();
		Assert.assertEquals(nlt.connCanvasText().trim(), "Connected to Canvas");
		nlt.clickCreateCourseButton();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseCreatedtext().trim(),
				"Your course has been created.");
		nltCourse.ClickSelectCouseButton();
		Assert.assertEquals(productTitle.trim(), nlt.accordionsName("ZAPS")
				.toString().trim());
		// Expand the EBook accordian
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		nltZAPSChapList = nlt.getNLTChapterlist("ZAPS");
		// compare DLP Chapter vs NLT Chapters for IQ

		@SuppressWarnings("unchecked")
		List<String> diffList = new ArrayList<>(CollectionUtils.disjunction(
				zpasChapterList, nltZAPSChapList));
		LogUtil.log(diffList);

		@SuppressWarnings("unchecked")
		List<String> SameList = new ArrayList<>(CollectionUtils.intersection(
				zpasChapterList, nltZAPSChapList));
		LogUtil.log(SameList);
		nlt.selectCheckBoxes(0, "ZAPS");
		int count = nlt.getCountCheckboxes("ZAPS");
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		Assert.assertEquals(count, nlt.getCheckBoxesCount("ZAPS"));
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		Assert.assertTrue(nlt.isCheckBoxSelected(0, "ZAPS"));
		nlt.selectCheckBoxes(-1, "ZAPS");
		int count1 = nlt.getCountCheckboxes("ZAPS");
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		Assert.assertEquals(count1, nlt.getCheckBoxesCount("ZAPS"));
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		Assert.assertTrue(nlt.isAssignmentNameChboxFocus("ZAPS"));
		Thread.sleep(2000);
		nlt.clickAssignmentNameChkbox();
		int assignmentcount = nlt.getCountCheckboxes("ZAPS");
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		Assert.assertEquals(assignmentcount, nlt.getCheckBoxesCount("ZAPS"));
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		nlt.clickAssignmentNameChkbox();
		int assignmentcount1 = nlt.getCountCheckboxes("ZAPS");
		Assert.assertEquals(assignmentcount1, 0);
		// Verify if the user selects due date and assignment checkbox is
		// automatically checked on
		nlt.selectDueDate(0);
		nlt.clickDueDateCancel();
		Assert.assertFalse(nlt.isCheckBoxSelected(0, "ZAPS"));
		nlt.selectDueDate(0);
		nlt.clickSetDueDatebutton();
		Assert.assertTrue(nlt.isCheckBoxSelected(0, "ZAPS"));
		// Uncheck the Check box and verify date is Not NULL
		nlt.selectCheckBoxes(0, "ZAPS");
		Assert.assertNotNull(nlt.getDateValue(), "The Date field  is NOT null");
		// Enter a date in all Due date section and verify Check boxes
		nlt.clickClearButton();
		Thread.sleep(3000);
		nlt.selectCheckBoxes(0, "ZAPS");
		Thread.sleep(3000);
		nlt.selectDueDate(3);
		int count2 = nlt.getCountCheckboxes("ZAPS");
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		Assert.assertEquals(count2, nlt.getCheckBoxesCount("ZAPS"));
		// LMS point value update and Validate the check box
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		nlt.clickAssignmentNameChkbox();
		nlt.selectCheckBoxes(0, "ZAPS");
		nlt.selectLMSpointValue(0);
		Thread.sleep(4000);
		ReusableMethods.clickTAB(driver);
		Assert.assertTrue(nlt.isCheckBoxSelected(0, "ZAPS"));
		nlt.selectCheckBoxes(0, "ZAPS");
		Thread.sleep(1000);
		nlt.selectLMSpointValue(3);
		int lmscount = nlt.getCountCheckboxes("ZAPS");
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		Assert.assertNotNull(lmscount);
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		nlt.clickAssignmentNameChkbox();
		Thread.sleep(2000);
		String getError = nlt.geterrorLMSpointValue(1, "9jh7");
		Assert.assertEquals("Invalid entry. Enter a number 1-999.", getError);
		String boundaryvalue = nlt.setboundayLMSpointValue(2, 2000);
		Assert.assertEquals(999, Integer.parseInt(boundaryvalue));
		System.out.println(boundaryvalue);
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(4000);
		nlt.clickExpandButton("ZAPS");
		Thread.sleep(2000);
		nlt.clickAssignmentNameChkbox();
		nlt.selectCheckBoxes(0, "ZAPS");
		nlt.selectLMSpointValue(0);
		Thread.sleep(1000);
		nlt.selectDueDate(0);
		nlt.setDueDate("5:00 PM");
		nlt.clickSetDueDatebutton();
		Thread.sleep(2000);
		nlt.clickreviewSendToLMS();
		Thread.sleep(5000);
		ReusableMethods.scrollToBottom(driver);
		Thread.sleep(2000);
		nlt.clickCancel();
		// Get the LMS Point values Due Date and Selected Chapter Name for
		// Graded Accordians
		dltAccdNames = nlt.accordionsNameList();
		getgradedAssignmentValues = new LinkedHashMap<String, String>();
		for (int i = 0; i < dltAccdNames.size(); i++) {
			/*
			 * String getvalue = dltAccdNames.get(i).toString(); StringTokenizer
			 * stk = new StringTokenizer(getvalue); firstWord =stk.nextToken();
			 */
			if (dltAccdNames.get(i).contains("ZAPS")) {
				// nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
				Thread.sleep(3000);
				boolean isdisplayed = driver
						.findElements(
								By.xpath("//p[contains(@class,'_groupAssignmentPicker_icon-text')]"))
						.size() > 0;
				if (isdisplayed == true) {
					String nocontentText = nlt.contentNoQUE();
					Assert.assertEquals(
							"This content is unavailable right now.",
							nocontentText.trim());
					nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
					continue;
				}
				boolean isDueDate = nlt.isColumnName("Due Date");
				if (isDueDate == true) {
					getgradedAssignmentValues.put("accdName " + i,
							nlt.getaccordName());
					getgradedAssignmentValues.put("chapterName " + i,
							nlt.getChapterName(0));
					getgradedAssignmentValues.put("datevalue " + i,
							nlt.getDateValue());
					getgradedAssignmentValues.put("lmspointValue " + i,
							nlt.getLMSpointValue(0));
				} else {
					getgradedAssignmentValues.put("accdNamenongrade " + i,
							nlt.getaccordName());
					getgradedAssignmentValues.put("chapterNamenonGrade " + i,
							nlt.getChapterName(0));
				}
				nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
				break;
			}
		}
		Thread.sleep(2000);
		nlt.clickreviewSendToLMS();
		Thread.sleep(1000);
		nlt.clickSendToLMS();
		Thread.sleep(2000);
		String titlenlt = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver, titlenlt);
		ReusableMethods.switchToNewWindow(1, driver);
		Thread.sleep(5000);
		// Click Assignment link.
		driver.navigate().refresh();
		Thread.sleep(5000);
		LMSpage.clickNavigationlink("Assignments");
		LMSpage.getAssignmentNames();
		LMSpage.getassignmentDueDateMap();
		LMSpage.getassignmentLmsPointMap();
		LinkedHashMap<String, String> mergeMapsgraded = new LinkedHashMap<String, String>();
		mergeMapsgraded.putAll(LMSpage.getAssignmentNames());
		mergeMapsgraded.putAll(LMSpage.getassignmentDueDateMap());
		mergeMapsgraded.putAll(LMSpage.getassignmentLmsPointMap());
		Assert.assertEquals(getgradedAssignmentValues.get("accdName 2") + ": "
				+ getgradedAssignmentValues.get("chapterName 2").toString(),
				mergeMapsgraded.get("Assignment Name 0").toString());

		String getDate1 = GetDate.getDateandTime(getgradedAssignmentValues
				.get("datevalue 2"));
		String getDateLMS1 = LMSpage.getassignmentDueDateMap()
				.get("Due Date 0").replace(",", "");
		Assert.assertEquals(getDate1.toString(), getDateLMS1.toString());
		Assert.assertEquals(getgradedAssignmentValues.get("lmspointValue 2")
				.concat(" pts"), mergeMapsgraded.get("LMSPoint value 0"));
		LMSpage.clickNavigationlink("Settings");
		driver.findElement(By.linkText("Delete this Course")).click();
		driver.findElement(
				By.xpath("//div[@class='form-actions']/button[@type='submit'][contains(text(),'Delete Course')]"))
				.click();
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
