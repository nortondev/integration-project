package com.wwnorton.Integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.Objects.GearMenu_Page;
import com.wwnorton.Integration.Objects.LMSCANVAS;
import com.wwnorton.Integration.Objects.NLTCoursePage;
import com.wwnorton.Integration.Objects.NLTPage;
import com.wwnorton.Integration.Objects.NciaDLP;
import com.wwnorton.Integration.Objects.RegisterPurchasePage;
import com.wwnorton.Integration.Objects.SmartWork5NewRegisteration;
import com.wwnorton.Integration.utlities.GetDate;
import com.wwnorton.Integration.utlities.LogUtil;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.TestListener;

@Listeners({ TestListener.class })
public class AS536_NonGradedResource extends PropertiesFile {
	LMSCANVAS LMSpage;
	NLTPage nlt;
	NLTCoursePage nltCourse;
	String FirstName, LastName, emailID, Password, titleCode, productTitle;
	WebElement getEmailcourseInvi = null;
	NciaDLP ncia;
	SmartWork5NewRegisteration createaccount;
	List<WebElement> oRadiobutton;
	RegisterPurchasePage RP;
	GearMenu_Page gmlinks;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();
	List<String> dltAccdNames = new ArrayList<String>();
	LinkedHashMap<String, String> getgradedAssignmentValues;
	LinkedHashMap<String, String> getNongradedModuleValues;
	Actions SignIn;
	int nltKey, lmsKey;

	@Parameters({ "browser" })
	@BeforeTest()
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS-536 On relaunching DLT, user should see assignments sent previously for graded and ungraded accordions with due dates and LMS point values")
	@Stories("AS-536 On relaunching DLT, user should see assignments sent previously for graded and ungraded accordions with due dates and LMS point values")
	@Test()
	public void senttoDateandLMSpointvalue() throws InterruptedException {
		driver = getDriver();
		String getDiscipline = jsonobject.getAsJsonObject("Bookinfo")
				.get("Discipline").getAsString();
		String getBookTitle =jsonobject.getAsJsonObject("Bookinfo")
				.get("BookTitle").getAsString();
		String getEdition = jsonobject.getAsJsonObject("Bookinfo")
				.get("Edition").getAsString();
		LMSpage = new LMSCANVAS();
		nlt = new NLTPage();
		nltCourse = new NLTCoursePage();
		ReusableMethods RM = new ReusableMethods();
		emailID = jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserLogin").getAsString();
		String courseName = RM.courseCreation(emailID, driver);
		LMSpage.dltInstloginLMSCANVAS();
		Thread.sleep(2000);
		LMSpage.clickAcceptButton();
		Thread.sleep(1000);
		LMSpage.clickCouseNameLink(courseName);
		LMSpage.clickPublishButton.click();
		getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		LMSpage.selectCourseRadioOption("assignments");
		Thread.sleep(3000);
		LMSpage.choseandPublish();
		Thread.sleep(5000);
		String getpublished = LMSpage.published.getText();
		Assert.assertEquals("Published", getpublished.trim());
		Thread.sleep(3000);
		try {
			ReusableMethods.setDLTEnvironment(driver);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Assert.assertEquals(nlt.lmsText(),
				"Successfully connected to your Learning Management System (LMS).");
		Assert.assertNotNull(
				jsonobject.getAsJsonObject("DLTLMSUser")
						.get("LMSINSTUserLogin").getAsString(), nlt
						.getUserName().toLowerCase());
		nlt.clickContinue();
		nlt.selectState(5);
		Thread.sleep(1000);
		nlt.selectSchoolName(4);
		nlt.setCourseName(courseName);
		// Select Time Zone
		nlt.selectTimeZone("Eastern");
		Thread.sleep(3000);
		try {
			nlt.selectStartDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(2000);
		try {
			nlt.selectEndDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(2000);
		nlt.clickContinue();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseMaterialText(),
				"Select Course Material");
		nlt.selectSearchByBookTitleRadioOption();
		nlt.selectDiscipline(getDiscipline);
		nlt.selectBookTitle(getBookTitle);
		nlt.selectEdition(getEdition);
		Thread.sleep(2000);
		nlt.clickContinue();
		Assert.assertEquals(nlt.getconfirmBookpopup(), "Confirm Book");
		nlt.clickNobook();
		Assert.assertEquals(nlt.getCourseMaterialText(),
				"Select Course Material");
		Thread.sleep(2000);
		nlt.clickContinue();
		nlt.clickYesbook();
		Thread.sleep(2000);
		nlt.isSiteLicensePopUp();
		Assert.assertEquals(nlt.connCanvasText().trim(), "Connected to Canvas");
		nlt.clickCreateCourseButton();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseCreatedtext().trim(),
				"Your course has been created.");
		nltCourse.ClickSelectCouseButton();
		// Verify Review and Send to LMS button is disabled intially
		ReusableMethods.scrollToBottom(driver);
		Assert.assertFalse(nlt.reviewSendToLMS(), " Button is disabled");
		// for Ebook UnGraded Click the Check boxes login if Check box and LMS
		// point values are displayed then Enter else select checkbox
		dltAccdNames = nlt.accordionsNameList();
		// Also enter the values for Due date
		for (int i = 0; i < dltAccdNames.size(); i++) {
			nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
			Thread.sleep(2000);
			boolean isDueDate = nlt.isColumnName("Due Date");
			if (!isDueDate == true) {
				nlt.selectCheckBoxes(0, dltAccdNames.get(i).toString());
			} else {
				nlt.selectDueDate(0);
				nlt.clickSetDueDatebutton();
				nlt.selectLMSpointValue(0);
			}
			Thread.sleep(2000);
			nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());

		}

		nlt.clickreviewSendToLMS();
		Thread.sleep(5000);
		ReusableMethods.scrollToBottom(driver);
		Thread.sleep(2000);
		nlt.clickCancel();

		// Get the LMS Point values Due Date and Selected Chapter Name for
		// Graded Accordians
		getgradedAssignmentValues = new LinkedHashMap<String, String>();
		for (int i = 0; i < dltAccdNames.size(); i++) {
			/*
			 * String getvalue = dltAccdNames.get(i).toString(); StringTokenizer
			 * stk = new StringTokenizer(getvalue); firstWord =stk.nextToken();
			 */
			nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
			Thread.sleep(3000);
			boolean isdisplayed = driver
					.findElements(
							By.xpath("//p[contains(@class,'_groupAssignmentPicker_icon-text')]"))
					.size() > 0;
			if (isdisplayed == true) {
				String nocontentText = nlt.contentNoQUE();
				Assert.assertEquals("This content is unavailable right now.",
						nocontentText.trim());
				nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
				continue;
			}
			boolean isDueDate = nlt.isColumnName("Due Date");
			if (isDueDate == true) {
				getgradedAssignmentValues.put("accdName " + i,
						nlt.getaccordName());
				getgradedAssignmentValues.put("chapterName " + i,
						nlt.getChapterName(0));
				getgradedAssignmentValues.put("datevalue " + i,
						nlt.getDateValue());
				getgradedAssignmentValues.put("lmspointValue " + i,
						nlt.getLMSpointValue(0));
			} else {
				getgradedAssignmentValues.put("accdNamenongrade " + i,
						nlt.getaccordName());
				getgradedAssignmentValues.put("chapterNamenonGrade " + i,
						nlt.getChapterName(0));
			}
			nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
		}

		Thread.sleep(2000);
		nlt.clickreviewSendToLMS();
		Thread.sleep(1000);
		nlt.clickSendToLMS();
		Thread.sleep(2000);
		String titlenlt = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver, titlenlt);
		ReusableMethods.switchToNewWindow(1, driver);
		// Click Assignment link.
		driver.navigate().refresh();
		Thread.sleep(5000);
		LMSpage.clickNavigationlink("Assignments");
		LMSpage.getAssignmentNames();
		LMSpage.getassignmentDueDateMap();
		LMSpage.getassignmentLmsPointMap();
		LinkedHashMap<String, String> mergeMapsgraded = new LinkedHashMap<String, String>();
		mergeMapsgraded.putAll(LMSpage.getAssignmentNames());
		mergeMapsgraded.putAll(LMSpage.getassignmentDueDateMap());
		mergeMapsgraded.putAll(LMSpage.getassignmentLmsPointMap());
		Assert.assertEquals(getgradedAssignmentValues.get("accdName 3") + ": "
				+ getgradedAssignmentValues.get("chapterName 3").toString(),
				mergeMapsgraded.get("Assignment Name 0").toString());
		Assert.assertEquals(
				GetDate.getDateandTime(
						getgradedAssignmentValues.get("datevalue 3")).replace(
						"pm", " pm"), mergeMapsgraded.get("Due Date 0")
						.replace("pm", " pm"));
		Assert.assertEquals(getgradedAssignmentValues.get("lmspointValue 3")
				.concat(" pts"), mergeMapsgraded.get("LMSPoint value 0"));
		Assert.assertEquals(getgradedAssignmentValues.get("accdName 4") + ": "
				+ getgradedAssignmentValues.get("chapterName 4").toString(),
				mergeMapsgraded.get("Assignment Name 1").toString());
		Assert.assertEquals(
				GetDate.getDateandTime(
						getgradedAssignmentValues.get("datevalue 4")).replace(
						"pm", " pm"), mergeMapsgraded.get("Due Date 1")
						.replace("pm", " pm"));
		Assert.assertEquals(getgradedAssignmentValues.get("lmspointValue 4")
				.concat(" pts"), mergeMapsgraded.get("LMSPoint value 1"));
		Assert.assertEquals(getgradedAssignmentValues.get("accdName 5") + ": "
				+ getgradedAssignmentValues.get("chapterName 5").toString(),
				mergeMapsgraded.get("Assignment Name 2").toString());
		Assert.assertEquals(
				GetDate.getDateandTime(
						getgradedAssignmentValues.get("datevalue 5")).replace(
						"pm", " pm"), mergeMapsgraded.get("Due Date 2")
						.replace("pm", " pm"));
		Assert.assertEquals(getgradedAssignmentValues.get("lmspointValue 5")
				.concat(" pts"), mergeMapsgraded.get("LMSPoint value 2"));
		/*
		 * Assert.assertEquals(getgradedAssignmentValues.get("accdName 7") +
		 * ": " + getgradedAssignmentValues.get("chapterName 7").toString(),
		 * mergeMapsgraded.get("Assignment Name 3").toString());
		 * Assert.assertEquals(GetDate.getDateandTime(getgradedAssignmentValues
		 * .get("datevalue 7")).replace("pm", " pm"),
		 * mergeMapsgraded.get("Due Date 3") .replace("pm", " pm"));
		 * Assert.assertEquals(getgradedAssignmentValues.get("lmspointValue 7")
		 * .concat(" pts"), mergeMapsgraded.get("LMSPoint value 3"));
		 */

		LMSpage.clickNavigationlink("Modules");
		Thread.sleep(2000);
		LMSpage.getModuleItemTitle();
		LinkedHashMap<String, String> mergeMapsNongraded = new LinkedHashMap<String, String>();
		mergeMapsNongraded.putAll(LMSpage.getModuleItemTitle());
		Assert.assertEquals(getgradedAssignmentValues.get("accdNamenongrade 1")
				.concat(getgradedAssignmentValues.get("chapterNamenonGrade 1"))
				.replace(":", ""), mergeMapsNongraded.get("Module Name 0")
				.replaceAll("[-:] +", ""));
		String dltData = getgradedAssignmentValues.get("accdNamenongrade 2")
				.concat(getgradedAssignmentValues.get("chapterNamenonGrade 2"))
				.replace(":", "");

		String lmsData = mergeMapsNongraded.get("Module Name 1").replaceAll(
				"[-:] +", "");

		Assert.assertNotNull(dltData, lmsData);

		/*
		 * Assert.assertEquals(getgradedAssignmentValues.get("chapterNamenonGrade 9"
		 * ).replaceAll("[-:] +", ""),
		 * mergeMapsNongraded.get("Module Name 2").replaceAll("[-:] +", ""));
		 */

		try {
			ReusableMethods.setDLTEnvironment(driver);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		for (int i = 0; i < dltAccdNames.size(); i++) {
			nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
			Thread.sleep(2000);
			boolean elementexist = ReusableMethods
					.isdisplayed(
							driver,
							"//td[starts-with(@class,'_table_statusTimestamp')]/span[starts-with(.,'Sent')]");
			if (elementexist == true) {
				String sentDateValue = nlt.getSentDate();
				Assert.assertNotNull(sentDateValue);
				LogUtil.log(sentDateValue);
			}
			nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
		}
		String getValue = null;
		for (int i = 0; i < dltAccdNames.size(); i++) {
			nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
			Thread.sleep(2000);
			boolean isDueDate = nlt.isColumnName("Due Date");
			if (isDueDate == true) {
				nlt.selectLMSpointValue(0);
				getValue = nlt.getLMSpointValue(0);
				Thread.sleep(2000);
				ReusableMethods.clickTAB(driver);
				// nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
				break;
			}
			Thread.sleep(2000);
			nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
		}

		ReusableMethods.scrollToBottom(driver);
		nlt.clickreviewSendToLMS();
		Thread.sleep(1000);
		nlt.clickSendToLMS();
		Thread.sleep(2000);
		String titlenlt1 = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver, titlenlt1);
		ReusableMethods.switchToNewWindow(1, driver);
		// Click Assignment link.
		driver.navigate().refresh();
		Thread.sleep(5000);
		LMSpage.clickNavigationlink("Assignments");
		Thread.sleep(5000);
		LinkedHashMap<String, String> lmspointValues = new LinkedHashMap<String, String>();
		String lmspointvalue = null;
		;
		List<WebElement> listEle = driver
				.findElements(By
						.xpath("//div[@class='assignment-list']//div[contains(@class,'ig-details__item js-score')]/span[@class='non-screenreader']"));
		for (int i = listEle.size() - 1; i < listEle.size();) {
			try {
				lmspointvalue = listEle.get(i).getText();
			} catch (StaleElementReferenceException e) {
				// TODO Auto-generated catch block
				lmspointvalue = listEle.get(i).getText();
			}
			lmspointValues.put("LMSPoint value " + i, lmspointvalue);
			break;

		}
		Assert.assertEquals(getValue.concat(" pts"), lmspointvalue);
		Thread.sleep(5000);
		LMSpage.clickNavigationlink("Settings");
		driver.findElement(By.linkText("Delete this Course")).click();
		driver.findElement(
				By.xpath("//div[@class='form-actions']/button[@type='submit'][contains(text(),'Delete Course')]"))
				.click();
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
