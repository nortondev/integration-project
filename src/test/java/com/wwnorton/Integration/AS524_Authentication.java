package com.wwnorton.Integration;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.Objects.GearMenu_Page;
import com.wwnorton.Integration.Objects.LMSCANVAS;
import com.wwnorton.Integration.Objects.NLTPage;
import com.wwnorton.Integration.Objects.NciaDLP;
import com.wwnorton.Integration.Objects.RegisterPurchasePage;
import com.wwnorton.Integration.Objects.SmartWork5NewRegisteration;
import com.wwnorton.Integration.utlities.FakerNames;
import com.wwnorton.Integration.utlities.Mailinator;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.TestListener;

@Listeners({TestListener.class})
public class AS524_Authentication extends PropertiesFile 
{
	LMSCANVAS LMSpage;
	NLTPage nlt;
	String FirstName,LastName,emailID,Password,titleCode;
	WebElement getEmailcourseInvi = null;
	NciaDLP signinPage;
	SmartWork5NewRegisteration createaccount;
	List<WebElement> oRadiobutton;
	RegisterPurchasePage RP;
	GearMenu_Page gmlinks;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();
		
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("enjmusic13");
		
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify that user authentication window appears for the first time users and once authorized user should not see the authentication window")
	@Stories("Verify that user authentication window appears for the first time users and once authorized user should not see the authentication window")
	@Test()
	public void firstTimeAuthenticationInst() throws Exception{
		driver = getDriver();
		//String getrandonValue = GetRandomId.randomAlphaNumeric(3);
		signinPage = new NciaDLP();
		createaccount = new SmartWork5NewRegisteration();
		RP = new RegisterPurchasePage();
		LMSpage = new LMSCANVAS();
		nlt= new NLTPage();
		gmlinks = new GearMenu_Page();
		ReusableMethods.checkPageIsReady(driver);
		createaccount.gearButtonSignIn.click();		
		boolean isRadiobuttons = driver.findElements(By.name("register_login_choice")).size() >0;
		if(isRadiobuttons==true){
		oRadiobutton = driver.findElements(By.name("register_login_choice"));
		oRadiobutton.get(1).click();
		}
		createaccount.SignInButton.click();
		Thread.sleep(3000);
		FirstName = FakerNames.getStudentFName();
		LastName = FakerNames.getStudentFName();
		createaccount.FirstName_LastName.sendKeys(FirstName + " " + LastName);
		emailID = FirstName + "_" + LastName + "@mailinator.com";
		createaccount.StudentEmail.sendKeys(emailID);
		createaccount.StudentEmail.sendKeys(Keys.CONTROL + "t");
		Password = "TableTop1";
		RP.Password.sendKeys(Password);
		RP.Password2.sendKeys(Password);		
		Thread.sleep(3000);
		boolean isOKButton = driver.findElements(By.xpath("//button[@type='button']/span[contains(text(),'OK')]")).size() >0;
		if(isOKButton==true){
		RP.oKbutton.click();
		}
		RP.register_purchase_choice_trial.click();
		RP.Sign_Up_for_Trial_AccessButton.click();
		Thread.sleep(3000);		
		RP.OK_SignUpforTrialAccess_button.click();
		Thread.sleep(2000);
		RP.confirm_Registration(emailID.toLowerCase());
		RP.term_privacypolicy_popup();
		wait = new WebDriverWait(driver, 100);
		wait.until(ExpectedConditions.elementToBeClickable(RP.Get_Started));
		RP.Get_Started.click();
		//SignOut User 
		ReusableMethods.checkPageIsReady(driver);
		signinPage.SignOut();
		//Login as Inst and Authorize inst above email ID in NCIA
		Thread.sleep(5000);
		signinPage.clickSignin_Register();
		Thread.sleep(5000);
		Actions SignIn = new Actions(driver);
		SignIn.moveToElement(signinPage.yes_SignIn);
		SignIn.click();
		SignIn.perform();
		String UserName = jsonobject.getAsJsonObject("InstructorLogin")
				.get("Instloginemail").getAsString();
		signinPage.Username.sendKeys(UserName);
		signinPage.Password.sendKeys(jsonobject
				.getAsJsonObject("InstructorLogin").get("Instloginpassword")
				.getAsString());
		signinPage.SignInButton.click();
		ReusableMethods.checkPageIsReady(driver);  
		Thread.sleep(2000);
		wait = new WebDriverWait(driver, 50);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions
				.visibilityOf(signinPage.gear_button_username));
		boolean isLoader = driver.findElements(By.xpath("//div[@style='display:block;']")).size() >0;
		if(isLoader==true){
		WebElement loader = driver.findElement(By.xpath("//div[@style='display:block;']"));		
		ReusableMethods.loadingWaitDisapper(driver, loader);
		}
		signinPage.gear_button_username.click();
		gmlinks.AuthorizeInstructorAccount.click();
		RP.authorizeInst(FirstName, LastName, emailID.toLowerCase());
		ReusableMethods.checkPageIsReady(driver);
		signinPage.SignOut();
		ReusableMethods RM = new ReusableMethods();
		String courseName =RM.courseCreation(emailID.toLowerCase(),driver);
		Thread.sleep(2000);
		Mailinator m = new Mailinator(driver);
		m.SearchEmail(emailID.trim());
		getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(4000);
		String getEmailSubjectCourseInvi = "Course Invitation";		
		WebDriverWait wait = new WebDriverWait(driver,10);
		int counter = 0; //optional, just to cut off infinite waiting 
		while( getEmailcourseInvi == null && counter != 15 ){
		    try{
		    	getEmailcourseInvi = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[starts-with(@class,'table')]/tbody/tr/td[contains(.,'Course Invitation')]")));
		    } catch(TimeoutException te) {
		        System.out.println("Not loaded yet, continuing");
		        counter++;
		        driver.navigate().refresh();
		    }
		}

		getEmailcourseInvi = driver
				.findElement(By
						.xpath("//table[starts-with(@class,'table')]/tbody/tr/td[contains(.,'Course Invitation')]"));

		String getcourseInvifText = getEmailcourseInvi.getText().trim();
		Assert.assertEquals(getEmailSubjectCourseInvi, getcourseInvifText);
		getEmailcourseInvi.click();
		m.clickGetStarted();
		ReusableMethods.switchToNewWindow(3, driver);
		//LMSpage.dltInstloginLMSCANVAS();
		Thread.sleep(2000);
		LMSpage.clickAcceptButton();
		Thread.sleep(4000);
		LMSpage.clickCreateMyAccount();
		LMSpage.enterPassword("TableTop1");
		LMSpage.clickAgreecheckbox();
		LMSpage.clickRegisterButton();
		Thread.sleep(4000);
		ReusableMethods.closeCurrentWindow(3, driver);
		driver.close();
		ReusableMethods.closeCurrentWindow(2, driver);
		driver.close();
		ReusableMethods.switchToNewWindow(0, driver);
		LMSpage.dltNewInstloginLMSCANVAS(emailID.toLowerCase(),"TableTop1");
		Thread.sleep(4000);
		LMSpage.clickCouseNameLink(courseName);
		Thread.sleep(4000);
		LMSpage.clickToggleButton();
		Thread.sleep(4000);
		LMSpage.clickPublishButton();
		getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		LMSpage.selectCourseRadioOption("assignments");
		Thread.sleep(3000);
		LMSpage.choseandPublish();
		Thread.sleep(5000);
		String getpublished = LMSpage.published.getText();
		Assert.assertEquals("Published", getpublished.trim());
		Thread.sleep(3000);
		ReusableMethods.setDLTEnvironment(driver);
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		// login to NLT
		nlt.loginTestMakerApp(emailID.toLowerCase(),"TableTop1");
		List<String> getpara = new ArrayList<String>();
		getpara =nlt.getparaText();
		Assert.assertEquals(getpara.get(0), "To use our tool you need to authorize Canvas access to your account. You will only have to do this once.");
		Assert.assertEquals(getpara.get(1), "After clicking, \"Proceed to Authorize\" below, a Canvas link will open and you will need to click \"Authorize\". You will then be redirected back to our tool.");
		nlt.isCancelButton();
		nlt.isAuthorizeButton();
		nlt.clickAuthorizeButton();
		String headline = nlt.verifyLoginConfContent();
		//lower Environment WWNorton Deep Linking Tool
		//Production W.W. Norton Learning Tools	
		String getEnv = ReusableMethods.getDLTEnvironment(driver);
		if(getEnv.equalsIgnoreCase("PRODUCTION")) {
		assertTrue(headline.contains("W.W. Norton Learning Tools"));
		String loginpara = nlt.verifyLoginConfContentPara();
		assertTrue(loginpara.contains("W.W. Norton Learning Tools is requesting access to your account."));
		} else {
			assertTrue(headline.contains("WWNorton Deep Linking Tool"));
			String loginpara = nlt.verifyLoginConfContentPara();
			assertTrue(loginpara.contains("WWNorton Deep Linking Tool ("+getEnv+" - ES) is requesting access to your account."));
		}
		
		String userName = nlt.verifyUserNameLoginConfContent();
		Assert.assertEquals(userName, emailID.toLowerCase());
		nlt.clickAuthorizeButtonLoginConfContent();
		Assert.assertEquals(nlt.lmsText(), "Successfully connected to your Learning Management System (LMS).");
		String title = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver,title);
		ReusableMethods.switchToNewWindow(1, driver);
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
		Thread.sleep(3000);
		LMSpage.dltNewInstloginLMSCANVAS(emailID.toLowerCase(),"TableTop1");
		Thread.sleep(4000);
		LMSpage.clickCouseNameLink(courseName);
		Thread.sleep(3000);
		ReusableMethods.setDLTEnvironment(driver);
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Assert.assertEquals(nlt.lmsText(), "Successfully connected to your Learning Management System (LMS).");
		String title2 = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver,title2);
		ReusableMethods.switchToNewWindow(1, driver);
		Thread.sleep(5000);
		/*LMSpage.clickNavigationlink("Settings");
		Thread.sleep(1000);
		LMSpage.clickToggleArrow();
		driver.findElement(By.linkText("Delete this Course")).click();
		driver.findElement(
				By.xpath("//div[@class='form-actions']/button[@type='submit'][contains(text(),'Delete Course')]"))
				.click();*/
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
		String newCourse= RM.courseCreation(emailID.toLowerCase(),driver);
		Thread.sleep(3000);
		LMSpage.dltNewInstloginLMSCANVAS(emailID.toLowerCase(),"TableTop1");
		Thread.sleep(4000);
		LMSpage.clickCouseNameLink(newCourse);
		LMSpage.clickAcceptButton();
		LMSpage.clickToggleArrow();
		Thread.sleep(3000);
		LMSpage.clickPublishButton.click();
		getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		LMSpage.selectCourseRadioOption("assignments");
		Thread.sleep(3000);
		LMSpage.choseandPublish();
		Thread.sleep(5000);
		String getpublishedNew = LMSpage.published.getText();
		Assert.assertEquals("Published", getpublishedNew.trim());
		Thread.sleep(3000);
		ReusableMethods.setDLTEnvironment(driver);
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Assert.assertEquals(nlt.lmsText(), "Successfully connected to your Learning Management System (LMS).");
		String title3 = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver,title3);
		ReusableMethods.switchToNewWindow(1, driver);
		
		LMSpage.clickNavigationlink("Settings");
		Thread.sleep(4000);
		LMSpage.clickToggleButton();
		driver.findElement(By.linkText("Delete this Course")).click();
		driver.findElement(
				By.xpath("//div[@class='form-actions']/button[@type='submit'][contains(text(),'Delete Course')]"))
				.click();
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
		
	}
	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
