package com.wwnorton.Integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.Objects.GearMenu_Page;
import com.wwnorton.Integration.Objects.LMSCANVAS;
import com.wwnorton.Integration.Objects.NLTCoursePage;
import com.wwnorton.Integration.Objects.NLTPage;
import com.wwnorton.Integration.Objects.NciaDLP;
import com.wwnorton.Integration.Objects.RegisterPurchasePage;
import com.wwnorton.Integration.Objects.SmartWork5NewRegisteration;
import com.wwnorton.Integration.utlities.GetRandomId;
import com.wwnorton.Integration.utlities.LogUtil;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.TestListener;

@Listeners({ TestListener.class })
public class AS528_NOQUESelectCheckBoxAssignment extends PropertiesFile {
	LMSCANVAS LMSpage;
	NLTPage nlt;
	NLTCoursePage nltCourse;
	String FirstName, LastName, emailID, Password, titleCode, productTitle;
	WebElement getEmailcourseInvi = null;
	NciaDLP ncia;
	SmartWork5NewRegisteration createaccount;
	List<WebElement> oRadiobutton;
	RegisterPurchasePage RP;
	GearMenu_Page gmlinks;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();
	List<String> InQuizitiveChapList = new ArrayList<String>();
	List<String> nltIQChapList = new ArrayList<String>();
	Actions SignIn;

	@Parameters({ "browser" })
	@BeforeTest()
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS-528 On Assignment picker page ,user selects assignments for Review Activities by checking on the assignment")
	@Stories("AS-528 On Assignment picker page ,user selects assignments for Review Activities by checking on the assignment")
	@Test()
	public void checkboxSelectionforNOQUE() throws InterruptedException {
		driver = getDriver();
		String getDiscipline = jsonobject.getAsJsonObject("Bookinfo")
				.get("Discipline").getAsString();
		String getBookTitle =jsonobject.getAsJsonObject("Bookinfo")
				.get("BookTitle").getAsString();
		String getEdition = jsonobject.getAsJsonObject("Bookinfo")
				.get("Edition").getAsString();
		String subAccountlink = jsonobject.getAsJsonObject("SubAccountLink")
				.get("LinkName").getAsString();
		ncia = new NciaDLP();
		LMSpage = new LMSCANVAS();
		nltCourse = new NLTCoursePage();
		RP = new RegisterPurchasePage();
		nlt = new NLTPage();
		SignIn = new Actions(driver);
		try {
			ncia.clickSignin_Register();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SignIn.moveToElement(ncia.yes_SignIn);
		SignIn.click().perform();

		String UserName = jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserLogin").getAsString();
		ncia.Username.sendKeys(UserName);
		ncia.Password.sendKeys(jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserPassword").getAsString());
		ncia.SignInButton.click();
		ReusableMethods.checkPageIsReady(driver);
		try {
			ncia.clickDLPtile("Review Activities");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean isOkButton = driver
				.findElements(
						By.xpath("//button[@type='button']/span[contains(text(),'OK')]"))
				.size() > 0;
		if (isOkButton == true) {
			RP.oKbutton.click();
		}
		boolean isactivityDesc = driver
				.findElements(
						By.xpath("//div[@id='activity_group_description_title']/i|//div[@id='activity_group_description_title']"))
				.size() > 0;
		if (isactivityDesc == true) {
			productTitle = ncia.getProductTitle();
		} else {
			String edition = ncia.getProductEditon();
			productTitle = ncia.getProductTitle() + ", "
					+ ReusableMethods.upperCaseWords(edition);
		}
		InQuizitiveChapList = ncia.getDLPChapterlist();
		// if Alias is displayed

		ncia.clickDigitalResourceButton();
		try {
			ncia.SignOut();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.navigate().refresh();
		// Login into LMS and Select the Course and Click the Link

		try {
			LMSpage.navigateLMS();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		LMSpage.adminloginLMSCANVAS();
		LMSpage.clicklink("Admin");
		LMSpage.clickAdminlink("W.W. Norton");
		LMSpage.clickNavigationlink("Sub-Accounts");
		LMSpage.clickDLTlink(subAccountlink);
		// Click Course Button
		LMSpage.clickCourseButton();
		String getrandonValue = GetRandomId.randomAlphaNumeric(3);
		String courseName = "DEMOAUTOMATIONDLT " + getrandonValue;
		LMSpage.setDLTCourseName(courseName);
		LMSpage.setDLTReferenceCode("Demo");
		LMSpage.selectSubAccount();
		LMSpage.selectEnrollement();
		LMSpage.clickAddCourse();
		Thread.sleep(2000);
		LMSpage.selectCourseFilter();
		LMSpage.SearchCourse(courseName);
		Thread.sleep(2000);
		LMSpage.clickCourseLink(courseName);
		LMSpage.clickNavigationlink("People");
		LMSpage.clickPeopleButton();
		Thread.sleep(2000);
		LMSpage.selectRadioOption();
		LMSpage.enterEmailTextArea(jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserLogin").getAsString());
		LMSpage.selectRole("Teacher");
		LMSpage.clickNextButton();
		Thread.sleep(2000);
		LMSpage.clickAddUserButton();
		Assert.assertEquals("pending", LMSpage.getPendingText());
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
		Thread.sleep(2000);
		LMSpage.dltInstloginLMSCANVAS();
		Thread.sleep(2000);
		LMSpage.clickAcceptButton();
		Thread.sleep(1000);
		LMSpage.clickCouseNameLink(courseName);
		LMSpage.clickPublishButton.click();
		getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		LMSpage.selectCourseRadioOption("assignments");
		Thread.sleep(3000);
		LMSpage.choseandPublish();
		Thread.sleep(5000);
		String getpublished = LMSpage.published.getText();
		Assert.assertEquals("Published", getpublished.trim());
		Thread.sleep(3000);
		try {
			ReusableMethods.setDLTEnvironment(driver);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Assert.assertEquals(nlt.lmsText(),
				"Successfully connected to your Learning Management System (LMS).");
		Assert.assertNotNull(
				jsonobject.getAsJsonObject("DLTLMSUser")
						.get("LMSINSTUserLogin").getAsString(), nlt
						.getUserName().toLowerCase());
		nlt.clickContinue();
		nlt.selectState(5);
		Thread.sleep(1000);
		nlt.selectSchoolName(4);
		nlt.setCourseName(courseName);
		// Select Time Zone
		nlt.selectTimeZone("Eastern");
		Thread.sleep(3000);
		try {
			nlt.selectStartDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(2000);
		try {
			nlt.selectEndDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(2000);
		nlt.clickContinue();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseMaterialText(),
				"Select Course Material");
		nlt.selectSearchByBookTitleRadioOption();
		nlt.selectDiscipline(getDiscipline);
		nlt.selectBookTitle(getBookTitle);
		nlt.selectEdition(getEdition);
		Thread.sleep(2000);
		nlt.clickContinue();
		Assert.assertEquals(nlt.getconfirmBookpopup(), "Confirm Book");
		nlt.clickNobook();
		Assert.assertEquals(nlt.getCourseMaterialText(),
				"Select Course Material");
		Thread.sleep(2000);
		nlt.clickContinue();
		nlt.clickYesbook();
		Thread.sleep(2000);
		nlt.isSiteLicensePopUp();
		Assert.assertEquals(nlt.connCanvasText().trim(), "Connected to Canvas");
		nlt.clickCreateCourseButton();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseCreatedtext().trim(),
				"Your course has been created.");
		nltCourse.ClickSelectCouseButton();
		Assert.assertEquals(productTitle.trim(), nlt.accordionsName("Review")
				.toString().trim());
		// Expand the EBook accordian
		nlt.clickExpandButton("Review");
		Thread.sleep(2000);
		nltIQChapList = nlt.getNLTChapterlist("Review Activities");
		// compare DLP Chapter vs NLT Chapters for IQ

		@SuppressWarnings("unchecked")
		List<String> diffList = new ArrayList<>(CollectionUtils.disjunction(
				InQuizitiveChapList, nltIQChapList));
		LogUtil.log(diffList);

		@SuppressWarnings("unchecked")
		List<String> SameList = new ArrayList<>(CollectionUtils.intersection(
				InQuizitiveChapList, nltIQChapList));
		LogUtil.log(SameList);
		nlt.selectCheckBoxes(0, "Review Activities");
		int count = nlt.getCountCheckboxes("Review Activities");
		nlt.clickExpandButton("Review");
		Thread.sleep(2000);
		Assert.assertEquals(count, nlt.getCheckBoxesCount("Review Activities"));
		nlt.clickExpandButton("Review");
		Thread.sleep(2000);
		Assert.assertTrue(nlt.isCheckBoxSelected(0, "Review Activities"));
		nlt.selectCheckBoxes(-1, "Review Activities");
		int count1 = nlt.getCountCheckboxes("Review Activities");
		nlt.clickExpandButton("Review");
		Thread.sleep(2000);
		Assert.assertEquals(count1, nlt.getCheckBoxesCount("Review Activities"));
		nlt.clickExpandButton("Review");
		Thread.sleep(2000);
		Assert.assertTrue(nlt.isAssignmentNameChboxFocus("Review Activities"));
		Thread.sleep(2000);
		nlt.clickAssignmentNameChkbox();
		int assignmentcount = nlt.getCountCheckboxes("Review Activities");
		nlt.clickExpandButton("Review");
		Thread.sleep(2000);
		Assert.assertEquals(assignmentcount,
				nlt.getCheckBoxesCount("Review Activities"));
		nlt.clickExpandButton("Review");
		Thread.sleep(2000);
		nlt.clickAssignmentNameChkbox();
		int assignmentcount1 = nlt.getCountCheckboxes("Review Activities");
		Assert.assertEquals(assignmentcount1, 0);
		// Verify if the user selects due date and assignment checkbox is
		// automatically checked on
		nlt.selectDueDate(0);
		nlt.clickDueDateCancel();
		Assert.assertFalse(nlt.isCheckBoxSelected(0, "Review Activities"));
		nlt.selectDueDate(0);
		nlt.clickSetDueDatebutton();
		Assert.assertTrue(nlt.isCheckBoxSelected(0, "Review Activities"));
		// Uncheck the Check box and verify date is Not NULL
		nlt.selectCheckBoxes(0, "Review Activities");
		Assert.assertNotNull(nlt.getDateValue(), "The Date field  is NOT null");
		// Enter a date in all Due date section and verify Check boxes
		nlt.clickClearButton();
		nlt.selectDueDate(-1);
		int count2 = nlt.getCountCheckboxes("Review Activities");
		nlt.clickExpandButton("Review");
		Thread.sleep(2000);
		Assert.assertEquals(count2, nlt.getCheckBoxesCount("Review Activities"));
		// LMS point value update and Validate the check box
		nlt.clickExpandButton("Review");
		Thread.sleep(2000);
		nlt.clickAssignmentNameChkbox();
		nlt.selectCheckBoxes(0, "Review Activities");
		nlt.selectLMSpointValue(0);
		Thread.sleep(1000);
		Assert.assertTrue(nlt.isCheckBoxSelected(0, "Review Activities"));
		nlt.selectCheckBoxes(0, "Review Activities");
		Thread.sleep(1000);
		nlt.selectLMSpointValue(-1);
		int lmscount = nlt.getCountCheckboxes("Review Activities");
		nlt.clickExpandButton("Review");
		Thread.sleep(2000);
		Assert.assertNotNull(lmscount);
		nlt.clickExpandButton("Review");
		Thread.sleep(2000);
		nlt.clickAssignmentNameChkbox();
		Thread.sleep(2000);
		String getError = nlt.geterrorLMSpointValue(1, "9jh7");
		Assert.assertEquals("Invalid entry. Enter a number 1-999.", getError);
		String titlenlt = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver, titlenlt);
		ReusableMethods.switchToNewWindow(1, driver);
		Thread.sleep(5000);
		LMSpage.clickNavigationlink("Settings");
		driver.findElement(By.linkText("Delete this Course")).click();
		driver.findElement(
				By.xpath("//div[@class='form-actions']/button[@type='submit'][contains(text(),'Delete Course')]"))
				.click();
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}
}
