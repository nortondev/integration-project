package com.wwnorton.Integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.Objects.LMSCANVAS;
import com.wwnorton.Integration.Objects.NLTCoursePage;
import com.wwnorton.Integration.Objects.NLTPage;
import com.wwnorton.Integration.Objects.NciaDLP;
import com.wwnorton.Integration.Objects.SmartWork5NewRegisteration;
import com.wwnorton.Integration.utlities.DataProvider;
import com.wwnorton.Integration.utlities.GetRandomId;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.TestListener;
@Listeners({TestListener.class})
public class AS526_CompareDLPDLTAccordions extends PropertiesFile {
	LMSCANVAS LMSpage;
	NLTPage nlt;
	NLTCoursePage nltCourse;
	String FirstName, LastName, emailID, Password, titleCode, productTitle;
	WebElement getEmailcourseInvi = null;
	NciaDLP ncia;
	SmartWork5NewRegisteration createaccount;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();
	
	List<String> dlpselectorNamelist = new ArrayList<String>();
	List<String> nltAccordNameList = new ArrayList<String>();
	Actions SignIn;
	SoftAssert Softassrt;

	@Parameters({ "browser" })
	@BeforeTest()
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		Softassrt = new SoftAssert();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS-526 As user submits course creation details, system navigates to assignments picker page ,user verifies all accordians  with DLP")
	@Stories("AS-526 As user submits course creation details, system navigates to assignments picker page ,user verifies all accordians  with DLP")
	@Test()
	public void compareAccordionsDLTandDLP() throws InterruptedException  {		
		driver = getDriver();
		String getDiscipline = jsonobject.getAsJsonObject("Bookinfo")
				.get("Discipline").getAsString();
		String getBookTitle =jsonobject.getAsJsonObject("Bookinfo")
				.get("BookTitle").getAsString();
		String getEdition = jsonobject.getAsJsonObject("Bookinfo")
				.get("Edition").getAsString();
		String subAccountlink = jsonobject.getAsJsonObject("SubAccountLink")
				.get("LinkName").getAsString();
		ncia = new NciaDLP();
		LMSpage = new LMSCANVAS();
		nlt = new NLTPage();
		nltCourse = new NLTCoursePage();
		SignIn = new Actions(driver);
		List<String> productCodeList = null;
		try {
			productCodeList = DataProvider.readDataProviderEntlJson();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	
		for(int i=0; i<productCodeList.size(); i++){
			String productCodeName =productCodeList.get(i).toString();
			PropertiesFile.setURL(productCodeName);
			boolean isSigninButton = driver.findElements(By.xpath("//button[@id='login_button']")).size() >0;
			if(isSigninButton==true) {
			try {
				ncia.clickSignin_Register();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SignIn.moveToElement(ncia.yes_SignIn);
			SignIn.click().perform();

			String UserName = jsonobject.getAsJsonObject("DLTLMSUser")
					.get("LMSINSTUserLogin").getAsString();
			ncia.Username.sendKeys(UserName);
			ncia.Password.sendKeys(jsonobject.getAsJsonObject("DLTLMSUser")
					.get("LMSINSTUserPassword").getAsString());
			ncia.SignInButton.click();
			}
			ReusableMethods.checkPageIsReady(driver);
			dlpselectorNamelist =ncia.getProductCodeNames();
			boolean isactivityDesc = driver.findElements(By.xpath("//div[@id='activity_group_description_title']/i")).size() >0;
			if(isactivityDesc==true){
				productTitle=	ncia.getProductTitle();
			} else {
			String edition= ncia.getProductEditon();
			productTitle =ncia.getProductTitle()+", "+ ReusableMethods.upperCaseWords(edition);
		    }
			
			try {
				ncia.SignOut();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			driver.manage().deleteAllCookies();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			driver.navigate().refresh();
			// Login into LMS and Select the Course and Click the Link

			try {
				LMSpage.navigateLMS();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				LMSpage.adminloginLMSCANVAS();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				LMSpage.clicklink("Admin");
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LMSpage.clickAdminlink("W.W. Norton");
			LMSpage.clickNavigationlink("Sub-Accounts");
			LMSpage.clickDLTlink(subAccountlink);
			// Click Course Button
			LMSpage.clickCourseButton();
			String getrandonValue = GetRandomId.randomAlphaNumeric(3);
			String courseName = "DEMOAUTOMATIONDLT " + getrandonValue;
			LMSpage.setDLTCourseName(courseName);
			LMSpage.setDLTReferenceCode("Demo");
			LMSpage.selectSubAccount();
			LMSpage.selectEnrollement();
			LMSpage.clickAddCourse();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LMSpage.selectCourseFilter();
			LMSpage.SearchCourse(courseName);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LMSpage.clickCourseLink(courseName);
			LMSpage.clickNavigationlink("People");
			LMSpage.clickPeopleButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LMSpage.selectRadioOption();
			LMSpage.enterEmailTextArea(jsonobject.getAsJsonObject("DLTLMSUser")
					.get("LMSINSTUserLogin").getAsString());
			try {
				LMSpage.selectRole("Teacher");
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LMSpage.clickNextButton();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LMSpage.clickAddUserButton();
			Assert.assertEquals("pending", LMSpage.getPendingText());
			Thread.sleep(3000);
			LMSpage.clicklink("Account");
			Thread.sleep(3000);
			LMSpage.logoutLMS();
			Thread.sleep(2000);
			LMSpage.dltInstloginLMSCANVAS();
			Thread.sleep(2000);
			LMSpage.clickAcceptButton();
			Thread.sleep(1000);
			LMSpage.clickCouseNameLink(courseName);
			LMSpage.clickPublishButton.click();
			getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			LMSpage.selectCourseRadioOption("assignments");
			Thread.sleep(3000);
			LMSpage.choseandPublish();
			Thread.sleep(5000);
			String getpublished = LMSpage.published.getText();
			Assert.assertEquals("Published", getpublished.trim());
			Thread.sleep(3000);
			try {
				ReusableMethods.setDLTEnvironment(driver);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ReusableMethods.switchToNewWindow(2, driver);
			ReusableMethods.checkPageIsReady(driver);
			Assert.assertEquals(nlt.lmsText(),
					"Successfully connected to your Learning Management System (LMS).");
			Assert.assertNotNull(
					jsonobject.getAsJsonObject("DLTLMSUser")
							.get("LMSINSTUserLogin").getAsString(), nlt
							.getUserName().toLowerCase());
			nlt.clickContinue();
			nlt.selectState(5);
			Thread.sleep(1000);
			nlt.selectSchoolName(4);
			nlt.setCourseName(courseName);
			// Select Time Zone
			nlt.selectTimeZone("Eastern");
			Thread.sleep(3000);
			try {
				nlt.selectStartDate();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Thread.sleep(2000);
			try {
				nlt.selectEndDate();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Thread.sleep(2000);
			nlt.clickContinue();
			Thread.sleep(2000);
			Assert.assertEquals(nlt.getCourseMaterialText(),
					"Select Course Material");
			nlt.selectSearchByBookTitleRadioOption();
			nlt.selectDiscipline(getDiscipline);
			nlt.selectBookTitle(getBookTitle);
			nlt.selectEdition(getEdition);
			Thread.sleep(2000);
			nlt.clickContinue();
			Assert.assertEquals(nlt.getconfirmBookpopup(), "Confirm Book");
			nlt.clickNobook();
			Assert.assertEquals(nlt.getCourseMaterialText(), "Select Course Material");
			Thread.sleep(2000);
			nlt.clickContinue();
			nlt.clickYesbook();
			Thread.sleep(2000);
			nlt.isSiteLicensePopUp();
			Assert.assertEquals(nlt.connCanvasText().trim(), "Connected to Canvas");
			nlt.clickCreateCourseButton();
			Thread.sleep(2000);
			Assert.assertEquals(nlt.getCourseCreatedtext().trim(),
					"Your course has been created.");
			nltCourse.ClickSelectCouseButton();
			//Compare the Accordions Name vs DLP 
			ReusableMethods.checkPageIsReady(driver);
			nltAccordNameList =nlt.accordionsNameList();
			//Soft Assertion
			
			for(int dlp=0; dlp<dlpselectorNamelist.size(); dlp++){
				if(dlpselectorNamelist.get(dlp).startsWith("Ebook")){
					String str = dlpselectorNamelist.get(dlp);
					String newStr = str.replace(str, "");
					dlpselectorNamelist.set(dlp, "Ebook: " + newStr.concat(productTitle));
				}
				else if(dlpselectorNamelist.get(dlp).endsWith("Ebook")){
					String str = dlpselectorNamelist.get(dlp);
					dlpselectorNamelist.set(dlp, "Ebook: " + str.replace("Ebook", ""));
				}
					for(int dlt=0; dlt<nltAccordNameList.size(); dlt++){
						if(nltAccordNameList.get(dlt).contains("Getting Started")) {
						dlt = dlt +1;	
						} 
						if(dlt>=dlp){
							String dlpstr=dlpselectorNamelist.get(dlp).trim();
							String dltstr=nltAccordNameList.get(dlt).trim();
							if(dlpstr.equals(dltstr)){
								Softassrt.assertEquals(dltstr,dlpstr);
								System.out.println("The values are  " +dlpstr+ "equal " +dltstr);
							}else {
								Softassrt.assertEquals(dltstr,dlpstr);
								System.out.println("The values are  " +dlpstr+ " NOT equal " +dltstr);

								
							}
						}
						dlp++;
				}
			}
		
			Thread.sleep(5000);
			String titlenlt = driver.getTitle();
			ReusableMethods.SwitchTabandClose(driver, titlenlt);
			ReusableMethods.switchToNewWindow(1, driver);
			Thread.sleep(2000);
			LMSpage.clickNavigationlink("Settings");
			driver.findElement(By.linkText("Delete this Course")).click();
			driver.findElement(
					By.xpath("//div[@class='form-actions']/button[@type='submit'][contains(.,'Delete Course')]"))
					.click();
			Thread.sleep(3000);
			LMSpage.clicklink("Account");
			Thread.sleep(3000);
			LMSpage.logoutLMS();
			driver.manage().deleteAllCookies();
			driver.navigate().refresh();
			
		}
		Softassrt.assertAll();		
		
	}
	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}

}
