package com.wwnorton.Integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.Objects.GearMenu_Page;
import com.wwnorton.Integration.Objects.LMSCANVAS;
import com.wwnorton.Integration.Objects.NLTCoursePage;
import com.wwnorton.Integration.Objects.NLTPage;
import com.wwnorton.Integration.Objects.NciaDLP;
import com.wwnorton.Integration.Objects.RegisterPurchasePage;
import com.wwnorton.Integration.Objects.SmartWork5NewRegisteration;
import com.wwnorton.Integration.utlities.LogUtil;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.TestListener;

@Listeners({ TestListener.class })
public class AS530_SendtoLMS extends PropertiesFile {
	LMSCANVAS LMSpage;
	NLTPage nlt;
	NLTCoursePage nltCourse;
	String FirstName, LastName, emailID, Password, titleCode, productTitle;
	WebElement getEmailcourseInvi = null;
	NciaDLP ncia;
	SmartWork5NewRegisteration createaccount;
	List<WebElement> oRadiobutton;
	RegisterPurchasePage RP;
	GearMenu_Page gmlinks;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();
	List<String> InQuizitiveChapList = new ArrayList<String>();
	List<String> nltIQChapList = new ArrayList<String>();
	Actions SignIn;

	@Parameters({ "browser" })
	@BeforeTest()
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS-530 After selecting assignments, user clicks on sent to LMS button, user should be navigated to overlay screen which displays no of items selected under each accordian")
	@Stories("AS-530 After selecting assignments, user clicks on sent to LMS button, user should be navigated to overlay screen which displays no of items selected under each accordian")
	@Test()
	public void sendToLMS() throws InterruptedException {
		driver = getDriver();
		String getDiscipline = jsonobject.getAsJsonObject("Bookinfo")
				.get("Discipline").getAsString();
		String getBookTitle = jsonobject.getAsJsonObject("Bookinfo")
				.get("BookTitle").getAsString();
		String getEdition = jsonobject.getAsJsonObject("Bookinfo")
				.get("Edition").getAsString();
		LMSpage = new LMSCANVAS();
		nlt = new NLTPage();
		nltCourse = new NLTCoursePage();
		ReusableMethods RM = new ReusableMethods();
		emailID = jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserLogin").getAsString();
		String courseName = RM.courseCreation(emailID, driver);
		LogUtil.log(courseName);
		Thread.sleep(2000);
		LMSpage.dltInstloginLMSCANVAS();
		Thread.sleep(2000);
		LMSpage.clickAcceptButton();
		Thread.sleep(1000);
		LMSpage.clickCouseNameLink(courseName);
		LMSpage.clickPublishButton.click();
		getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		LMSpage.selectCourseRadioOption("assignments");
		Thread.sleep(3000);
		LMSpage.choseandPublish();
		Thread.sleep(5000);
		String getpublished = LMSpage.published.getText();
		Assert.assertEquals("Published", getpublished.trim());
		Thread.sleep(3000);
		try {
			ReusableMethods.setDLTEnvironment(driver);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Assert.assertEquals(nlt.lmsText(),
				"Successfully connected to your Learning Management System (LMS).");
		Assert.assertNotNull(
				jsonobject.getAsJsonObject("DLTLMSUser")
						.get("LMSINSTUserLogin").getAsString(), nlt
						.getUserName().toLowerCase());
		nlt.clickContinue();
		nlt.selectState(5);
		Thread.sleep(1000);
		nlt.selectSchoolName(4);
		nlt.setCourseName(courseName);
		// Select Time Zone
		nlt.selectTimeZone("Eastern");
		Thread.sleep(3000);
		try {
			nlt.selectStartDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(2000);
		try {
			nlt.selectEndDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(2000);
		nlt.clickContinue();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseMaterialText(),
				"Select Course Material");
		ReusableMethods.checkPageIsReady(driver);
		nlt.selectSearchByBookTitleRadioOption();
		nlt.selectDiscipline(getDiscipline);
		nlt.selectBookTitle(getBookTitle);
		nlt.selectEdition(getEdition);
		Thread.sleep(2000);
		nlt.clickContinue();
		Assert.assertEquals(nlt.getconfirmBookpopup(), "Confirm Book");
		nlt.clickNobook();
		Assert.assertEquals(nlt.getCourseMaterialText(),
				"Select Course Material");
		Thread.sleep(2000);
		nlt.clickContinue();
		nlt.clickYesbook();
		Thread.sleep(2000);
		nlt.isSiteLicensePopUp();
		Assert.assertEquals(nlt.connCanvasText().trim(), "Connected to Canvas");
		nlt.clickCreateCourseButton();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseCreatedtext().trim(),
				"Your course has been created.");
		nltCourse.ClickSelectCouseButton();
		// Verify Review and Send to LMS button is disabled intially
		Assert.assertFalse(nlt.reviewSendToLMS(), " Button is disabled");
		// Get the List of accordion and expand one by one and select the Check
		// boxes randomly
		nlt.clickCheckboxes_accordion();
		Assert.assertTrue(nlt.reviewSendToLMS(), " Button is Enabled");
		// Compare the count after clicking Review and Send to LMS button is
		// clicked
		LinkedHashMap<String, Integer> AssignmentSelTable = nlt
				.getAccordionValues();
		nlt.clickreviewSendToLMS();
		LinkedHashMap<String, Integer> lmsPopSection = nlt
				.getAccordionValuesLMSPop();
		// Compare 2 HashMaps
		boolean isEqual = AssignmentSelTable.equals(lmsPopSection);
		LogUtil.log("Both Tables are Equal " + isEqual);
		Assert.assertEquals(nlt.getHeaderTitle(), "Send Course Content to LMS");
		Assert.assertTrue(nlt.isCancel(), " Cancel Button is Displayed");
		Assert.assertTrue(nlt.isSendtoLMS(), " Send To LMS Button is Displayed");
		// click Cancel button
		nlt.clickCancel();
		Assert.assertTrue(nlt.reviewSendToLMS(), " Button is Enabled");
		nlt.clickreviewSendToLMS();
		nlt.clickSendToLMS();
		Thread.sleep(5000);
		Assert.assertEquals("We are sending your content to your LMS.",
				nlt.getContentTextLMS());
		List<String> contentPoints = nlt.getContentPointsLMS();
		List<String> contentRefreshLMSchild = nlt
				.getContentPointsRefreshLMSchild();
		Assert.assertEquals(contentPoints.get(0).toString(),
				"Refresh your LMS course, and confirm that your content appears.");
		Assert.assertEquals(contentRefreshLMSchild.get(0).toString(),
				"Graded activities will appear in the Assignments section.");
		Assert.assertEquals(contentRefreshLMSchild.get(1).toString(),
				"Ungraded links will appear in the Modules section.");
		Assert.assertEquals(contentPoints.get(1).toString(),
				"Edit and move your content within your LMS.");
		Assert.assertEquals(contentPoints.get(2).toString(),
				"Relaunch this tool from your LMS to send more content.");
		nlt.clickLink("LMS Integration Help Notes");
		ReusableMethods.switchToNewWindow(3, driver);
		WebElement getCanvasTitle = driver.findElement(By
				.xpath("//h1[@class='hg-article-title']"));
		Assert.assertEquals(getCanvasTitle.getText().toString(),
				"Canvas LMS Integration with Norton Learning Tools");
		String title = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver, title);
		ReusableMethods.switchToNewWindow(2, driver);
		nlt.clickLink("Norton Representative");
		ReusableMethods.switchToNewWindow(3, driver);
		WebElement getWwNortonTitle = driver.findElement(By
				.xpath("//div[contains(@class,'headerDefault')]/h1/p"));
		Assert.assertEquals(getWwNortonTitle.getText().toString(),
				"Find Your Rep");
		title = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver, title);
		ReusableMethods.switchToNewWindow(2, driver);
		String titlenlt = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver, titlenlt);
		ReusableMethods.switchToNewWindow(1, driver);
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}
}
