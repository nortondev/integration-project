package com.wwnorton.Integration;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.Objects.CreateNewStudentSet;
import com.wwnorton.Integration.Objects.GearMenu_Page;
import com.wwnorton.Integration.Objects.LMSCANVAS;
import com.wwnorton.Integration.Objects.NLTCoursePage;
import com.wwnorton.Integration.Objects.NLTPage;
import com.wwnorton.Integration.Objects.NciaDLP;
import com.wwnorton.Integration.Objects.RegisterPurchasePage;
import com.wwnorton.Integration.Objects.SmartWork5NewRegisteration;
import com.wwnorton.Integration.utlities.GetDate;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.TestListener;
import com.wwnorton.Integration.utlities.Timezone;

@Listeners({ TestListener.class })
public class AS542_DayLightSaving extends PropertiesFile {
	LMSCANVAS LMSpage;
	CreateNewStudentSet SSID;
	NLTPage nlt;
	NLTCoursePage nltCourse;
	NciaDLP ncia;
	String FirstName, LastName, emailID, Password, timezone, titleCode,
			winHandleBefore, getNCIADate, getDLtdate, dateBeforeDayLight,
			dateonDayLight, dateAfterDaylight;
	WebElement getEmailcourseInvi = null;
	String getDate1, getDate2, getDate3, nciaGAUDate1, nciaGAUDate2,
			nciaGAUDate3, nltDateIQ;
	NciaDLP signinPage;
	SmartWork5NewRegisteration createaccount;
	List<WebElement> oRadiobutton;
	RegisterPurchasePage RP;
	GearMenu_Page gmlinks;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();

	Actions SignIn;
	public static final String assignmentTitleName = "Chapter 17";
	List<String> dltAccdNames = new ArrayList<String>();
	LinkedHashMap<String, String> getgradedAssignmentValues,
			getgradedastValueUpdate;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		// PropertiesFile.setURL("enjmusic13");

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS-542 Create a student set in Daylight saving timezone and verify the set due date are displayed correctly ")
	@Stories("AS-542 Create a student set in Daylight saving timezone and verify the set due date are displayed correctly ")
	@Test()
	public void daylightTimeZone() throws InterruptedException, ParseException {
		driver = getDriver();
		String getDiscipline = jsonobject.getAsJsonObject("Bookinfo")
				.get("Discipline").getAsString();
		String getBookTitle = jsonobject.getAsJsonObject("Bookinfo")
				.get("BookTitle").getAsString();
		String getEdition = jsonobject.getAsJsonObject("Bookinfo")
				.get("Edition").getAsString();
		dateBeforeDayLight = jsonobject.getAsJsonObject("DayLightDates")
				.get("DateBeforeDayLight").getAsString();
		dateonDayLight = jsonobject.getAsJsonObject("DayLightDates")
				.get("DateOnDayLight").getAsString();
		dateAfterDaylight = jsonobject.getAsJsonObject("DayLightDates")
				.get("DateAfterDayLight").getAsString();
		LMSpage = new LMSCANVAS();
		nlt = new NLTPage();
		nltCourse = new NLTCoursePage();
		ncia = new NciaDLP();
		SSID = new CreateNewStudentSet();
		ReusableMethods RM = new ReusableMethods();
		emailID = jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserLogin").getAsString();
		String courseName = RM.courseCreation(emailID, driver);
		LMSpage.dltInstloginLMSCANVAS();
		Thread.sleep(2000);
		LMSpage.clickAcceptButton();
		Thread.sleep(1000);
		LMSpage.clickCouseNameLink(courseName);
		LMSpage.clickPublishButton.click();
		getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		LMSpage.selectCourseRadioOption("assignments");
		Thread.sleep(3000);
		LMSpage.choseandPublish();
		Thread.sleep(5000);
		String getpublished = LMSpage.published.getText();
		Assert.assertEquals("Published", getpublished.trim());
		Thread.sleep(3000);
		try {
			ReusableMethods.setDLTEnvironment(driver);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Assert.assertEquals(nlt.lmsText(),
				"Successfully connected to your Learning Management System (LMS).");
		Assert.assertNotNull(
				jsonobject.getAsJsonObject("DLTLMSUser")
						.get("LMSINSTUserLogin").getAsString(), nlt
						.getUserName().toLowerCase());
		nlt.clickContinue();
		nlt.selectState(5);
		Thread.sleep(1000);
		nlt.selectSchoolName(4);
		nlt.setCourseName(courseName);
		// Select Time Zone
		nlt.selectTimeZone("Eastern");
		timezone = nlt.getTimeZone();
		Thread.sleep(3000);
		try {
			nlt.selectStartDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(2000);
		try {
			nlt.selectDaylightEndDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(2000);
		nlt.clickContinue();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseMaterialText(),
				"Select Course Material");
		nlt.selectSearchByBookTitleRadioOption();
		nlt.selectDiscipline(getDiscipline);
		nlt.selectBookTitle(getBookTitle);
		nlt.selectEdition(getEdition);
		Thread.sleep(2000);
		nlt.clickContinue();
		Assert.assertEquals(nlt.getconfirmBookpopup(), "Confirm Book");
		nlt.clickNobook();
		Assert.assertEquals(nlt.getCourseMaterialText(),
				"Select Course Material");
		Thread.sleep(2000);
		nlt.clickContinue();
		nlt.clickYesbook();
		Thread.sleep(2000);
		nlt.isSiteLicensePopUp();
		Assert.assertEquals(nlt.connCanvasText().trim(), "Connected to Canvas");
		nlt.clickCreateCourseButton();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseCreatedtext().trim(),
				"Your course has been created.");
		nltCourse.ClickSelectCouseButton();
		// Verify Review and Send to LMS button is disabled intially
		ReusableMethods.scrollToBottom(driver);
		Assert.assertFalse(nlt.reviewSendToLMS(), " Button is disabled");
		// for Ebook UnGraded Click the Check boxes login if Check box and LMS
		// point values are displayed then Enter else select checkbox
		dltAccdNames = nlt.accordionsNameList();
		// Also enter the values for Due date
		for (int i = 0; i < dltAccdNames.size(); i++) {
			if (dltAccdNames.get(i).contains("InQuizitive")) {
				nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
				Thread.sleep(2000);
				boolean isDueDate = nlt.isColumnName("Due Date");
				if (isDueDate == true) {
					nlt.selectDueDate_jsondata1(3, dateBeforeDayLight);
					Thread.sleep(2000);
					nlt.setDueDate("9:00 AM");
					nlt.clickSetDueDatebutton();
					Thread.sleep(2000);
					nlt.selectDueDate_jsondata1(4, dateonDayLight);
					Thread.sleep(2000);
					nlt.setDueDate("1:00 PM");
					nlt.clickSetDueDatebutton();
					Thread.sleep(2000);
					nlt.selectDueDate_jsondata1(5, dateAfterDaylight);
					Thread.sleep(2000);
					nlt.setDueDate("5:00 PM");
					nlt.clickSetDueDatebutton();
					Thread.sleep(2000);
					nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
					break;
				}
				Thread.sleep(2000);
				nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
			}
		}
		getgradedAssignmentValues = new LinkedHashMap<String, String>();
		for (int i = 0; i < dltAccdNames.size(); i++) {
			if (dltAccdNames.get(i).contains("InQuizitive")) {
				nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
				Thread.sleep(3000);
				boolean isdisplayed = driver
						.findElements(
								By.xpath("//p[contains(@class,'_groupAssignmentPicker_icon-text')]"))
						.size() > 0;
				if (isdisplayed == true) {
					String nocontentText = nlt.contentNoQUE();
					Assert.assertEquals(
							"This content is unavailable right now.",
							nocontentText.trim());
					nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
					continue;
				}
				boolean isDueDate = nlt.isColumnName("Due Date");
				if (isDueDate == true) {
					List<WebElement> tableSelectedList = driver
							.findElements(By
									.xpath("//ancestor-or-self::tr[contains(@class,'table_selected')]"));
					for (int t = 0; t < tableSelectedList.size(); t++) {
						getgradedAssignmentValues.put("chapterName " + t,
								nlt.getChapterName(t));
						if (t == 0) {
							getgradedAssignmentValues.put("datevalue " + t,
									nlt.getDateValue(t, t + 1));
						} else if (t == 1) {
							getgradedAssignmentValues.put("datevalue " + t,
									nlt.getDateValue(t + 1, t + 2));
						} else if (t == 2) {
							getgradedAssignmentValues.put("datevalue " + t,
									nlt.getDateValue(t + 2, t + 3));
						}
					}

				}
				Thread.sleep(2000);
				nlt.clickAccdExpandButton(dltAccdNames.get(i).toString());
			}

			continue;

		}
		Thread.sleep(2000);
		try {
			nlt.clickreviewSendToLMS();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(1000);
		nlt.clickSendToLMS();
		Thread.sleep(2000);
		String titlenlt = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver, titlenlt);
		ReusableMethods.switchToNewWindow(1, driver);
		driver.navigate().refresh();
		Thread.sleep(5000);
		ReusableMethods.checkPageIsReady(driver);
		LMSpage.clickNavigationlink("Assignments");
		ReusableMethods.checkPageIsReady(driver);
		getDate1 = GetDate.getDateandTime(getgradedAssignmentValues
				.get("datevalue 0"));

		String getDateLMS1 = LMSpage.getassignmentDueDateMap()
				.get("Due Date 0").replace(",", "");
		Assert.assertEquals(getDate1.toString(), getDateLMS1.toString());
		getDate2 = GetDate.getDateandTime(getgradedAssignmentValues
				.get("datevalue 1"));

		String getDateLMS2 = LMSpage.getassignmentDueDateMap()
				.get("Due Date 1").replace(",", "");
		Assert.assertEquals(getDate2.toString(), getDateLMS2.toString());
		getDate3 = GetDate.getDateandTime(getgradedAssignmentValues
				.get("datevalue 2"));

		String getDateLMS3 = LMSpage.getassignmentDueDateMap()
				.get("Due Date 2").replace(",", "");
		Assert.assertEquals(getDate3.toString(), getDateLMS3.toString());

		LMSpage.clickCreatedAssignmentLink();
		LMSpage.clickLoadAssignmentButton();
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		String url = driver.getCurrentUrl();
		// updateURL
		String index = url.substring(url.lastIndexOf("/"));
		String newindex = url.replace(index, "/enjmusic13");
		driver.get(newindex);
		// ReusableMethods.loadingWaitDisapper(driver, ncia.Loader);
		ReusableMethods.checkPageIsReady(driver);
		try {
			ncia.clickDLPtile("InQuizitive");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ncia.clickOKButton();
		try {
			SSID.selectByPartOfVisibleText(courseName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// get the Date for the assignment
		nciaGAUDate1 = ncia.getGAUDate(0);
		String dltDate1 = GetDate.convertDate(getgradedAssignmentValues
				.get("datevalue 0"));
		Assert.assertEquals(dltDate1, nciaGAUDate1);
		nciaGAUDate2 = ncia.getGAUDate(1);
		String dltDate2 = GetDate.convertDate(getgradedAssignmentValues
				.get("datevalue 1"));
		Assert.assertEquals(dltDate2, nciaGAUDate2);
		nciaGAUDate3 = ncia.getGAUDate(2);
		String dltDate3 = GetDate.convertDate(getgradedAssignmentValues
				.get("datevalue 2"));
		Assert.assertEquals(dltDate3, nciaGAUDate3);
		Thread.sleep(4000);
		// select different chapters for IQ
		ncia.clickSetLink(assignmentTitleName);
		// 30 Min add
		try {
			ncia.SelectCurrentGauDate();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Thread.sleep(2000);
		ReusableMethods.clickTAB(driver);
		Thread.sleep(1000);
		ncia.getHourGAUTime();
		Thread.sleep(1000);
		try {
			ncia.selectTimeZone("Mountain");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ncia.clickSGAUntilbutton();
		Thread.sleep(2000);
		String nciaMountainDate = ncia.getGAUDate(0);
		System.out.println("The Mountain Time is  " + nciaMountainDate);
		String nciaTitle = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver, nciaTitle);
		ReusableMethods.switchToNewWindow(1, driver);
		driver.navigate().refresh();
		Thread.sleep(2000);
		try {
			ReusableMethods.setDLTEnvironment(driver);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Navigate back to DLT
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		getgradedastValueUpdate = new LinkedHashMap<String, String>();
		for (int j = 0; j < dltAccdNames.size(); j++) {
			if (dltAccdNames.get(j).contains("InQuizitive")) {
				nlt.clickAccdExpandButton(dltAccdNames.get(j).toString());
				Thread.sleep(3000);
				nltDateIQ = nlt.getChapterBasedDateValue(assignmentTitleName);
				nlt.checkCheckBox(assignmentTitleName);
				Thread.sleep(2000);
				nlt.clickAccdExpandButton(dltAccdNames.get(j).toString());
			}
			continue;
		}
		Thread.sleep(2000);
		String nydate = Timezone.timeConversionZone(nciaMountainDate,
				"America/Denver", "America/New_York");
		Assert.assertEquals(GetDate.convertDate(nltDateIQ), nydate);
		Thread.sleep(2000);
		nlt.clickreviewSendToLMS();
		Thread.sleep(1000);
		nlt.clickSendToLMS();
		Thread.sleep(2000);
		String titlenlt1 = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver, titlenlt1);
		ReusableMethods.switchToNewWindow(1, driver);
		driver.navigate().refresh();
		Thread.sleep(5000);
		ReusableMethods.checkPageIsReady(driver);
		LMSpage.clickNavigationlink("Assignments");
		ReusableMethods.checkPageIsReady(driver);
		String getDateLMSIQ = LMSpage.getassignmentDueDateMap().get(
				"Due Date 3");
		String getIQDate = GetDate.getDateandTime(nltDateIQ);
		String cYear = GetDate.getCurrentYear();
		if(getIQDate.contains(cYear)){
		 String newDateIQ  = getIQDate.replace(cYear, "");
		 Assert.assertEquals(newDateIQ.replaceAll("\\s", "").toString(), getDateLMSIQ.replaceAll("\\s", "").toString());
		} else {
		Assert.assertEquals(getIQDate.toString(), getDateLMSIQ.toString());
		}

		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
	}

	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}
}
