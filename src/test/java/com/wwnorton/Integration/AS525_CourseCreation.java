package com.wwnorton.Integration;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import com.google.gson.JsonObject;
import com.wwnorton.Integration.Objects.GearMenu_Page;
import com.wwnorton.Integration.Objects.LMSCANVAS;
import com.wwnorton.Integration.Objects.NLTCoursePage;
import com.wwnorton.Integration.Objects.NLTPage;
import com.wwnorton.Integration.Objects.NciaDLP;
import com.wwnorton.Integration.Objects.RegisterPurchasePage;
import com.wwnorton.Integration.Objects.SmartWork5NewRegisteration;
import com.wwnorton.Integration.utlities.GetRandomId;
import com.wwnorton.Integration.utlities.LogUtil;
import com.wwnorton.Integration.utlities.PropertiesFile;
import com.wwnorton.Integration.utlities.ReadUIJsonFile;
import com.wwnorton.Integration.utlities.ReusableMethods;
import com.wwnorton.Integration.utlities.TestListener;

@Listeners({TestListener.class})
public class AS525_CourseCreation extends PropertiesFile 
{
	LMSCANVAS LMSpage;
	NLTPage nlt;
	NLTCoursePage nltCourse;
	String FirstName,LastName,emailID,Password,timezone,titleCode, bookCode,bookTitle,bookSubTitle,bookEditon, bookAuthor;
	WebElement getEmailcourseInvi = null;
	NciaDLP signinPage;
	SmartWork5NewRegisteration createaccount;
	List<WebElement> oRadiobutton;
	RegisterPurchasePage RP;
	GearMenu_Page gmlinks;
	ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJasonObject.readUIJson();
		
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		//PropertiesFile.setURL("enjmusic13");
		
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("AS-525 As user logs in ,course creation page appears, user enters course creation details and product details and  submits")
	@Stories("AS-525 As user logs in ,course creation page appears, user enters course creation details and product details and  submits")
	@Test()
	public void courseCreation() throws Exception{
		driver = getDriver();
		String getDiscipline = jsonobject.getAsJsonObject("Bookinfo")
				.get("Discipline").getAsString();
		String getBookTitle =jsonobject.getAsJsonObject("Bookinfo")
				.get("BookTitle").getAsString();
		String getEdition = jsonobject.getAsJsonObject("Bookinfo")
				.get("Edition").getAsString();
		
		String subAccountlink = jsonobject.getAsJsonObject("SubAccountLink")
				.get("LinkName").getAsString();
		String getrandonValue = GetRandomId.randomAlphaNumeric(3);
		LMSpage = new LMSCANVAS();
		nlt= new NLTPage();
		nltCourse = new NLTCoursePage();
		LMSpage.navigateLMS();
		LMSpage.adminloginLMSCANVAS();
		LMSpage.clicklink("Admin");
		LMSpage.clickAdminlink("W.W. Norton");
		LMSpage.clickNavigationlink("Sub-Accounts");
		LMSpage.clickDLTlink(subAccountlink);
		//Click Course Button 
		LMSpage.clickCourseButton();
		String courseName = "DEMOAUTOMATIONDLT " + getrandonValue;
		LMSpage.setDLTCourseName(courseName);
		LMSpage.setDLTReferenceCode("Demo");
		LMSpage.selectSubAccount();
		LMSpage.selectEnrollement();
		LMSpage.clickAddCourse();
		Thread.sleep(2000);
		LMSpage.selectCourseFilter();
		LMSpage.SearchCourse(courseName);
		Thread.sleep(2000);
		LMSpage.clickCourseLink(courseName);
		LMSpage.clickNavigationlink("People");
		LMSpage.clickPeopleButton();
		Thread.sleep(2000);
		LMSpage.selectRadioOption();
		LMSpage.enterEmailTextArea(jsonobject.getAsJsonObject("DLTLMSUser")
				.get("LMSINSTUserLogin").getAsString());
		LMSpage.selectRole("Teacher");
		LMSpage.clickNextButton();
		Thread.sleep(2000);
		LMSpage.clickAddUserButton();
		Assert.assertEquals("pending", LMSpage.getPendingText());
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
		Thread.sleep(2000);
		LMSpage.dltInstloginLMSCANVAS();
		Thread.sleep(2000);
		LMSpage.clickAcceptButton();
		Thread.sleep(1000);
		LMSpage.clickCouseNameLink(courseName);
		LMSpage.clickPublishButton.click();
		getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		LMSpage.selectCourseRadioOption("assignments");
		Thread.sleep(3000);
		LMSpage.choseandPublish();
		Thread.sleep(5000);
		String getpublished = LMSpage.published.getText();
		Assert.assertEquals("Published", getpublished.trim());
		Thread.sleep(3000);
		ReusableMethods.setDLTEnvironment(driver);
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Assert.assertEquals(nlt.lmsText(), "Successfully connected to your Learning Management System (LMS).");
		//Assert.assertEquals(jsonobject.getAsJsonObject("DLTLMSUser").get("LMSINSTUserLogin").getAsString(), nlt.getUserName().toLowerCase());
		nlt.clickContinue();
		Assert.assertEquals(nlt.getCoursedetailspage(), "Enter Course Details");
		ReusableMethods.scrollToBottom(driver);
		nlt.clickPrevious();
		Thread.sleep(2000);
		nlt.clickContinue();
		//Validate the Required Fields
		Thread.sleep(2000);
		ReusableMethods.scrollToBottom(driver);
		nlt.clickContinue();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.validateError("state_combobox"), "This is a required field.");
		Assert.assertEquals(nlt.validateError("school_name_combobox"), "This is a required field.");
		Assert.assertEquals(nlt.validateError("course_name"), "This is a required field.");
		Assert.assertEquals(nlt.validateError("course_timezone_combobox"), "This is a required field.");
		Assert.assertEquals(nlt.validateError("course_start_date_picker"), "This is a required field.");
		Assert.assertEquals(nlt.validateError("course_end_date_picker"), "This is a required field.");
		//User selects country, state, province
		nlt.selectState(5);
		Thread.sleep(1000);
		nlt.selectSchoolName(4);
		nlt.setCourseName(courseName);
		//Select Time Zone 
		nlt.selectTimeZone("Eastern");
		timezone = nlt.getTimeZone();
		LogUtil.log(timezone);
		System.out.println((timezone));
		Thread.sleep(3000);
		nlt.selectStartDate();
		Thread.sleep(2000);
		nlt.selectEndDate();
		Thread.sleep(2000);
		String schoolName = nlt.getschoolNameCoursePage().trim();
		System.out.println(schoolName);
		String startDate = nlt.getstartdateCoursePage();
		String endDate = nlt.getEnddateCoursePage();
		nlt.clickContinue();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseMaterialText(), "Select Course Material");
		nlt.selectSearchByBookTitleRadioOption();
		nlt.selectDiscipline(getDiscipline);
		nlt.selectBookTitle(getBookTitle);
		nlt.selectEdition(getEdition);
		//nlt.clickTitleCodelink();
		/*titleCode =nlt.getTitleCode();
		System.out.println(titleCode);
		String title = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver,title);
		ReusableMethods.switchToNewWindow(2, driver);
		nlt.setProductCode(titleCode);*/
		Thread.sleep(2000);
		nlt.clickContinue();
		Assert.assertEquals(nlt.getconfirmBookpopup(), "Confirm Book");
		nlt.clickNobook();
		Assert.assertEquals(nlt.getCourseMaterialText(), "Select Course Material");
		Thread.sleep(2000);
		nlt.clickContinue();
		nlt.clickYesbook();
		Thread.sleep(2000);
		nlt.isSiteLicensePopUp();
		Assert.assertEquals(nlt.connCanvasText().trim(), "Connected to Canvas");
		Assert.assertEquals(nlt.getCourseDetails("Course Name:").trim(), courseName);
		Assert.assertEquals(nlt.getCourseDetails("School:").trim(), schoolName);
		Assert.assertEquals(nlt.getCourseDetails("Timezone:").trim(), timezone);		
		Assert.assertEquals(ReusableMethods.dateConversion(nlt.getCourseDetails("Start Date:")).trim(), startDate);
		Assert.assertEquals(ReusableMethods.dateConversion(nlt.getCourseDetails("End Date:")).trim(), endDate);
	//	Assert.assertEquals(nlt.gettitleCodeCourseinfopage().trim(), titleCode);
		bookCode = nltCourse.getBookDetails(0);
		bookTitle=nltCourse.getBookDetails(1);
		bookSubTitle=nltCourse.getBookDetails(2);
		bookEditon=nltCourse.getBookDetails(3); 
		bookAuthor=nltCourse.getBookDetails(4);		
		String startDatecc = nlt.getCourseDetails("Start Date:").trim();
		String endDatecc =nlt.getCourseDetails("End Date:").trim();
		nlt.clickCreateCourseButton();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCourseCreatedtext().trim(), "Your course has been created.");
		// Your course has been created. Page Verification 
		Assert.assertEquals(courseName,nltCourse.getCourseName());
		String schoolNamestr = nltCourse.getSchoolName();	
        LogUtil.log(schoolNamestr);
		//ReusableMethods.comparePartofString(schoolName, schoolNamestr);		
		Assert.assertEquals(startDatecc.trim(), nltCourse.getStartDate().replace("-", "").trim());
		Assert.assertEquals(endDatecc+" "+nltCourse.getTimeZone(), nltCourse.getEndDate());
		//Compare on Course Created page with Course Information page
		Assert.assertEquals(timezone,nltCourse.getTimeZone());
		Assert.assertEquals(bookCode,nltCourse.getBookDetails(0));
		Assert.assertEquals(bookTitle,nltCourse.getBookDetails(1));
		Assert.assertEquals(bookSubTitle,nltCourse.getBookDetails(2));
		Assert.assertEquals(bookEditon,nltCourse.getBookDetails(3)); 
		Assert.assertEquals(bookAuthor,nltCourse.getBookDetails(4));	
		
		Assert.assertEquals("Need to review or edit your assignments?", nltCourse.getheaderCalloutText().toString());
		Assert.assertEquals("Edit or create custom assignments before sending them to your LMS.", nltCourse.geteditAssignmentText().toString());
		Assert.assertTrue(nltCourse.getreviewCouseButton());
		Assert.assertEquals("Ready to send your assignments?", nltCourse.getReadysendassignmentsText().toString());
		String assignmentText =ReusableMethods.removeAsciiChar("We recommend only sending assignments after you’e finished editing them to ensure consistency between your Norton content and your LMS.");
		Assert.assertEquals(assignmentText, nltCourse.getsendAssignmentText().toString());
		Assert.assertTrue(nltCourse.getSelectContentButton());
		nltCourse.ClickSelectCouseButton();
		Thread.sleep(2000);
		Assert.assertEquals(nlt.getCoursecontentLMStext().trim(), "Select Course Content for Your LMS");
		String nlttitle = driver.getTitle();
		ReusableMethods.SwitchTabandClose(driver,nlttitle);
		ReusableMethods.switchToNewWindow(1, driver);
		Thread.sleep(5000);
		LMSpage.clickNavigationlink("Settings");
		driver.findElement(By.linkText("Delete this Course")).click();
		driver.findElement(
				By.xpath("//div[@class='form-actions']/button[@type='submit'][contains(text(),'Delete Course')]"))
				.click();
		Thread.sleep(3000);
		LMSpage.clicklink("Account");
		Thread.sleep(3000);
		LMSpage.logoutLMS();
	}
	@AfterTest
	public void Logout() throws InterruptedException {
		PropertiesFile.tearDownTest();
	}
}
